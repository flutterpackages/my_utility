/// Returns true if the generic type parameter `T` is nullable,
/// i.e. if `T` is actually `T?`.
bool isNullableType<T>() => null is T;
