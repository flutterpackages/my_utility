import "package:meta/meta.dart";
import "package:yaml/yaml.dart";

/// {@template my_utility.functions.yaml_map_to_json_map.yamlMapToJsonMap}
/// Recursively converts [map] to a new map of type `Map<String, dynamic>`.
///
/// [converters] may be used to convert some specific objects to json
/// representable objects. The first converter in the set that has a match
/// with the type of the current value, as per [ToJsonObjectConverter.isType]
/// will be used to convert the current value. There should only be one
/// converter for each object type.
/// {@endtemplate}
Map<String, dynamic> yamlMapToJsonMap(
  YamlMap map, {
  Set<ToJsonObjectConverter<dynamic>> converters = const {},
}) {
  final newMap = <String, dynamic>{};

  for (final MapEntry(key: k, value: v) in map.entries) {
    if (k is! String)
      throw FormatException(
        "Each key in the YamlMap must be of type String, but there is one "
        "of type '${k.runtimeType}'.",
        map,
        k,
      );

    newMap[k] = _switchValue(v, converters);
  }

  return newMap;
}

dynamic _switchValue(
  dynamic value,
  Set<ToJsonObjectConverter<dynamic>> converters,
) {
  switch (value) {
    case YamlMap():
      return yamlMapToJsonMap(
        value,
        converters: converters,
      );

    case YamlList():
      final newValue = List<dynamic>.filled(value.length, null);
      var i = 0;
      for (final item in value) {
        newValue[i] = _switchValue(item, converters);
        i++;
      }
      return newValue;

    case YamlNode():
      return _switchValue(value.value, converters);

    default:
      for (final item in converters) {
        if (item.isType(value)) return item.convert(value);
      }

      return value;
  }
}

extension YamlMapToJsonMapExtension on YamlMap {
  /// {@macro my_utility.functions.yaml_map_to_json_map.yamlMapToJsonMap}
  Map<String, dynamic> toJsonMap({
    Set<ToJsonObjectConverter<dynamic>> converters = const {},
  }) =>
      yamlMapToJsonMap(this, converters: converters);
}

/// A [ToJsonObjectConverter] is a class that checks if an object is of type `T`
/// and converts it to a json representable object. Converters are considered
/// equal, if their generic type argument `T` is the same.
@immutable
abstract base class ToJsonObjectConverter<T> {
  const ToJsonObjectConverter();

  @override
  int get hashCode => T.hashCode;

  @override
  bool operator ==(Object other) => other is ToJsonObjectConverter<T>;

  /// Returns true, if [value] is of type `T`.
  bool isType(dynamic value) => value is T;

  /// Converts the provided `value` to a json representable object.
  dynamic convert(T value);
}

@immutable
final class ToJsonObjectCallbackConverter<T> extends ToJsonObjectConverter<T> {
  const ToJsonObjectCallbackConverter(this._convert);

  final dynamic Function(T) _convert;

  @override
  dynamic convert(T value) => _convert(value);
}
