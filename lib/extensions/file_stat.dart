import "package:universal_io/io.dart";

import "../io/file_stat_permission.dart";

extension MyUtilityExtensionFileStatGroupPermissions on FileStat {
  Set<FileStatPermission> get groupPermissions =>
      _fromBitmask(permissions & 0x0F0);
}

extension MyUtilityExtensionFileStatOtherPermissions on FileStat {
  Set<FileStatPermission> get othersPermissions =>
      _fromBitmask(permissions & 0xF00);
}

extension MyUtilityExtensionFileStatOwnerPermissions on FileStat {
  Set<FileStatPermission> get ownerPermissions =>
      _fromBitmask(permissions & 0x00F);
}

extension MyUtilityExtensionFileStatPermissions on FileStat {
  int get permissions => mode & 0xFFF;
}

Set<FileStatPermission> _fromBitmask(int bitmask) {
  return FileStatPermission.values
      .where((element) => 0 != bitmask & element.mask)
      .toSet();
}
