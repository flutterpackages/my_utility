import "package:intl/intl.dart" as intl;

extension MyUtilityExtensionDateTimeEndOfDay on DateTime {
  /// Get a new [DateTime] with the same [year], [month] and [day],
  /// but the time is set to the last microsecond before a new day starts.
  DateTime get endOfDay => copyWith(
        hour: 23,
        minute: 59,
        second: 59,
        millisecond: 999,
        microsecond: 999,
      );
}

extension MyUtilityExtensionDateTimeStartOfDay on DateTime {
  /// Get a new [DateTime] with the same [year], [month] and [day],
  /// but the time is set to the first microsecond when [day] started.
  DateTime get startOfDay => copyWith(
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
        microsecond: 0,
      );
}

extension MyUtilityExtensionDateTimeToHumanReadableTimestamp on DateTime {
  /// Convert this [DateTime] to a human readable timestamp.
  ///
  /// ### Example
  ///
  /// ```dart
  /// final date = DateTime(2024, 5, 7, 5, 9, 4);
  ///
  /// // Prints "Tue\t7 May 05:09:04 CEST 2024"
  /// // The value of the timezone depends on your region.
  /// print(date.toHumanReadableTimestamp())
  /// ```
  String toHumanReadableTimestamp([String? locale]) {
    final date = this;

    // See https://pub.dev/documentation/intl/latest/intl/DateFormat-class.html
    var res = intl.DateFormat("E\td LLLL HH:mm:ss ", locale).format(date);
    res += "${date.timeZoneName} ${date.year.toString().padLeft(4, "0")}";

    return res;
  }
}

extension MyUtilityExtensionDateTimeToTimestamp on DateTime {
  /// Returns a timestamp of this [DateTime].
  ///
  /// If [toUtc] is true the this [DateTime] is converted to the
  /// UTC timezone and the result will be `yyyyMMddTHHmmss.mmmuuuZ`.
  ///
  /// If [includeFractions] is true then the milliseconds and microseconds
  /// will be included in the result, otherwise they will be left out.
  ///
  /// With [includeFractions] `yyyyMMddTHHmmss.mmmuuu`
  ///
  /// Without [includeFractions] `yyyyMMddTHHmmss`
  String toTimestamp({
    bool toUtc = true,
    bool includeFractions = true,
  }) {
    final date = toUtc ? this.toUtc() : this;

    var res = date.toIso8601String().replaceAll(RegExp("[-:]*"), "");
    if (includeFractions) return res;

    res = res.substring(0, res.indexOf("."));
    return "$res${toUtc ? "Z" : ""}";
  }
}
