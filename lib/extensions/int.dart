import "../src/int_extension.dart";
import "../src/utils.dart"
    show
        MyUtilityPlatformAgnosticUtilsExtensionBigInt,
        MyUtilityPlatformAgnosticUtilsExtensionInt;

extension MyUtilityExtensionIntIsSafe on int {
  /// Check whether or not this is a safe integer for the current platform.
  ///
  /// This getter only really makes sense when compiling to JavaScript
  /// because, integers are restricted to values that can be represented
  /// exactly by double-precision floating point values. The available integer
  /// values include all integers between -2^53 and 2^53, and some integers
  /// with larger magnitude.
  ///
  /// For example, when compiling to JavaScript an integer is considered safe,
  /// if [Number.isSafeInteger](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger) returns true.
  ///
  /// When compiling to native Dart VM code, an integer is always considered
  /// safe.
  bool get isSafe => MyUtilityPlatformAgnosticUtilsExtensionInt(this).isSafe;
}

extension MyUtilityExtensionIntIsUnsigned on int {
  /// Check if this is an unsigned integer with the specified [size].
  ///
  /// [size] must be between 1 and 8, both inclusive, else an [ArgumentError]
  /// gets thrown.
  bool isUnsigned([int size = 8]) => isUnsignedInteger(this, size);
}

extension MyUtilityExtensionIntIsUint8 on int {
  /// Check if this is an unsigned 8 bit integer.
  bool get isUint8 => isUnsigned(1);
}

extension MyUtilityExtensionIntIsUint16 on int {
  /// Check if this is an unsigned 16 bit integer.
  bool get isUint16 => isUnsigned(2);
}

extension MyUtilityExtensionIntIsUint32 on int {
  /// Check if this is an unsigned 32 bit integer.
  bool get isUint32 => isUnsigned(4);
}

extension MyUtilityExtensionIntIsUint64 on int {
  /// Check if this is an unsigned 64 bit integer.
  bool get isUint64 => isUnsigned(8);
}

extension MyUtilityExtensionIntToRadixStringAsUnsigned on int {
  /// Converts this [int] to a string representation in the given [radix].
  /// This is always interpreted as an unsigned integer, which means that
  /// negative numbers will be interpreted as per their [Two's Complement](https://en.wikipedia.org/wiki/Two's_complement)
  /// representation.
  ///
  /// In the string representation, lower-case letters are used for digits above
  /// '9', with 'a' being 10 and 'f' being 15.
  ///
  /// The [radix] argument must be 2 or 16.
  ///
  /// Example:
  /// ```dart
  /// // Binary (base 2).
  /// print(12.toRadixStringAsUnsigned(2)); // 1100
  /// print(31.toRadixStringAsUnsigned(2)); // 11111
  /// print(2021.toRadixStringAsUnsigned(2)); // 11111100101
  /// print((-12).toRadixStringAsUnsigned(2)); // 1111111111111111111111111111111111111111111111111111111111110100
  ///
  /// // Octal (base 8).
  /// print(12.toRadixStringAsUnsigned(8)); // 14
  /// print(31.toRadixStringAsUnsigned(8)); // 37
  /// print(2021.toRadixStringAsUnsigned(8)); // 3745
  /// print((-12).toRadixStringAsUnsigned(8)); // 1777777777777777777764
  ///
  /// // Hexadecimal (base 16).
  /// print(12.toRadixStringAsUnsigned(16)); // c
  /// print(31.toRadixStringAsUnsigned(16)); // 1f
  /// print(2021.toRadixStringAsUnsigned(16)); // 7e5
  /// print((-12).toRadixStringAsUnsigned(16)); // fffffffffffffff4
  ///
  /// // Base 36.
  /// print(((35 * 36 + 1).toRadixStringAsUnsigned(36)); // z1
  /// ```
  ///
  /// See also [toRadixString]
  String toRadixStringAsUnsigned(int radix) {
    return BigInt.from(this)
        .checkIsSafeInteger()
        .toUnsigned(64)
        .toRadixString(radix);
  }
}
