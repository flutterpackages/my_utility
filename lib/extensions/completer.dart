import "dart:async";

extension MyUtilityExtensionCompleterCompleteErrorSafe<T> on Completer<T> {
  /// Checks if [isCompleted] is true, before calling [completeError].
  void completeErrorSafe(Object error, [StackTrace? stackTrace]) {
    if (isCompleted) return;
    completeError(error, stackTrace);
  }
}

extension MyUtilityExtensionCompleterCompleteSafe<T> on Completer<T> {
  /// Checks if [isCompleted] is true, before calling [complete].
  void completeSafe([FutureOr<T>? value]) {
    if (isCompleted) return;
    complete(value);
  }
}

extension MyUtilityExtensionCompleterIsNotCompleted<T> on Completer<T> {
  bool get isNotCompleted => !isCompleted;
}
