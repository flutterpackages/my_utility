extension MyUtilityExtensionSymbolName on Symbol {
  String get name {
    final s = toString();
    return s.substring(s.indexOf('"') + 1, s.lastIndexOf('"'));
  }
}
