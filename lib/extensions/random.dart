import "dart:collection";
import "dart:math" as math;
import "dart:typed_data";

import "../date.dart";
import "../extensions.dart";

extension MyUtilityExtensionRandomNextAlphaNumericString on math.Random {
  /// Generates a [String] containing only alpha numeric characters.
  ///
  /// Alpha numeric characters include lower- and uppercase characters from
  /// `a` to `z` and numbers from `0` to `9`.
  String nextAlphaNumericString(int length) {
    return nextAsciiString(
      length,
      ranges: const [
        // 0 to 9
        ConstRange<int>(48, 57),
        // A to Z
        ConstRange<int>(65, 90),
        // a to z
        ConstRange<int>(97, 122),
      ],
    );
  }
}

extension MyUtilityExtensionRandomNextAsciiCharacter on math.Random {
  /// Generates a [String] containing a randomly generated ASCII character
  /// uniformly distributed in the range from [asciiStart], inclusive, to
  /// [asciiEnd], inclusive.
  ///
  /// By default a character between `A` and `Z` will be generated.
  ///
  /// Use [nextAsciiString] if you want to generate multiple characters,
  ///
  /// See [ASCII Table](https://www.ascii-code.com/)
  String nextAsciiCharacter({
    int asciiStart = 65,
    int asciiEnd = 90,
  }) {
    _checkAsciiRangeValues(asciiStart, "asciiStart", asciiEnd, "asciiEnd");
    return String.fromCharCode(nextIntBetween(asciiStart, asciiEnd + 1));
  }
}

extension MyUtilityExtensionRandomNextAsciiNumberString on math.Random {
  /// Generates a [String] containing uniformly distributed and randomly
  /// generated ASCII number characters .
  ///
  /// See [ASCII Table](https://www.ascii-code.com/)
  String nextAsciiNumberString(int length) => nextAsciiString(
        length,
        ranges: const [ConstRange<int>(48, 57)],
      );
}

extension MyUtilityExtensionRandomNextAsciiString on math.Random {
  /// Generates a [String] containing randomly generated ASCII characters
  /// uniformly distributed in the range from [asciiStart] to [asciiEnd],
  /// both inclusive.
  ///
  /// By default a [String] containing only the characters between `A`
  /// and `Z` will be generated.
  ///
  /// See [ASCII Table](https://www.ascii-code.com/)
  String nextAsciiString(
    int length, {
    List<Range<int>> ranges = const [],
    @Deprecated(
      "Will be removed in version 2.0.0, use "
      "the 'ranges' parameter instead.",
    )
    int asciiStart = 65,
    @Deprecated(
      "Will be removed in version 2.0.0, use "
      "the 'ranges' parameter instead.",
    )
    int asciiEnd = 90,
  }) {
    _checkAsciiRangeValues(asciiStart, "asciiStart", asciiEnd, "asciiEnd");
    final asciiArgsRange = IntRange(asciiStart, asciiEnd);

    const defaultRange = ConstRange<int>(65, 90);

    return _nextAsciiStringFromRanges(
      length,
      ranges: [
        ...ranges,
        if (ranges.isEmpty) defaultRange,
        if (!_equalRanges(asciiArgsRange, defaultRange)) asciiArgsRange,
      ],
    );
  }

  String _nextAsciiStringFromRanges(
    int length, {
    List<Range<int>> ranges = const [ConstRange<int>(65, 90)],
  }) {
    final uniqueRanges = HashSet<Range<int>>(
      equals: _equalRanges,
      hashCode: (e) => Object.hash(e.start, e.endInclusive),
    );

    for (var i = 0; i < ranges.length; i++) {
      final item = ranges[i];
      _checkAsciiRange(item, "ranges[$i]");
      uniqueRanges.add(item);
    }

    final uniqueRangesAsList = uniqueRanges.toList(growable: false);

    final codes = List<int>.filled(length, -1);
    for (var i = 0; i < length; i++) {
      final range = uniqueRangesAsList.getRandom(this);
      codes[i] = nextIntInRange(range);
    }

    return String.fromCharCodes(codes);
  }
}

extension MyUtilityExtensionRandomNextBytes on math.Random {
  /// Generates a list of non-negative random bytes uniformly distributed in
  /// the range from [min], inclusive, to [max], exclusive.
  ///
  /// [count] determines how many bytes will be generated. Must be greater
  /// than 0.
  ///
  /// The provided range, given by [min] and [max], must be valid. A range
  /// from [min] to [max] is valid if `0 ≤ min < max ≤ 256`.
  Uint8List nextBytes(
    int count, {
    int min = 0,
    int max = 256,
  }) {
    if (count < 0)
      throw ArgumentError.value(
        count,
        "count",
        "Must be greater than 0",
      );

    RangeError.checkValueInInterval(min, 0, 255, "min");
    RangeError.checkValueInInterval(max, 1, 256, "max");
    if (min >= max)
      throw RangeError(
        "The minimum value ($min) must be less than the maximum value ($max)",
      );

    final bytes = Uint8List(count);
    for (var i = 0; i < count; i++) {
      bytes[i] = nextIntBetween(min, max);
    }

    return bytes;
  }
}

extension MyUtilityExtensionRandomNextDateTime on math.Random {
  /// Generates a [DateTime].
  ///
  /// ## Parameters
  ///
  /// [yearRange] - The range for the year. If omitted, then the default
  /// inclusive minimum year is `1900` and the inclusive maximum year is
  /// `9999`.
  ///
  /// [monthRange] - The range for the month. If set to null then a random
  /// month between `1` and [DateTime.monthsPerYear] will be generated.
  ///
  /// [dayRange] - The range for the day. If set to null then a random day
  /// between `1` and the result returned by
  /// `MyDateUtility.getDaysInMonth(year, month)` will be generated.
  ///
  /// [hourRange] - The range for the hour. If set to null then a random hour
  /// between 0 inclusive and 23 inclusive will be generated. Defaults to `0`.
  ///
  /// [minuteRange] - The range for the minute. If set to null then a random
  /// minute between 0 inclusive and 59 inclusive will be generated.
  /// Defaults to `0`.
  ///
  /// [secondRange] - The range for the second. If set to null then a random
  /// second between 0 inclusive and 59 inclusive will be generated.
  /// Defaults to `0`.
  ///
  /// [millisecondRange] - The range for the second. If set to null then a
  /// random millisecond between 0 inclusive and 999 inclusive will be
  /// generated. Defaults to `0`.
  ///
  /// [microsecondRange] - The range for the second. If set to null then a
  /// random microsecond between 0 inclusive and 999 inclusive will be
  /// generated. Defaults to `0`.
  ///
  /// [isUtc] - Whether or not the new [DateTime] should be created in the UTC
  /// timezone, or in the local timezone. Defaults to the local timezone.
  ///
  /// ## See
  ///
  ///   * [MyDateUtility.getDaysInMonth]
  DateTime nextDateTime({
    Range<int>? yearRange,
    Range<int>? monthRange = const ConstRange.single(1),
    Range<int>? dayRange = const ConstRange.single(1),
    Range<int>? hourRange = ConstRange.zero,
    Range<int>? minuteRange = ConstRange.zero,
    Range<int>? secondRange = ConstRange.zero,
    Range<int>? millisecondRange = ConstRange.zero,
    Range<int>? microsecondRange = ConstRange.zero,
    bool isUtc = false,
  }) {
    final int year;
    if (null == yearRange) {
      year = nextIntBetween(1900, 10000);
    } else if (yearRange.start == yearRange.endInclusive) {
      year = yearRange.start;
    } else {
      // Check that start is greater than 0
      if (yearRange.start <= 0)
        throw ArgumentError.value(
          yearRange.toString(),
          "yearRange.start",
          "Invalid range. The start must be greater than 0",
        );

      // Check that inc. end is greater than, or equal to start
      if (yearRange.start > yearRange.endInclusive)
        throw ArgumentError.value(
          yearRange.toString(),
          "yearRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      year = nextIntInRange(yearRange);
    }

    final int month;
    if (null == monthRange) {
      month = nextIntBetween(1, DateTime.monthsPerYear + 1);
    } else if (monthRange.start == monthRange.endInclusive) {
      month = monthRange.start;
    } else {
      // Check that start is greater than 0
      if (monthRange.start <= 0)
        throw ArgumentError.value(
          monthRange.toString(),
          "monthRange.start",
          "Invalid range. The start must be greater than 0",
        );

      // Check that inc. end is smaller than, or equal to DateTime.monthsPerYear
      if (monthRange.endInclusive > DateTime.monthsPerYear)
        throw ArgumentError.value(
          monthRange.toString(),
          "monthRange.endInclusive",
          "Invalid range. The inclusive end must be smaller than, or equal to, "
              "the amount of months per year, which is "
              "${DateTime.monthsPerYear}",
        );

      // Check that inc. end is greater than, or equal to start
      if (monthRange.start > monthRange.endInclusive)
        throw ArgumentError.value(
          monthRange.toString(),
          "monthRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      month = nextIntInRange(monthRange);
    }

    final int day;
    final daysInMonth = MyDateUtility.getDaysInMonth(year, month);
    if (null == dayRange) {
      day = nextIntBetween(1, daysInMonth + 1);
    } else if (dayRange.start == dayRange.endInclusive) {
      day = dayRange.start;
    } else {
      // Check that start is greater than 0
      if (dayRange.start <= 0)
        throw ArgumentError.value(
          dayRange.toString(),
          "dayRange.start",
          "Invalid range. The start must be greater than 0",
        );

      // Check that inc. end is smaller than, or equal to maxDays
      if (dayRange.endInclusive > daysInMonth)
        throw ArgumentError.value(
          dayRange.toString(),
          "dayRange.endInclusive",
          "Invalid range. The inclusive end must be smaller than, or equal to "
              "the amount of days in the $month.month of the year $year, which "
              "is $daysInMonth",
        );

      // Check that inc. end is greater than, or equal to start
      if (dayRange.start > dayRange.endInclusive)
        throw ArgumentError.value(
          dayRange.toString(),
          "dayRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      day = nextIntInRange(dayRange);
    }

    final int hour;
    if (null == hourRange) {
      hour = nextIntBetween(0, 23 + 1);
    } else if (hourRange.start == hourRange.endInclusive) {
      hour = hourRange.start;
    } else {
      // Check that start is greater than 0
      if (hourRange.start <= 0)
        throw ArgumentError.value(
          hourRange.toString(),
          "hourRange.start",
          "Invalid range. The start must be greater than, or equal to 0",
        );

      // Check that inc. end is smaller than, or equal to 23
      if (hourRange.endInclusive > 23)
        throw ArgumentError.value(
          hourRange.toString(),
          "hourRange.endInclusive",
          "Invalid range. The inclusive end must be smaller than, or equal to "
              "23. This is because a day has 24 hours and valid hours are from "
              "0 inclusive to 23 inclusive.",
        );

      // Check that inc. end is greater than, or equal to start
      if (hourRange.start > hourRange.endInclusive)
        throw ArgumentError.value(
          hourRange.toString(),
          "hourRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      hour = nextIntInRange(hourRange);
    }

    final int minute;
    if (null == minuteRange) {
      minute = nextIntBetween(0, 59 + 1);
    } else if (minuteRange.start == minuteRange.endInclusive) {
      minute = minuteRange.start;
    } else {
      // Check that start is greater than 0
      if (minuteRange.start <= 0)
        throw ArgumentError.value(
          minuteRange.toString(),
          "minuteRange.start",
          "Invalid range. The start must be greater than, or equal to 0",
        );

      // Check that inc. end is smaller than, or equal to 59
      if (minuteRange.endInclusive > 59)
        throw ArgumentError.value(
          minuteRange.toString(),
          "minuteRange.endInclusive",
          "Invalid range. The inclusive end must be smaller than, or equal to "
              "59. This is because an hour has 60 minutes and valid minutes "
              "are from 0 inclusive to 59 inclusive.",
        );

      // Check that inc. end is greater than, or equal to start
      if (minuteRange.start > minuteRange.endInclusive)
        throw ArgumentError.value(
          minuteRange.toString(),
          "minuteRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      minute = nextIntInRange(minuteRange);
    }

    final int second;
    if (null == secondRange) {
      second = nextIntBetween(0, 59 + 1);
    } else if (secondRange.start == secondRange.endInclusive) {
      second = secondRange.start;
    } else {
      // Check that start is greater than 0
      if (secondRange.start <= 0)
        throw ArgumentError.value(
          secondRange.toString(),
          "secondRange.start",
          "Invalid range. The start must be greater than, or equal to 0",
        );

      // Check that inc. end is smaller than, or equal to 59
      if (secondRange.endInclusive > 59)
        throw ArgumentError.value(
          secondRange.toString(),
          "secondRange.endInclusive",
          "Invalid range. The inclusive end must be smaller than, or equal to "
              "59. This is because a minute has 60 seconds and valid seconds "
              "are from 0 inclusive to 59 inclusive.",
        );

      // Check that inc. end is greater than, or equal to start
      if (secondRange.start > secondRange.endInclusive)
        throw ArgumentError.value(
          secondRange.toString(),
          "secondRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      second = nextIntInRange(secondRange);
    }

    final int millisecond;
    if (null == millisecondRange) {
      millisecond = nextIntBetween(0, 999 + 1);
    } else if (millisecondRange.start == millisecondRange.endInclusive) {
      millisecond = millisecondRange.start;
    } else {
      // Check that start is greater than 0
      if (millisecondRange.start <= 0)
        throw ArgumentError.value(
          millisecondRange.toString(),
          "millisecondRange.start",
          "Invalid range. The start must be greater than, or equal to 0",
        );

      // Check that inc. end is smaller than, or equal to 999
      if (millisecondRange.endInclusive > 999)
        throw ArgumentError.value(
          millisecondRange.toString(),
          "millisecondRange.endInclusive",
          "Invalid range. The inclusive end must be smaller than, or equal to "
              "999. This is because a second has 1000 milliseconds and valid "
              "milliseconds are from 0 inclusive to 999 inclusive.",
        );

      // Check that inc. end is greater than, or equal to start
      if (millisecondRange.start > millisecondRange.endInclusive)
        throw ArgumentError.value(
          millisecondRange.toString(),
          "millisecondRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      millisecond = nextIntInRange(millisecondRange);
    }

    final int microsecond;
    if (null == microsecondRange) {
      microsecond = nextIntBetween(0, 999 + 1);
    } else if (microsecondRange.start == microsecondRange.endInclusive) {
      microsecond = microsecondRange.start;
    } else {
      // Check that start is greater than 0
      if (microsecondRange.start <= 0)
        throw ArgumentError.value(
          microsecondRange.toString(),
          "microsecondRange.start",
          "Invalid range. The start must be greater than, or equal to 0",
        );

      // Check that inc. end is smaller than, or equal to 999
      if (microsecondRange.endInclusive > 999)
        throw ArgumentError.value(
          microsecondRange.toString(),
          "microsecondRange.endInclusive",
          "Invalid range. The inclusive end must be smaller than, or equal to "
              "999. This is because a millisecond has 1000 microseconds and "
              "valid microseconds are from 0 inclusive to 999 inclusive.",
        );

      // Check that inc. end is greater than, or equal to start
      if (microsecondRange.start > microsecondRange.endInclusive)
        throw ArgumentError.value(
          microsecondRange.toString(),
          "microsecondRange",
          "Invalid range. "
              "The start must be before, or equal to the inclusive end",
        );

      microsecond = nextIntInRange(microsecondRange);
    }

    return isUtc
        ? DateTime.utc(
            year,
            month,
            day,
            hour,
            minute,
            second,
            millisecond,
            microsecond,
          )
        : DateTime(
            year,
            month,
            day,
            hour,
            minute,
            second,
            millisecond,
            microsecond,
          );
  }
}

extension MyUtilityExtensionRandomNextIntBetween on math.Random {
  /// Generates a non-negative random integer uniformly distributed in the range
  /// from [min], inclusive, to [max], exclusive.
  int nextIntBetween(int min, int max) {
    final val = nextInt(max - min);
    return min + val;
  }
}

extension MyUtilityExtensionRandomNextIntInRange on math.Random {
  /// Generates a non-negative random integer uniformly distributed in the range
  /// from `range.start` to `range.endInclusive`, both inclusive.
  int nextIntInRange(Range<int> range) {
    return nextIntBetween(range.start, range.endInclusive + 1);
  }
}
//

void _checkAsciiRange(Range<int> range, String name) {
  _checkAsciiRangeValues(
    range.start,
    "$name.start",
    range.endInclusive,
    "$name.end",
  );
}

void _checkAsciiRangeValues(
  int start,
  String startName,
  int end,
  String endName,
) {
  const validAsciiRange =
      "ASCII characters only range from 0 to 127, both inclusive.";

  RangeError.checkValueInInterval(
    start,
    0,
    127,
    startName,
    validAsciiRange,
  );

  RangeError.checkValueInInterval(
    end,
    0,
    127,
    endName,
    validAsciiRange,
  );

  if (start > end)
    throw RangeError(
      "The start value for the ascii characters ($startName=$start) must be "
      "less than, or equal to the maximum value ($endName=$end)",
    );
}

bool _equalRanges(Range<int> a, Range<int> b) {
  return a.start == b.start && a.endInclusive == b.endInclusive;
}
