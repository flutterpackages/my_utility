import "../dartx.dart";
import "../errors/unreachable_code_error.dart";

extension MyUtilityExtensionDeepCopyIterable<T> on Iterable<T> {
  static final _copyFactories = <Type, dynamic Function(dynamic)>{};

  static void addCopyFactory<E>(E Function(E value) factory) {
    _copyFactories[E] = (value) => factory(value);
  }

  static void removeCopyFactory<E>() {
    _copyFactories.remove(E);
  }

  static E _deepCopy<E>(E element) {
    switch (element) {
      case Iterable():
        return _deepCopyIterable(element) as E;

      case Map():
        // The MyUtilityExtensionDeepCopyIterableOfMap should always be
        // used, if T is a Map, because it is a direct extension on
        // Iterable<Map<K,V>>.
        throw UnreachableCodeError();

      default:
        // Use a user-defined factory for the current type, if there is one.
        for (final MapEntry(key: type, value: factory)
            in _copyFactories.entries) {
          if (element.runtimeType == type) return factory(element) as E;
        }

        return element;
    }
  }

  static List<E> _deepCopyIterable<E>(
    Iterable<E> value, [
    bool growable = true,
  ]) {
    // The copy object must be created using the .toList() method
    // so that copy is of type List<E>. If just a list literal (<E>[]) is used
    // then copy will be of type List<dynamic>.
    final copy = value.toList();

    // Clear the list and add all deep copied values.
    copy.clear();
    for (final item in value) {
      copy.add(_deepCopy(item));
    }

    return growable ? copy : copy.toUnmodifiable();
  }

  /// Returns a new [List] that contains a deep copy of
  /// every element from this.
  List<T> deepCopy({bool growable = true}) {
    return map(_deepCopy).toList(growable: growable);
  }
}

extension MyUtilityExtensionDeepCopyIterableOfMap<K, V> on Iterable<Map<K, V>> {
  /// Returns a new [List] that contains a deep copy of
  /// every element from this.
  List<Map<K, V>> deepCopy({bool growable = true}) {
    return map((e) => e.deepCopy()).toList(growable: growable);
  }
}

extension MyUtilityExtensionDeepCopyIterableOfMapWithKeyMap<K1, V1, V>
    on Iterable<Map<Map<K1, V1>, V>> {
  /// Returns a new [List] that contains a deep copy of
  /// every element from this.
  List<Map<Map<K1, V1>, V>> deepCopy({bool growable = true}) {
    return map((e) => e.deepCopy()).toList(growable: growable);
  }
}

extension MyUtilityExtensionDeepCopyIterableOfMapWithValueMap<K1, V1, K>
    on Iterable<Map<K, Map<K1, V1>>> {
  /// Returns a new [List] that contains a deep copy of
  /// every element from this.
  List<Map<K, Map<K1, V1>>> deepCopy({bool growable = true}) {
    return map((e) => e.deepCopy()).toList(growable: growable);
  }
}

extension MyUtilityExtensionDeepCopyIterableOfMapWithKeyAndValueMap<K1, V1, K2,
    V2> on Iterable<Map<Map<K1, V1>, Map<K2, V2>>> {
  /// Returns a new [List] that contains a deep copy of
  /// every element from this.
  List<Map<Map<K1, V1>, Map<K2, V2>>> deepCopy({bool growable = true}) {
    return map((e) => e.deepCopy()).toList(growable: growable);
  }
}

//

extension MyUtilityExtensionDeepCopyMap<K, V> on Map<K, V> {
  /// Returns a new [Map] that contains a deep copy of
  /// every key and value from this.
  // ignore: use_to_and_as_if_applicable
  Map<K, V> deepCopy() => Map.of(this);
}

extension MyUtilityExtensionDeepCopyMapWithKeyMap<K1, V1, V>
    on Map<Map<K1, V1>, V> {
  /// Returns a new [Map] that contains a deep copy of
  /// every key and value from this.
  Map<Map<K1, V1>, V> deepCopy() => Map.fromIterables(
        keys.map((e) => e.deepCopy()),
        values,
      );
}

extension MyUtilityExtensionDeepCopyMapWithValueMap<K1, V1, K>
    on Map<K, Map<K1, V1>> {
  /// Returns a new [Map] that contains a deep copy of
  /// every key and value from this.
  Map<K, Map<K1, V1>> deepCopy() => Map.fromIterables(
        keys,
        values.map((e) => e.deepCopy()),
      );
}

extension MyUtilityExtensionDeepCopyMapWithKeyAndValueMap<K1, V1, K2, V2>
    on Map<Map<K1, V1>, Map<K2, V2>> {
  /// Returns a new [Map] that contains a deep copy of
  /// every key and value from this.
  Map<Map<K1, V1>, Map<K2, V2>> deepCopy() => Map.fromIterables(
        keys.map((e) => e.deepCopy()),
        values.map((e) => e.deepCopy()),
      );
}
