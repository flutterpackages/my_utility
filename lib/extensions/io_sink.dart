import "dart:async";
import "package:universal_io/io.dart";

extension MyUtilityExtensionIOSinkIsStdioSink on IOSink {
  /// Returns true if this [IOSink] is a [Stdout].
  bool get isStdioSink => this is Stdout;
}

extension MyUtilityExtensionIOSinkUse on IOSink {
  /// Write some data to this [IOSink] in [workload] and after the [workload]
  /// is done, call [flush] and [close].
  Future<void> use(FutureOr<void> Function(IOSink sink) workload) async {
    await workload(this);
    await flush();
    await close();
  }
}
