// Always await results before returning them, because extension functions
// should be in the stack trace when an error occures.
// ignore_for_file: unnecessary_await_in_return

import "dart:async";
import "dart:convert";
import "dart:typed_data";

import "package:dartx/dartx.dart";
import "package:path/path.dart" as lib_path;
import "package:universal_io/io.dart";

import "../exceptions.dart";
import "../extensions.dart";
import "../src/get_unique_file.dart" as get_unique_file_impl;
import "../src/io_typedefs.dart";
import "../src/utils.dart";

extension MyUtilityExtensionFileAppendBytes on File {
  /// Appends an array of [bytes] to the end of this file.
  Future<void> appendBytes(List<int> bytes) async {
    final sink = openWrite(mode: FileMode.writeOnlyAppend);
    sink.add(bytes);
    await sink.flush();
    await sink.close();
  }
}

extension MyUtilityExtensionFileAppendString on File {
  /// Appends a [string] to the end of this file using the specified [encoding].
  Future<void> appendString(String string, {Encoding encoding = utf8}) async {
    final sink = openWrite(mode: FileMode.writeOnlyAppend, encoding: encoding);
    sink.write(string);
    await sink.flush();
    await sink.close();
  }
}

extension MyUtilityExtensionFileChangeFilename on File {
  /// Asynchronously changes **only the name** of this file.
  ///
  /// See also [changeFilenameSync].
  Future<File> changeFilename(
    String newFileName, {
    lib_path.Context? pathContext,
  }) async {
    pathContext ??= lib_path.context;
    final lastSeparatorIdx = path.lastIndexOf(pathContext.separator);
    final newPath = path.substring(0, lastSeparatorIdx + 1) + newFileName;
    return await rename(newPath);
  }
}

extension MyUtilityExtensionFileChangeFilenameSync on File {
  /// Synchronously changes **only the name** of this file.
  ///
  /// See also [changeFilename].
  File changeFilenameSync(
    String newFileName, {
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;
    final lastSeparatorIdx = path.lastIndexOf(pathContext.separator);
    final newPath = path.substring(0, lastSeparatorIdx + 1) + newFileName;
    return renameSync(newPath);
  }
}

extension MyUtilityExtensionFileMakeFilenameUnique on File {
  /// Asynchronously changes **only the name** of this file to a new unique
  /// name that does not already exist in [parent].
  ///
  /// * See [MyUtilityExtensionDirectoryGetUniqueFilename.getUniqueFilename]
  ///   for creating uniquely named files.
  ///
  /// * See [changeFilename] to asynchronously change the name of a file.
  ///
  /// * See [makeFilenameUniqueSync].
  Future<File> makeFilenameUnique({
    lib_path.Context? pathContext,
  }) async {
    pathContext ??= lib_path.context;

    final names = await get_unique_file_impl.getNamesInDir(
      directory: parent,
      pathContext: pathContext,
    );
    final uniqueName = get_unique_file_impl.getUniqueFilename(
      namesInDir: names,
      currentDir: parent,
    );

    return await changeFilename(uniqueName, pathContext: pathContext);
  }
}

extension MyUtilityExtensionFileMakeFilenameUniqueSync on File {
  /// Synchronously changes **only the name** of this file to a new unique
  /// name that does not already exist in [parent].
  ///
  /// * See [MyUtilityExtensionDirectoryGetUniqueFilename.getUniqueFilename]
  ///   for creating uniquely named files.
  ///
  /// * See [changeFilename] to asynchronously change the name of a file.
  ///
  /// * See [makeFilenameUnique].
  File makeFilenameUniqueSync({
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;

    final names = get_unique_file_impl.getNamesInDirSync(
      directory: parent,
      pathContext: pathContext,
    );
    final uniqueName = get_unique_file_impl.getUniqueFilename(
      namesInDir: names,
      currentDir: parent,
    );

    return changeFilenameSync(uniqueName, pathContext: pathContext);
  }
}

extension MyUtilityExtensionFileCopyWithProgress on File {
  /// Copies this file.
  ///
  /// If [newPath] is a relative path, it is resolved against
  /// the current working directory (`pathContext.current`).
  ///
  /// Returns a `Future<File>` that completes with a [File] for the copied file.
  ///
  /// If [newPath] identifies an existing file, that file is
  /// removed first. If [newPath] identifies an existing directory, the
  /// operation fails and the future completes with an exception.
  Future<File> copyWithProgress(
    String newPath,
    FileSystemEntityCopyProgressCb onProgress, {
    lib_path.Context? pathContext,
    FileFactory fileFactory = File.new,
    FileSystemEntityTypeProvider typeProvider = FileSystemEntity.type,
  }) async {
    pathContext ??= lib_path.context;

    // Resolve newPath against current directory, if it
    // is relative.
    final String resolvedNewPath;
    if (pathContext.isRelative(newPath)) {
      resolvedNewPath = pathContext.absolute(pathContext.current, newPath);
    } else {
      resolvedNewPath = newPath;
    }

    // Check that there exists no directory at newPath. Throw an exception
    // if anything other than a file already exists at newPath.
    final newPathType = await typeProvider(resolvedNewPath);

    if (FileSystemEntityType.file != newPathType &&
        FileSystemEntityType.notFound != newPathType)
      throw PathExistsException(
        newPath,
        const OSError(),
        "Cannot copy the file at '$path' to '$resolvedNewPath', because "
        "the target already exists and is of type: $newPathType",
      );

    final totalBytes = await length();
    var copiedBytes = 0;

    final newFile = fileFactory(resolvedNewPath);
    await newFile.recreate();

    final iStream = openRead();
    final sink = newFile.openWrite(mode: FileMode.writeOnly);

    await for (final bytes in iStream) {
      sink.add(bytes);
      copiedBytes += bytes.length;
      onProgress(totalBytes, copiedBytes);
    }

    await sink.flush();
    await sink.close();

    return newFile;
  }
}

extension MyUtilityExtensionFileCopyWithProgressIntoDirectory on File {
  /// Copies this file into [target].
  ///
  /// Returns a `Future<File>` that completes with a [File] for the copied file.
  ///
  /// If [target] already contains a file with the same name as this, then the
  /// existing file is removed first. If [target] contains a directory with the
  /// same name as this, then the operation fails and the future completes with
  /// an exception.
  Future<void> copyWithProgressIntoDirectory(
    Directory target,
    FileSystemEntityCopyProgressCb onProgress, {
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;
    final newFilepath = pathContext.join(
      target.path,
      getName(pathContext: pathContext),
    );
    return copyWithProgress(newFilepath, onProgress);
  }
}

extension MyUtilityExtensionFileExtension on File {
  /// See [getExtension].
  ///
  /// {@macro my_utility.extensions.file_system_entity.current_ctx}
  String get extension => getExtension();
}

extension MyUtilityExtensionFileForEachBlock on File {
  /// Reads file as byte blocks, where each block has the size [blockSize]. The
  /// last block may be smaller than [blockSize].
  ///
  /// [action] is called with each block.
  Future<void> forEachBlock(
    int blockSize,
    void Function(Uint8List buffer) action,
  ) async {
    assert(blockSize > 0);

    final raf = await open();

    while (true) {
      final buffer = await raf.read(blockSize);
      if (buffer.isEmpty) break;
      action(buffer);
    }

    await raf.close();
  }
}

extension MyUtilityExtensionFileGetExtension on File {
  /// Returns the file extension of the [path], the portion of the `name`
  /// from the last '.' to the end (including the '.' itself).
  ///
  /// ```dart
  /// File('path/to/foo.dart').getExtension(); // -> '.dart'
  /// File('path/to/foo').getExtension(); // -> ''
  /// File('path.to/foo').getExtension(); // -> ''
  /// File('path/to/foo.dart.js').getExtension(); // -> '.js'
  /// ```
  ///
  /// If the filename starts with a '.', then that is not considered an
  /// extension.
  ///
  /// ```dart
  /// File('~/.profile').getExtension();    // -> ''
  /// File('~/.notes.txt').getExtension();    // -> '.txt'
  /// ```
  String getExtension({lib_path.Context? pathContext}) {
    pathContext ??= lib_path.context;
    return pathContext.extension(path);
  }
}

extension MyUtilityExtensionFileGetNewFileWithSameName on File {
  /// Returns a new file with the same basename as this in [parent].
  ///
  /// If [parent] already contains a file with the same basename as this, then
  /// a counter will be appended to the new [File]s' basename, in the form
  /// `basename (counter).extension`.
  ///
  /// Example:
  /// dart
  /// ```
  /// ...
  /// final file = File("script.dart");
  /// final newFile = file.getNewFileWithIdenticalBasename();
  /// print(newFile.name); // -> prints "script (1).dart"
  /// ...
  /// ```
  File getNewFileWithIdenticalBasename({
    int maxTries = 999,
    lib_path.Context? pathContext,
    FileFactory factory = File.new,
  }) {
    // If this file does not already exist, return it
    if (!existsSync()) return this;

    pathContext ??= lib_path.context;

    // Match:
    // filename (123).ext
    // filename (123)
    //
    // But do not match:
    // filename(123).ext
    // filename ()
    // filename ().ext
    final indexFileRegex = RegExp(
      "(?<basename>.*)"
      r" \((?<index>\d+)\)"
      r"(\.(?<extension>.+))?",
    );

    final thisMatch = indexFileRegex.firstMatch(
      getName(pathContext: pathContext),
    );

    int index;
    final String basename;
    final String ext;
    if (null == thisMatch) {
      index = 0;
      basename = getNameWithoutExtension(pathContext: pathContext);
      ext = extension;
    } else {
      index = thisMatch.namedGroup("index")!.toInt();
      basename = thisMatch.namedGroup("basename")!;
      final extension = thisMatch.namedGroup("extension");
      ext = null == extension ? "" : ".$extension";
    }

    int loopCount;
    File newFile;

    for (loopCount = 0; loopCount < maxTries; loopCount++) {
      index++;
      newFile = factory(
        lib_path.join(
          getDirname(pathContext: pathContext),
          "$basename ($index)$ext",
        ),
      );
      if (!newFile.existsSync()) return newFile;
    }

    throw OutOfTriesException(
      tries: loopCount,
      message: "Could not create a new file with the same name "
          "as '$getName' in the directory '${parent.path}'",
    );
  }
}

extension MyUtilityExtensionFileRecreate on File {
  /// Recursively deletes this [File], if it exists
  /// and then recursively creates it again.
  ///
  /// See [recreateSync].
  Future<void> recreate() async {
    if (await exists()) {
      await delete(recursive: true);
    }

    await create(recursive: true);
  }
}

extension MyUtilityExtensionFileRecreateSync on File {
  /// Synchronously and recursively deletes this [File], if it exists
  /// and then recursively creates it again.
  ///
  /// See [recreate].
  void recreateSync() {
    if (existsSync()) {
      deleteSync(recursive: true);
    }

    createSync(recursive: true);
  }
}

extension MyUtilityExtensionFileResolveUri on File {
  Future<File> resolveUri({
    FileFactory factory = File.new,
  }) async {
    final file = factory(preparePathForResolve(this));
    return factory(await file.resolveSymbolicLinks());
  }
}

extension MyUtilityExtensionFileResolveUriSync on File {
  File resolveUriSync({
    FileFactory factory = File.new,
  }) {
    final file = factory(preparePathForResolve(this));
    return factory(file.resolveSymbolicLinksSync());
  }
}
