extension MyUtilityExtensionStringCamelOrPascalToSnake on String {
  /// Converts this string from camel-case or pascal-case to snake-case.
  ///
  /// ```dart
  /// void foo() {
  ///   print('abcDef'.camelToSnake()); // 'abc_def'
  ///   print('AbcDef'.camelToSnake()); // 'abc_def'
  /// }
  /// ```
  String camelOrPascalToSnake() {
    // See https://stackoverflow.com/a/1176023
    return replaceAllMapped(
      RegExp("(?<!^)(?=[A-Z])"),
      (m) => "_${m.group(0)!}",
    ).toLowerCase();
  }
}

extension MyUtilityExtensionStringCapitalize on String {
  /// Returns a copy of this string having its first letter uppercased and the
  /// rest lowercased.
  ///
  /// ```dart
  /// void foo() {
  ///   print('abcd'.capitalize()); // 'Abcd'
  ///   print(''.capitalize());     // ''
  ///   print('Abcd'.capitalize()); // 'Abcd'
  ///   print('ABCD'.capitalize()); // 'Abcd'
  /// }
  /// ```
  String capitalize() {
    switch (length) {
      case 0:
        return this;
      case 1:
        return toUpperCase();
      default:
        return substring(0, 1).toUpperCase() + substring(1).toLowerCase();
    }
  }
}

extension MyUtilityExtensionStringDecapitalize on String {
  /// Returns a copy of this string having its first letter lowercased.
  ///
  /// This function does not change the case of any letters other than the
  /// first one, as opposed to [MyUtilityExtensionStringCapitalize.capitalize].
  ///
  /// ```dart
  /// void foo() {
  ///   print('abcd'.capitalize()); // 'abcd'
  ///   print(''.capitalize());     // ''
  ///   print('Abcd'.capitalize()); // 'abcd'
  ///   print('ABCD'.capitalize()); // 'aBCD'
  /// }
  /// ```
  String decapitalize() {
    switch (length) {
      case 0:
        return this;
      case 1:
        return toLowerCase();
      default:
        return substring(0, 1).toLowerCase() + substring(1);
    }
  }
}

extension MyUtilityExtensionStringSnakeToCamel on String {
  /// Converts this string from snake-case to camel-case.
  ///
  /// ```dart
  /// void foo() {
  ///   print('abc_def'.snakeToCamel()); // 'abcDef'
  ///   print('ABC_DEF'.snakeToCamel()); // 'abcDef'
  ///   print('ABC'.snakeToCamel());     // 'abc'
  ///   print('abc'.snakeToCamel());     // 'abc'
  /// }
  /// ```
  String snakeToCamel() => snakeToPascal().decapitalize();
}

extension MyUtilityExtensionStringSnakeToPascal on String {
  /// Converts this string from snake-case to pascal-case.
  ///
  /// ```dart
  /// void foo() {
  ///   print('abc_def'.snakeToPascal()); // 'AbcDef'
  ///   print('ABC_DEF'.snakeToPascal()); // 'AbcDef'
  ///   print('ABC'.snakeToPascal());     // 'Abc'
  ///   print('abc'.snakeToPascal());     // 'Abc'
  /// }
  /// ```
  String snakeToPascal() {
    // See https://stackoverflow.com/a/1176023
    return split("_").map((word) => word.capitalize()).join();
  }
}

extension MyUtilityExtensionStringSplitLine on String {
  /// Split this [String], that is a single long line, without any linebreaks
  /// into multiple [String]s with a maximum length of [maxLength].
  ///
  /// The length of a line may be more than [maxLength], if the long line has
  /// no whitespaces that allow it to be split into multiple lines.
  ///
  /// This line is split where a whitespace is.
  List<String> splitLine(int maxLength) {
    return split("\n")
        .map((e) => e._splitLineImpl(maxLength))
        .expand((element) => element)
        .toList();
  }

  List<String> _splitLineImpl(int maxLength) {
    assert(!contains("\n"));

    if (length <= maxLength) return [this];

    const whitespace = 32;
    final lines = <String>[];

    final end = length;
    var sliceStart = 0;

    for (var i = maxLength; i < end; i += maxLength) {
      // Loop until [i] is the index of a whitespace, or if [i] is equal to the
      // end (length) of this string.
      while (codeUnitAt(i) != whitespace) {
        i++;
        if (i == end) break;
      }

      // If [i] is greater than [maxLength], try to navigate backward to find
      // a whitespace.
      if (i - sliceStart > maxLength) {
        for (var j = i - 1; j > sliceStart; j--) {
          if (codeUnitAt(j) != whitespace) continue;

          // Found a whitespace before [i] -> update [i] and exit for-loop.
          i = j;
          break;
        }
      }

      lines.add(substring(sliceStart, i).trim());
      sliceStart = i + 1;
    }

    if (sliceStart < end) {
      lines.add(substring(sliceStart, end).trim());
    }

    return lines;
  }
}
