import "../math/double_with_tolerance.dart";

extension UtilityExtensionDoubleWithTolerance on double {
  DoubleWithTolerance withTolerance({
    double epsilon = DoubleWithTolerance.precisionErrorTolerance,
  }) {
    return DoubleWithTolerance(this, epsilon: epsilon);
  }
}
