import "dart:async";

import "package:path/path.dart" as lib_path;
import "package:universal_io/io.dart";

import "../extensions.dart";
import "../src/get_unique_file.dart" as get_unique_file_impl;
import "../src/io_typedefs.dart";
import "../src/utils.dart";

extension MyUtilityExtensionDirectoryContains on Directory {
  /// Checks if this directory contains the [entity].
  ///
  /// The [entity] can be a [File] or a [Directory].
  /// If [recursive] is `true`, it checks the subdirectories too.
  ///
  /// Returns a `Future<bool>` holding the value.
  ///
  /// For the sync method, see [containsSync].
  Future<bool> contains(
    FileSystemEntity entity, {
    bool recursive = false,
    FileSystemEntityPathComparator identical = FileSystemEntity.identicalSync,
  }) async {
    final entities = list(recursive: recursive);
    return entities.any((element) => identical(entity.path, element.path));
  }
}

extension MyUtilityExtensionDirectoryContainsSync on Directory {
  /// Checks if this directory contains the [entity].
  ///
  /// The [entity] can be a [File] or a [Directory].
  /// If [recursive] is `true`, it checks the subdirectories too.
  ///
  /// Returns a [bool].
  ///
  /// For the async method, see [contains].
  bool containsSync(
    FileSystemEntity entity, {
    bool recursive = false,
    FileSystemEntityPathComparator identical = FileSystemEntity.identicalSync,
  }) {
    final entities = listSync(recursive: recursive);
    return entities.any((element) => identical(entity.path, element.path));
  }
}

extension MyUtilityExtensionDirectoryCopyRecursively on Directory {
  /// Copies all of the files in this directory to [target].
  ///
  /// This is similar to `cp -R <from> <to>`:
  /// * Symlinks are supported.
  /// * Existing files are over-written, if any.
  /// * If [target] is within `this`, throws [ArgumentError].
  /// * If `this` and [target] are canonically the same, no operation occurs.
  ///
  /// For the sync version see [copyRecursivelySync]
  Future<void> copyRecursively(
    Directory target, {
    bool followLinks = true,
    LinkFactory linkFactory = Link.new,
    FileFactory fileFactory = File.new,
    DirectoryFactory dirFactory = Directory.new,
    lib_path.Context? pathContext,
  }) async {
    pathContext ??= lib_path.context;

    if (pathContext.canonicalize(path) == pathContext.canonicalize(target.path))
      return;

    if (pathContext.isWithin(path, target.path))
      throw ArgumentError("Cannot copy $path to ${target.path}");

    await target.create(recursive: true);

    await for (final entity
        in list(recursive: true, followLinks: followLinks)) {
      final copyTo = pathContext.join(
        target.path,
        pathContext.relative(entity.path, from: path),
      );

      if (entity is Directory) {
        await dirFactory(copyTo).create(recursive: true);
      } else if (entity is File) {
        await fileFactory(entity.path).copy(copyTo);
      } else if (entity is Link) {
        final linkTarget = await entity.target();
        await linkFactory(copyTo).create(linkTarget, recursive: true);
      }
    }
  }
}

extension MyUtilityExtensionDirectoryCopyRecursivelySync on Directory {
  /// Copies all of the files in this directory to [target].
  ///
  /// This is similar to `cp -R <from> <to>`:
  /// * Symlinks are supported.
  /// * Existing files are over-written, if any.
  /// * If [target] is within `this`, throws [ArgumentError].
  /// * If `this` and [target] are canonically the same, no operation occurs.
  ///
  /// For the async version see [copyRecursively]
  void copyRecursivelySync(
    Directory target, {
    bool followLinks = true,
    LinkFactory linkFactory = Link.new,
    FileFactory fileFactory = File.new,
    DirectoryFactory dirFactory = Directory.new,
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;

    if (pathContext.canonicalize(path) == pathContext.canonicalize(target.path))
      return;

    if (pathContext.isWithin(path, target.path))
      throw ArgumentError("Cannot copy $path to ${target.path}");

    target.createSync(recursive: true);

    for (final file in listSync(recursive: true, followLinks: followLinks)) {
      final copyTo = pathContext.join(
        target.path,
        pathContext.relative(file.path, from: path),
      );
      if (file is Directory) {
        dirFactory(copyTo).createSync(recursive: true);
      } else if (file is File) {
        fileFactory(file.path).copySync(copyTo);
      } else if (file is Link) {
        final linkTarget = file.targetSync();
        linkFactory(copyTo).createSync(linkTarget, recursive: true);
      }
    }
  }
}

extension MyUtilityExtensionDirectoryCopyRecursivelyWithProgress on Directory {
  /// Copies all of the files in this directory to [target].
  ///
  /// This is similar to `cp -R <from> <to>`:
  /// * Symlinks are supported.
  /// * Existing files are over-written, if any.
  /// * If [target] is within `this`, throws [ArgumentError].
  /// * If `this` and [target] are canonically the same, no operation occurs.
  Future<void> copyRecursivelyWithProgress(
    Directory target,
    FileSystemEntityCopyProgressCb onProgress, {
    bool followLinks = true,
    LinkFactory linkFactory = Link.new,
    FileFactory fileFactory = File.new,
    DirectoryFactory dirFactory = Directory.new,
    lib_path.Context? pathContext,
  }) async {
    pathContext ??= lib_path.context;

    if (pathContext.canonicalize(path) == pathContext.canonicalize(target.path))
      return;

    if (pathContext.isWithin(path, target.path))
      throw ArgumentError("Cannot copy $path to ${target.path}");

    await target.create(recursive: true);

    final totalBytes = await length();
    var copiedBytes = 0;

    await for (final file in list(recursive: true, followLinks: followLinks)) {
      final copyTo = pathContext.join(
        target.path,
        pathContext.relative(file.path, from: path),
      );

      if (file is Directory) {
        await dirFactory(copyTo).create(recursive: true);
      } else if (file is File) {
        var lastProgress = 0;
        await fileFactory(file.path).copyWithProgress(
          copyTo,
          (_, progress) {
            copiedBytes += progress - lastProgress;
            lastProgress = progress;
            onProgress(totalBytes, copiedBytes);
          },
        );
      } else if (file is Link) {
        final linkTarget = await file.target();
        await linkFactory(copyTo).create(linkTarget, recursive: true);
      }
    }
  }
}

extension MyUtilityExtensionDirectoryDirectory on Directory {
  /// Returns a directory within the [Directory]
  ///
  /// ```dart
  /// Directory androidDir = Directory('flutter-app/android');
  /// Directory mainSrc = androidDir.directory("app/src/main");
  /// ```
  Directory directory(
    String dirPath, {
    lib_path.Context? pathContext,
    DirectoryFactory dirFactory = Directory.new,
  }) {
    pathContext ??= lib_path.context;
    final newPath = pathContext.join(absolute.path, dirPath);
    return dirFactory(newPath);
  }
}

extension MyUtilityExtensionDirectoryFile on Directory {
  /// Returns a [File] within the [Directory]
  ///
  /// ```dart
  /// Directory androidDir = Directory('flutter-app/android');
  /// File manifestFile = androidDir.file("app/src/main/AndroidManifest.xml");
  /// ```
  File file(
    String filePath, {
    lib_path.Context? pathContext,
    FileFactory fileFactory = File.new,
  }) {
    pathContext ??= lib_path.context;
    final newPath = pathContext.join(absolute.path, filePath);
    return fileFactory(newPath);
  }
}

extension MyUtilityExtensionDirectoryGetUniqueFile on Directory {
  /// Get a new random and unique filename in this [Directory], and create a
  /// new [File] with that name.
  ///
  /// {@macro my_utility.src.get_unique_file.getUniqueFilename}
  ///
  /// See also [getUniqueFileSync]
  Future<File> getUniqueFile({
    int length = 30,
    int? maxTries,
    String? prefix,
    String? extension,
    lib_path.Context? pathContext,
    FileFactory factory = File.new,
  }) async {
    pathContext ??= lib_path.context;

    final names = await get_unique_file_impl.getNamesInDir(
      directory: this,
      pathContext: pathContext,
    );

    return get_unique_file_impl.getUniqueFile(
      currentDir: this,
      length: length,
      maxTries: maxTries,
      factory: factory,
      namesInDir: names,
      pathContext: pathContext,
      extension: extension,
      prefix: prefix,
    );
  }
}

extension MyUtilityExtensionDirectoryGetUniqueFileSync on Directory {
  /// Get a new random and unique filename in this [Directory], and create a
  /// new [File] with that name.
  ///
  /// {@macro my_utility.src.get_unique_file.getUniqueFilename}
  ///
  /// See also [getUniqueFile]
  File getUniqueFileSync({
    int length = 30,
    int? maxTries,
    String? prefix,
    String? extension,
    lib_path.Context? pathContext,
    FileFactory factory = File.new,
  }) {
    pathContext ??= lib_path.context;

    final names = get_unique_file_impl.getNamesInDirSync(
      directory: this,
      pathContext: pathContext,
    );

    return get_unique_file_impl.getUniqueFile(
      currentDir: this,
      length: length,
      maxTries: maxTries,
      factory: factory,
      namesInDir: names,
      pathContext: pathContext,
      extension: extension,
      prefix: prefix,
    );
  }
}

extension MyUtilityExtensionDirectoryGetUniqueFilename on Directory {
  /// Get a new random and unique filename in this [Directory].
  ///
  /// {@macro my_utility.src.get_unique_file.getUniqueFilename}
  ///
  /// See also [getUniqueFilenameSync]
  Future<String> getUniqueFilename({
    int length = 30,
    int? maxTries,
    String? prefix,
    String? extension,
    lib_path.Context? pathContext,
  }) async {
    pathContext ??= lib_path.context;

    final names = await get_unique_file_impl.getNamesInDir(
      directory: this,
      pathContext: pathContext,
    );

    return get_unique_file_impl.getUniqueFilename(
      currentDir: this,
      length: length,
      maxTries: maxTries,
      namesInDir: names,
      extension: extension,
      prefix: prefix,
    );
  }
}

extension MyUtilityExtensionDirectoryGetUniqueFilenameSync on Directory {
  /// Get a new random and unique filename in this [Directory].
  ///
  /// {@macro my_utility.src.get_unique_file.getUniqueFilename}
  ///
  /// See also [getUniqueFilename]
  String getUniqueFilenameSync({
    int length = 30,
    int? maxTries,
    String? prefix,
    String? extension,
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;

    final names = get_unique_file_impl.getNamesInDirSync(
      directory: this,
      pathContext: pathContext,
    );

    return get_unique_file_impl.getUniqueFilename(
      currentDir: this,
      length: length,
      maxTries: maxTries,
      namesInDir: names,
      extension: extension,
      prefix: prefix,
    );
  }
}

extension MyUtilityExtensionDirectoryLength on Directory {
  /// The length of all files in this [Directory].
  ///
  /// Returns a `Future<int>` that completes with the length
  /// of all [File]s in this [Directory] in bytes.
  Future<int> length({
    bool followLinks = true,
  }) async {
    var len = 0;

    await for (final item in list(recursive: true, followLinks: followLinks)) {
      if (item is! File) continue;
      len += await item.length();
    }

    return len;
  }
}

extension MyUtilityExtensionDirectoryLengthSync on Directory {
  /// The length of all files in this [Directory] provided
  /// synchronously.
  ///
  /// Throws a [FileSystemException] if the operation fails.
  int lengthSync({
    bool followLinks = true,
  }) {
    var len = 0;

    for (final item in listSync(recursive: true, followLinks: followLinks)) {
      if (item is! File) continue;
      len += item.lengthSync();
    }

    return len;
  }
}

extension MyUtilityExtensionDirectoryRecreate on Directory {
  /// Recursively deletes this [Directory], if it exists
  /// and then recursively creates it again.
  Future<void> recreate() async {
    if (await exists()) {
      await delete(recursive: true);
    }

    await create(recursive: true);
  }
}

extension MyUtilityExtensionDirectoryRecreateSync on Directory {
  /// Synchronously and recursively deletes this [Directory], if it exists
  /// and then recursively creates it again.
  void recreateSync() {
    if (existsSync()) {
      deleteSync(recursive: true);
    }

    createSync(recursive: true);
  }
}

extension MyUtilityExtensionDirectoryResolveUri on Directory {
  Future<Directory> resolveUri({
    DirectoryFactory factory = Directory.new,
  }) async {
    final dir = factory(preparePathForResolve(this));
    return factory(await dir.resolveSymbolicLinks());
  }
}

extension MyUtilityExtensionDirectoryResolveUriSync on Directory {
  Directory resolveUriSync({
    DirectoryFactory factory = Directory.new,
  }) {
    final dir = factory(preparePathForResolve(this));
    return factory(dir.resolveSymbolicLinksSync());
  }
}
