import "dart:convert";

extension MyUtilityExtensionMapInverted<K, V> on Map<K, V> {
  /// Returns a new [Map], where the keys are the values of this [Map]
  /// and the values are the keys of this [Map].
  Map<V, K> inverted() => map((key, value) => MapEntry(value, key));
}

extension MyUtilityExtensionMapToPrettyString<K, V> on Map<K, V> {
  String toPrettyString() {
    Object? toEncodable(Object? object) {
      return object?.toString();
    }

    final encoder = JsonEncoder.withIndent("  ", toEncodable);
    return encoder.convert(this);
  }
}
