import "package:path/path.dart" as lib_path;
import "package:universal_io/io.dart";

extension MyUtilityExtensionFileSystemEntityAbsolutePosixPath
    on FileSystemEntity {
  String get absolutePosixPath {
    var res = lib_path.posix.fromUri(absolute.uri);

    if (Platform.isWindows) {
      // An absolute path on windows starts with a drive letter like "C:\Users\peter"
      // converting that to a posix path yield "/C:/Users/peter", but the '/' before
      // the drive letter causes errors, so we need to remove that.
      res = res.substring(1);
    }

    return res;
  }
}

extension MyUtilityExtensionFileSystemEntityGetDirname on FileSystemEntity {
  /// Gets the part of [path] before the last separator.
  /// ```dart
  /// File('path/to/foo.dart').getDirname(); // -> 'path/to'
  /// Directory('path/to').getDirname();     // -> 'path'
  /// ```
  ///
  /// Trailing separators are ignored.
  /// ```dart
  /// Directory('path/to/').getDirname(); // -> 'path'
  /// ```
  ///
  /// If an absolute path contains no directories, only a root, then the root
  /// is returned.
  /// ```dart
  /// Directory('/').getDirname();   // -> '/' (posix)
  /// Directory(r'c:\').getDirname(); // -> 'c:\' (windows)
  /// ```
  ///
  /// If a relative path has no directories, then '.' is returned.
  /// ```dart
  /// Directory('foo').getDirname();  // -> '.'
  /// Directory('').getDirname();  // -> '.'
  /// ```
  String getDirname({
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;
    return pathContext.dirname(path);
  }
}

extension MyUtilityExtensionFileSystemEntityGetName on FileSystemEntity {
  /// Gets the part of [path] after the last separator.
  /// ```dart
  /// File('path/to/foo.dart').getName(); // -> 'foo.dart'
  /// Directory('path/to').getName();          // -> 'to'
  /// ```
  ///
  /// Trailing separators are ignored.
  /// ```dart
  /// Directory('path/to/').getName(); // -> 'to'
  /// ```
  String getName({
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;
    return pathContext.basename(path);
  }
}

extension MyUtilityExtensionFileSystemEntityGetNameWithoutExtension
    on FileSystemEntity {
  /// Gets the part of [path] after the last separator, and without any trailing
  /// file extension.
  /// ```dart
  /// File('path/to/foo.dart').getNameWithoutExtension(); // -> 'foo'
  /// ```
  ///
  /// Trailing separators are ignored.
  /// ```dart
  /// File('path/to/foo.dart/').getNameWithoutExtension(); // -> 'foo'
  /// ```
  String getNameWithoutExtension({
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;
    return pathContext.basenameWithoutExtension(path);
  }
}

extension MyUtilityExtensionFileSystemEntityIsWithin on FileSystemEntity {
  /// Returns `true` if this entity is a path beneath `parent`, and `false`
  /// otherwise.
  ///
  /// ```dart
  /// Directory('/root/path/foo.dart').isWithin(Directory('/root/path')); // -> true
  ///
  /// Directory('/root/path').isWithin(Directory('/root/other')); // -> false
  ///
  /// Directory('/root/path').isWithin(Directory('/root/path')) // -> false
  /// ```
  bool isWithin(
    Directory parent, {
    lib_path.Context? pathContext,
  }) {
    pathContext ??= lib_path.context;
    return pathContext.isWithin(parent.path, path);
  }
}

extension MyUtilityExtensionFileSystemEntityDirname on FileSystemEntity {
  /// See [getDirname].
  ///
  /// {@macro my_utility.extensions.file_system_entity.current_ctx}
  String get dirname => getDirname();
}

extension MyUtilityExtensionFileSystemEntityName on FileSystemEntity {
  /// See [getName].
  ///
  /// {@macro my_utility.extensions.file_system_entity.current_ctx}
  String get name => getName();
}

extension MyUtilityExtensionFileSystemEntityNameWithoutExtension
    on FileSystemEntity {
  /// See [getNameWithoutExtension].
  ///
  /// {@macro my_utility.extensions.file_system_entity.current_ctx}
  String get nameWithoutExtension => getNameWithoutExtension();
}


/// {@template my_utility.extensions.file_system_entity.current_ctx}
/// Uses the path context of the current system.
/// {@endtemplate}
