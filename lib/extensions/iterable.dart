import "dart:math" as math;

extension MyUtilityExtensionIterableGetRandom<E> on Iterable<E> {
  E getRandom([math.Random? random]) {
    random ??= math.Random();
    return elementAt(random.nextInt(length));
  }
}

extension MyUtilityExtensionIterableOperatorSquareBrackets<E> on Iterable<E> {
  /// Syntactic sugar for calling [elementAt] with [index].
  E operator [](int index) => elementAt(index);
}

extension MyUtilityExtensionIterableToIndexKeyMap<E> on Iterable<E> {
  Map<int, E> toIndexKeyMap() => toMap(
        key: (index, _) => index,
        value: (_, element) => element,
      );
}

extension MyUtilityExtensionIterableToMap<E> on Iterable<E> {
  Map<K, V> toMap<K, V>({
    required K Function(int index, E element) key,
    required V Function(int index, E element) value,
  }) {
    final entries = <MapEntry<K, V>>[];
    final it = iterator..moveNext();

    for (var i = 0; i < length; i++, it.moveNext()) {
      entries.add(
        MapEntry(
          key(i, it.current),
          value(i, it.current),
        ),
      );
    }
    return Map.fromEntries(entries);
  }
}
