import "dart:async";

import "package:async/async.dart" show CancelableOperation;

import "../src/initial_delay_stream_transformer.dart";

typedef _FullErrorHandler = void Function(Object, StackTrace);
typedef _HalfErrorHandler = void Function(Object);

extension MyUtilityExtensionStreamListenAsFuture<T> on Stream<T> {
  /// Adds a subscription to this stream.
  ///
  /// [listenAsFuture] calls [listen] and registers extra handlers for the
  /// `onError` and `onDone` events so that the [Future] returned by
  /// [listenAsFuture] successfully completes, once this stream is done, or
  /// received an error (in case [cancelOnError] is true).
  ///
  /// On each data event from this stream, the [onData] handler
  /// is called. If [onData] is `null`, nothing happens.
  ///
  /// On errors from this stream, the [onError] handler is called with the
  /// error object and possibly a stack trace.
  ///
  /// The [onError] callback must be of type `void Function(Object error)` or
  /// `void Function(Object error, StackTrace)`.
  /// The function type determines whether [onError] is invoked with a stack
  /// trace argument.
  /// The stack trace argument may be [StackTrace.empty] if this stream received
  /// an error without a stack trace.
  /// Otherwise it is called with just the error object.
  ///
  /// If [cancelOnError] is `true`, the returned [Future] completes with the
  /// first error event, otherwise all error events are ignored by the
  /// [Future].
  ///
  /// If [cancelOnError] is `false`, or this stream emitted no error event, then
  /// [onDone] is called once this stream closes and sends the done event. If
  /// [onDone] is `null`, nothing happens.
  CancelableOperation<void> listenAsFuture(
    void Function(T event)? onData, {
    Function? onError,
    void Function()? onDone,
    bool cancelOnError = false,
  }) {
    // Validate onError callback.
    if (null != onError &&
        onError is! _FullErrorHandler &&
        onError is! _HalfErrorHandler)
      throw ArgumentError(
        "onError callback must take either an Object "
        "(the error), or both an Object (the error) and a StackTrace.",
      );

    final comp = Completer<void>();
    var hadError = false;

    void handleError(Object err, StackTrace st) {
      hadError = true;

      // Call user provided error handler. [onError] is either of type
      // [_FullErrorHandler], [_HalfErrorHandler] or null because the parameter
      // is checked at the start of the [listenAsFuture] function.
      if (onError is _FullErrorHandler) {
        onError(err, st);
      } else if (onError is _HalfErrorHandler) {
        onError(err);
      }

      if (cancelOnError) {
        comp.completeError(err, st);
      }
    }

    final sub = listen(
      onData,
      onError: handleError,
      onDone: comp.complete,
      cancelOnError: cancelOnError,
    );

    var isCanceled = false;
    return CancelableOperation<void>.fromFuture(
      () async {
        try {
          // Await here, because [listenAsFuture] should appear in the
          // stack trace, if an error occures.
          await comp.future;
        } finally {
          if (!isCanceled && (!cancelOnError || !hadError)) {
            onDone?.call();
          }
          await sub.cancel();
        }
      }(),
      onCancel: () async {
        isCanceled = true;
        await sub.cancel();
      },
    );
  }
}

extension MyUtilityExtensionStreamWithInitialDelay<T> on Stream<T> {
  /// If the time it takes for the first event to appear is less than
  /// [minOperationTime], then a [Future.delayed] is awaited for
  /// the remaining time. [withInitialDelay] only affects the `first event`
  /// of this [Stream]. The delay also affects any errors of this [Stream]. 
  ///
  /// The [Stream] returned by this method will take `at least` as much time as
  /// specified by [minOperationTime] to complete.
  ///
  /// When a subscription on the returned [Stream] is paused, then the internal
  /// stopwatch that is used to check if [minOperationTime] has already passed,
  /// will also be stopped. Resuming the subscription starts the watch again.
  Stream<T> withInitialDelay(
    Duration minOperationTime, {
    Duration threshold = const Duration(milliseconds: 50),
    @Deprecated(
      "Will be removed in version 2.0.0, use "
      "the 'threshold' parameter instead.",
    )
    int thresholdInMillis = 50,
  }) {
    // TODO(obemu): remove/change the asserts and if condition in v2.0.0

    // minOperationTime is less than 1 millisecond, therefore
    // we do not add a fake delay.
    if (minOperationTime.inMilliseconds < 1) return this;

    assert(minOperationTime < const Duration(minutes: 10));
    assert(minOperationTime.inMilliseconds >= thresholdInMillis);

    // Can be removed in v2.0.0, once [thresholdInMillis] is removed.
    var localThreshold = threshold;
    if (50 != thresholdInMillis) {
      if (const Duration(milliseconds: 50) != threshold)
        throw ArgumentError.value(
          thresholdInMillis,
          "thresholdInMillis",
          "Cannot provide a value for 'thresholdInMillis' and "
              "'threshold', you should only use the 'threshold' parameter",
        );

      localThreshold = Duration(milliseconds: thresholdInMillis);
    }

    return transform(
      InitialDelayStreamTransformer(
        minOperationTime: minOperationTime,
        threshold: localThreshold,
      ),
    );
  }
}
