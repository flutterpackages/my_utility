import "../io/file_stat_permission.dart";

extension MyUtilityExtensionFileStatPermissionCanExecute
    on Iterable<FileStatPermission> {
  bool get canExecute => contains(FileStatPermission.execute);
}

extension MyUtilityExtensionFileStatPermissionCanRead
    on Iterable<FileStatPermission> {
  bool get canRead => contains(FileStatPermission.read);
}

extension MyUtilityExtensionFileStatPermissionToOctalMode
    on Iterable<FileStatPermission> {
  String toOctalMode() {
    var val = 0;
    for (final item in this) {
      val |= item.mask;
    }
    return val.toRadixString(8);
  }
}

extension MyUtilityExtensionFileStatPermissionCanWrite
    on Iterable<FileStatPermission> {
  bool get canWrite => contains(FileStatPermission.write);
}

extension MyUtilityExtensionFileStatPermissionOperatorPipe
    on Iterable<FileStatPermission> {
  /// Returns a new [Set] of [FileStatPermission]s, with every
  /// [FileStatPermission] from this and [permission].
  Set<FileStatPermission> operator |(FileStatPermission permission) {
    return {...this, permission};
  }
}
