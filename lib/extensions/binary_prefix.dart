import "../data_units.dart";

// extension ... on num

extension MyUtilityExtensionNumBinaryPrefixToUnit on num {
  double toUnit(BinaryPrefix to, [BinaryPrefix from = BinaryPrefix.none]) {
    if (from == to) return toDouble();
    final withoutPrefix = from.factor.toDouble() * this;
    return withoutPrefix / to.factor.toDouble();
  }
}

extension MyUtilityExtensionNumBinaryPrefixToKibi on num {
  /// Convert this to the unit Kibibytes (`KiB`).
  double toKibi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.kibi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToMebi on num {
  /// Convert this to the unit Mebibytes (`MiB`).
  double toMebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.mebi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToGibi on num {
  /// Convert this to the unit Gibibytes (`GiB`).
  double toGibi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.gibi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToTebi on num {
  /// Convert this to the unit Tebibytes (`TiB`).
  double toTebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.tebi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToPebi on num {
  /// Convert this to the unit Pebibytes (`PiB`).
  double toPebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.pebi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToExbi on num {
  /// Convert this to the unit Exbibytes (`EiB`).
  double toExbi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.exbi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToZebi on num {
  /// Convert this to the unit Zebibytes (`ZiB`).
  double toZebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.zebi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToYobi on num {
  /// Convert this to the unit Yobibytes (`YiB`).
  double toYobi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.yobi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToRobi on num {
  /// Convert this to the unit Robibytes (`RiB`).
  double toRobi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.robi,
        from,
      );
}

extension MyUtilityExtensionNumBinaryPrefixToQuebi on num {
  /// Convert this to the unit Quebibytes (`QiB`).
  double toQuebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.quebi,
        from,
      );
}

// extension ... on BigInt

extension MyUtilityExtensionBigIntBinaryPrefixToUnit on BigInt {
  double toUnit(BinaryPrefix to, [BinaryPrefix from = BinaryPrefix.none]) {
    if (from == to) return toDouble();
    final withoutPrefix = from.factor * this;
    return withoutPrefix / to.factor;
  }
}

extension MyUtilityExtensionBigIntBinaryPrefixToKibi on BigInt {
  /// Convert this to the unit Kibibytes (`KiB`).
  double toKibi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.kibi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToMebi on BigInt {
  /// Convert this to the unit Mebibytes (`MiB`).
  double toMebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.mebi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToGibi on BigInt {
  /// Convert this to the unit Gibibytes (`GiB`).
  double toGibi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.gibi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToTebi on BigInt {
  /// Convert this to the unit Tebibytes (`TiB`).
  double toTebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.tebi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToPebi on BigInt {
  /// Convert this to the unit Pebibytes (`PiB`).
  double toPebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.pebi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToExbi on BigInt {
  /// Convert this to the unit Exbibytes (`EiB`).
  double toExbi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.exbi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToZebi on BigInt {
  /// Convert this to the unit Zebibytes (`ZiB`).
  double toZebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.zebi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToYobi on BigInt {
  /// Convert this to the unit Yobibytes (`YiB`).
  double toYobi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.yobi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToRobi on BigInt {
  /// Convert this to the unit Robibytes (`RiB`).
  double toRobi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.robi,
        from,
      );
}

extension MyUtilityExtensionBigIntBinaryPrefixToQuebi on BigInt {
  /// Convert this to the unit Quebibytes (`QiB`).
  double toQuebi([BinaryPrefix from = BinaryPrefix.none]) => toUnit(
        BinaryPrefix.quebi,
        from,
      );
}
