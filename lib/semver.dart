import "package:version/version.dart";

export "package:version/version.dart";

final class SemVer extends Version {
  SemVer(
    super.major,
    super.minor,
    super.patch, {
    super.preRelease,
    String? build,
  }) : super(build: build ?? "");

  factory SemVer._fromMatch(RegExpMatch match) {
    final major = int.parse(match.namedGroup("major")!);
    final minor = int.parse(match.namedGroup("minor")!);
    final patch = int.tryParse(match.namedGroup("patch") ?? "");
    final preRelease = match.namedGroup("pre_release");
    final build = match.namedGroup("build");
    return SemVer(
      major,
      minor,
      patch ?? 0,
      preRelease: null != preRelease ? [preRelease] : [],
      build: build,
    );
  }

  factory SemVer._fromSuper(Version version) {
    return SemVer(
      version.major,
      version.minor,
      version.patch,
      build: version.build,
      preRelease: version.preRelease,
    );
  }

  static final _regex = RegExp(
    r"(?<major>\d+)"
    r"\.(?<minor>\d+)"
    r"\.?(?<patch>\d+)?"
    r"(?:-(?<pre_release>[0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?"
    r"(?:\+(?<build>[0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?",
  );

  // ignore: prefer_constructors_over_static_methods
  static SemVer parse(String source) =>
      SemVer._fromSuper(Version.parse(source));

  static List<SemVer> find(String source) =>
      _regex.allMatches(source).map(SemVer._fromMatch).toList();
}
