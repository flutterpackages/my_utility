import "dart:mirrors";

const bool kIsMirrorsSupported = true;

Iterable<dynamic> findClassesOfType(Type key) sync* {
  final base = reflectClass(key);
  final libs = currentMirrorSystem().libraries.values;
  for (final lib in libs) {
    for (final item in lib.declarations.values) {
      if (item is! ClassMirror) continue;
      if (item.superclass != base) continue;
      yield item;
    }
  }
}
