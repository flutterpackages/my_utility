/// Whether or not the project has been compiled to JavaScript.
const bool kIsWeb = false;

/// The maximum value for an integer on the current platform.
const int kMaxInt = 0x7FFFFFFFFFFFFFFF;

/// The minimum value for an integer on the current platform.
const int kMinInt = 0x8000000000000000;

/// A list of integers where the value at `i` is the maximum value where `i`
/// is the number of used bytes in the integer.
///
/// This list differs depending on the current platform. For example, when
/// compiling to JavaScript the values `0xFFFFFFFFFFFFFF` and
/// `0xFFFFFFFFFFFFFFFF` are not included, because they cannot be represented
/// by an [int] (See how [int]s work in JavaScript).
const kMaxIntergersForBytes = <int>[
  0xFF,
  0xFFFF,
  0xFFFFFF,
  0xFFFFFFFF,
  0xFFFFFFFFFF,
  0xFFFFFFFFFFFF,
  0xFFFFFFFFFFFFFF,
  0xFFFFFFFFFFFFFFFF,
];
