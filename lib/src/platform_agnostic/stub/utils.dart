extension MyUtilityPlatformAgnosticUtilsExtensionInt on int {
  bool get isSafe =>
      throw UnsupportedError("The current platform is not supported");
}

extension MyUtilityPlatformAgnosticUtilsExtensionBigInt on BigInt {
  bool get isSafeInteger =>
      throw UnsupportedError("The current platform is not supported");

  BigInt checkIsSafeInteger([String? name]) =>
      throw UnsupportedError("The current platform is not supported");
}
