/// Whether or not `dart:mirrors` is supported on the current platform.
///
/// `dart:mirrors` is not supported, when compiling to JavaScript or compiling
/// to a native executable.
const bool kIsMirrorsSupported = false;

Iterable<dynamic> findClassesOfType(Type key) =>
    throw UnsupportedError("The current platform supports no mirror system");
