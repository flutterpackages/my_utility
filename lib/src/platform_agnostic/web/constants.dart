const bool kIsWeb = true;

const bool kIsMirrorSupported = false;

// For int limits see:
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number#number_encoding

// Value of `Number.MAX_SAFE_INTEGER` (2^53 - 1).
const int kMaxInt = 0x1FFFFFFFFFFFFF;

// Value of `Number.MIN_SAFE_INTEGER` (-2^53 + 1).
const int kMinInt = -0x1FFFFFFFFFFFFF;

const kMaxIntergersForBytes = <int>[
  0xFF,
  0xFFFF,
  0xFFFFFF,
  0xFFFFFFFF,
  0xFFFFFFFFFF,
  0xFFFFFFFFFFFF,
  // Cannot be represented in JS.
  //0xFFFFFFFFFFFFFF,
  //0xFFFFFFFFFFFFFFFF,
];
