import "utils.dart";

bool isUnsignedInteger(int value, int size) {
  final limit = switch (size) {
    1 => 0xFF,
    2 => 0xFFFF,
    3 => 0xFFFFFF,
    4 => 0xFFFFFFFF,
    5 => 0xFFFFFFFFFF,
    6 => 0xFFFFFFFFFFFF,
    7 || 8 => throw UnsupportedError(
        "Cannot check if an integer with size 7 (56 bit) or 8 (64 bit) is "
        "unsigned when compiling to JavaScript, because integers are "
        "restricted to values that can be represented exactly by "
        "double-precision floating point values. The available integer values "
        "only include integers up to 53 bits. See the documentation for the "
        "int class.\n"
        "Take a look at package 'fixnum', if you need to use integers with "
        "more than 53 bits.",
      ),
    _ => throw ArgumentError.value(
        size,
        "size",
        "The size must be between 1 and 8, both inclusive",
      )
  };

  final bigValue = BigInt.from(value).checkIsSafeInteger();
  final bigLimit = BigInt.from(limit);
  return BigInt.zero == (bigValue & ~bigLimit);
}
