import "dart:js_interop";

@JS("Number.isSafeInteger")
external JSBoolean _numberIsSafeInteger(JSNumber value);

@JS("Number")
external JSNumber _constructNumber(JSString value);

extension MyUtilityPlatformAgnosticUtilsExtensionInt on int {
  bool get isSafe => _numberIsSafeInteger(toJS).toDart;
}

extension MyUtilityPlatformAgnosticUtilsExtensionBigInt on BigInt {
  bool get isSafeInteger {
    final num = _constructNumber(toString().toJS);
    return _numberIsSafeInteger(num).toDart;
  }

  BigInt checkIsSafeInteger([String? name]) {
    if (isSafeInteger) return this;
    throw ArgumentError.value(
      this,
      name,
      "The number cannot be represented when compiling to JavaScript, because "
      "integers are restricted to values that can be represented exactly by "
      "double-precision floating point values. The available integer values "
      "only include integers up to 53 bits. See the documentation for the "
      "int class",
    );
  }
}
