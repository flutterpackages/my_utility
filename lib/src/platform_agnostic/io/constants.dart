const bool kIsWeb = false;

const bool kIsMirrorSupported = true;

// max value for 64 bit signed integer (2^63 - 1).
const int kMaxInt = 0x7FFFFFFFFFFFFFFF;

// min value for 64 bit signed integer (-2^63).
const int kMinInt = 0x8000000000000000;

const kMaxIntergersForBytes = <int>[
  0xFF,
  0xFFFF,
  0xFFFFFF,
  0xFFFFFFFF,
  0xFFFFFFFFFF,
  0xFFFFFFFFFFFF,
  0xFFFFFFFFFFFFFF,
  0xFFFFFFFFFFFFFFFF,
];
