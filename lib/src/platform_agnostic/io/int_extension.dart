bool isUnsignedInteger(int value, int size) {
  final limit = switch (size) {
    1 => 0xFF,
    2 => 0xFFFF,
    3 => 0xFFFFFF,
    4 => 0xFFFFFFFF,
    5 => 0xFFFFFFFFFF,
    6 => 0xFFFFFFFFFFFF,
    7 => 0xFFFFFFFFFFFFFF,
    8 => 0xFFFFFFFFFFFFFFFF,
    _ => throw ArgumentError.value(
        size,
        "size",
        "The size must be between 1 and 8, both inclusive",
      )
  };

  return 0 == (value & ~limit);
}

String toRadixStringAsUnsignedInteger(int value, int radix) {
  return BigInt.from(value).toUnsigned(64).toRadixString(radix);
}
