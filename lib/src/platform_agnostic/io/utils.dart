extension MyUtilityPlatformAgnosticUtilsExtensionInt on int {
  bool get isSafe => true;
}

extension MyUtilityPlatformAgnosticUtilsExtensionBigInt on BigInt {
  bool get isSafeInteger => 64 >= bitLength + 1;

  BigInt checkIsSafeInteger([String? name]) {
    if (isSafeInteger) return this;
    throw ArgumentError.value(
      this,
      name,
      "The number cannot be represented when compiling to native Dart VM, "
      "because integers are represented by 64 bit integers.",
    );
  }
}
