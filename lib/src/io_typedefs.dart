import "package:universal_io/io.dart";

/// Synchronously checks whether two paths refer to the same object in a
/// file system.
///
/// Comparing a link to its target should return `false`, as does comparing two
/// links that point to the same target. To check the target of a link, use
/// Link.target explicitly to fetch it. Directory links appearing inside a path
/// should be followed, though, to find the file system object.
///
/// Should throw an error if one of the paths points to an object that does not
/// exist.
typedef FileSystemEntityPathComparator = bool Function(
  String path1,
  String path2,
);

typedef FileSystemEntityTypeProvider = Future<FileSystemEntityType> Function(
  String path, {
  bool followLinks,
});

typedef FileFactory = File Function(String path);

typedef LinkFactory = Link Function(String path);

typedef DirectoryFactory = Directory Function(String path);

/// This is a callback that gets called each time some copy progress
/// is made for a [FileSystemEntity].
///
/// The [total] parameter is the total size in bytes of the [FileSystemEntity]
/// and the [progress] parameter is the amount of already copied bytes.
typedef FileSystemEntityCopyProgressCb = void Function(int total, int progress);
