import "dart:async";
import "dart:collection";

import "package:meta/meta.dart";

void _debugPrint(Object? obj) {
  assert(() {
    InitialDelayStreamTransformer.debugPrint?.call(obj);
    return true;
  }());
}

final class InitialDelayStreamTransformer<T>
    extends StreamTransformerBase<T, T> {
  InitialDelayStreamTransformer({
    required this.minOperationTime,
    required this.threshold,
  }) : assert(minOperationTime >= threshold) {
    if (minOperationTime.isNegative)
      throw ArgumentError.value(
        minOperationTime,
        "minOperationTime",
        "Must be a positive duration",
      );

    if (threshold.isNegative)
      throw ArgumentError.value(
        threshold,
        "threshold",
        "Must be a positive duration",
      );
  }

  final Duration minOperationTime;
  final Duration threshold;

  /// A function used by the [InitialDelayStreamTransformer] and its'
  /// implementation to print debugging information.
  ///
  /// This function will only be called in debug builds
  /// (i.e. `asserts` are enabled).
  ///
  /// If set to null, then nothing will be printed.
  /// The default value is null.
  @visibleForTesting
  static void Function(Object?)? debugPrint;

  @override
  Stream<T> bind(Stream<T> stream) =>
      _InitialDelayStreamController<T>(this, stream).stream;
}

//

final class _InitialDelayStreamController<T> {
  _InitialDelayStreamController(this._transformer, this._source)
      : _ctrl = StreamController(),
        _state = _stateInitial,
        _eventBuffer = Queue(),
        _firstEventWatch = Stopwatch(),
        _delayWatch = Stopwatch() {
    _ctrl.onListen = () {
      assert(0 == _state & _stateCanceled);
      assert(0 == _state & _stateSubscribed);

      _state |= _stateSubscribed;

      _debugPrint("onListen [state=$_stateAsBinaryString]");

      _firstEventWatch.start();
      _sourceSub = _source.listen(
        (e) => _onSourceEvent(_StreamValueEvent(e)),
        onError: (err, st) => _onSourceEvent(_StreamErrorEvent(err, st)),
        onDone: () => _onSourceEvent(_StreamDoneEvent()),
      );
    };

    _ctrl.onPause = () {
      assert(0 == _state & _stateCanceled);
      assert(0 != _state & _stateSubscribed);

      assert(0 == _state & _statePaused);
      _state |= _statePaused;

      _debugPrint("onPause [state=$_stateAsBinaryString]");

      if (0 == _state & _stateSourceEmittedFirstEvent) {
        _updateFirstEventDelay();
      } else if (0 != _state & _stateDelayIsWaiting) {
        _maybeStopDelayTimer();
      }
    };

    _ctrl.onResume = () {
      assert(0 == _state & _stateCanceled);
      assert(0 != _state & _stateSubscribed);

      assert(0 != _state & _statePaused);
      _state &= ~_statePaused;

      _debugPrint("onResume [state=$_stateAsBinaryString]");

      if (0 == _state & _stateSourceEmittedFirstEvent) {
        _firstEventWatch.start();
      } else if (0 != _state & _stateDelayIsWaiting ||
          0 == _state & _stateDelayHasAwaited) {
        if (!_maybeStartDelayTimer()) {
          _onAwaitedFirstEventDelay();
        }
      } else {
        _flushEventBuffer();
      }
    };

    _ctrl.onCancel = () async {
      assert(0 == _state & _stateCanceled);
      assert(
        0 != _state & _stateCtrlClosed || 0 != _state & _stateSubscribed,
        "Controller must be either closed or still subscribed",
      );

      _state |= _stateCanceled;
      _state &= ~_stateSubscribed;

      _debugPrint("onCancel [state=$_stateAsBinaryString]");

      await _close();
    };
  }

  /// The controller is in its initial state with no subscription.
  static const int _stateInitial = 0;

  /// The controller has a subscription, but hasn't been closed or canceled.
  static const int _stateSubscribed = 1 << 0;

  /// The subscription is canceled.
  static const int _stateCanceled = 1 << 1;

  /// The subscription is paused.
  static const int _statePaused = 1 << 2;

  // The following state relate to the controller, not the subscription.
  // If closed, adding more events is not allowed.
  // If executing an [addStream], new events are not allowed either, but will
  // be added by the stream.

  /// The controller is closed due to calling `_ctrl.close`.
  ///
  /// When the stream is closed, you can neither add new events nor add new
  /// listeners.
  static const int _stateCtrlClosed = 1 << 4;

  // The following state relate to the source stream, not the controller or
  // the subscription.

  /// [_source] emitted its' first value or error event.
  static const int _stateSourceEmittedFirstEvent = 1 << 8;

  /// [_source] emitted its' `done` event.
  static const int _stateSourceEmittedDone = 1 << 9;

  // The following state relate to the initial delay implementation.

  /// There is a need to wait until the first event from [_source] is forwarded
  /// to [_ctrl], because the value of `_transformer.minOperationTime` has not
  /// been reached yet.
  ///
  /// See [InitialDelayStreamTransformer.minOperationTime]
  static const int _stateDelayIsWaiting = 1 << 12;

  /// Indicates that [_onAwaitedFirstEventDelay] has been called.
  static const int _stateDelayHasAwaited = 1 << 13;

  /// The current state of this [_InitialDelayStreamController].
  int _state;

  /// Only used for loggin/debugging purposes.
  String get _stateAsBinaryString {
    const separator = " ";
    const minLen = 4 * 4;

    final res = StringBuffer("($_state) 0b");
    final str = _state.toRadixString(2);

    var zeroPrefixCount = minLen - str.length;
    if (zeroPrefixCount < 0) {
      zeroPrefixCount = 0;
    }

    for (var i = 0; i < zeroPrefixCount; i++) {
      if (i > 0 && i % 4 == 0) {
        res.write(separator);
      }
      res.write("0");
    }

    for (var i = 0; i < str.length; i++) {
      if ((i + zeroPrefixCount) % 4 == 0) {
        res.write(separator);
      }
      res.write(str[i]);
    }

    return res.toString();
  }

  /// The transformer holds all config parameters.
  final InitialDelayStreamTransformer<T> _transformer;

  /// The [StreamController] used to forward all events from [_source].
  final StreamController<T> _ctrl;

  final Stream<T> _source;
  late final StreamSubscription<T> _sourceSub;

  /// A list that holds the events from [_source] that were buffered, because
  /// the current [_state] was [_stateDelayIsWaiting].
  final Queue<_StreamEvent<T>> _eventBuffer;

  /// A [Stopwatch] that starts as soon as someone subscribes to [stream] and
  /// runs until the first event from [_source] is emitted.
  ///
  /// [_firstEventWatch] gets stopped and started accordingly to `_ctrl.onPause`
  /// and `_ctrl.onResume`, if [_stateSourceEmittedFirstEvent] has not been
  /// reached yet.
  final Stopwatch _firstEventWatch;

  /// The time needed to wait, until the first event from [_source] can be
  /// forwarded to [_ctrl].
  late Duration _firstEventDelay;

  /// A stopwatch to track how much time of [_firstEventDelay] has
  /// already passed.
  final Stopwatch _delayWatch;

  /// A timer that calls [_onAwaitedFirstEventDelay] after [_firstEventDelay]
  /// has passed.
  Timer? _delayTimer;

  /// The transformed stream with an initial delay.
  Stream<T> get stream => _ctrl.stream;

  /// Returns true, if [_delayTimer] has been started.
  ///
  /// [_firstEventDelay] is less than `_transformer.threshold`, if the
  /// timer has not been started.
  ///
  /// See [InitialDelayStreamTransformer.threshold].
  bool _maybeStartDelayTimer([bool isFirstStart = false]) {
    assert(0 == _state & _statePaused);
    assert(0 != _state & _stateSourceEmittedFirstEvent);

    // Do not wait if we are below the required threshold.
    if (_firstEventDelay < _transformer.threshold) {
      _debugPrint(
        "_maybeStartDelayTimer [state=$_stateAsBinaryString]: "
        "Not starting delay timer with _$_firstEventDelay",
      );
      return false;
    }

    _state |= _stateDelayIsWaiting;

    _debugPrint(
      "_maybeStartDelayTimer [state=$_stateAsBinaryString]: "
      "Starting delay timer with _$_firstEventDelay",
    );

    var delay = _firstEventDelay;

    // TODO(obemu): Maybe find a better fix for the following hack.
    if (isFirstStart &&
        _firstEventWatch.elapsed < const Duration(milliseconds: 100)) {
      // If the operation completed too fast on the first time the timer is
      // started, then we wait for [minOperationTime], because waiting for
      // [_firstEventDelay] in this case may not work (depends on the platform
      // and compilation type) so that we actually cause a delay of at least
      // [_transformer.minOperationTime].
      delay = _transformer.minOperationTime;
    }

    _delayWatch.start();
    _delayTimer = Timer(delay, _onAwaitedFirstEventDelay);

    return true;
  }

  void _maybeStopDelayTimer() {
    // Do nothing if timer is not running or if timer is null.
    if (!(_delayTimer?.isActive ?? false)) return;

    _debugPrint("Stopping delay timer");

    _delayWatch.stop();

    _delayTimer!.cancel();
    _delayTimer = null;

    // Update _firstEventDelay with the time we already waited.
    _firstEventDelay = _firstEventDelay - _delayWatch.elapsed;
    _delayWatch.reset();
  }

  /// Stops [_firstEventWatch] and updates [_firstEventDelay] accordingly.
  void _updateFirstEventDelay() {
    _firstEventWatch.stop();
    _firstEventDelay = _transformer.minOperationTime - _firstEventWatch.elapsed;
  }

  /// Called on any events from [_source].
  void _onSourceEvent(_StreamEvent<T> event) {
    assert(0 == _state & _stateCanceled);
    assert(0 != _state & _stateSubscribed);

    _debugPrint("_onSourceEvent [state=$_stateAsBinaryString]: $event");

    if (event is _StreamDoneEvent) {
      assert(0 == _state & _stateSourceEmittedDone);
      _state |= _stateSourceEmittedDone;
    }

    if (0 == _state & _stateSourceEmittedFirstEvent) {
      _state |= _stateSourceEmittedFirstEvent;

      if (0 == _state & _statePaused) {
        _updateFirstEventDelay();
        _maybeStartDelayTimer(true);
      }
    }

    if (0 != _state & _stateDelayIsWaiting || 0 != _state & _statePaused) {
      _bufferEvent(event);
      return;
    } else if (0 == _state & _stateDelayIsWaiting) {
      _forwardEvent(event);
      return;
    }

    // Should never arrive here.
    throw StateError("Cannot handle current state: $_stateAsBinaryString");
  }

  /// Called when the [_firstEventDelay] has been waited for.
  ///
  /// This method may never get called, if [_firstEventDelay] is less than
  /// `_transformer.threshold`.
  ///
  /// See [InitialDelayStreamTransformer.threshold]
  Future<void> _onAwaitedFirstEventDelay() async {
    assert(0 != _state & _stateSourceEmittedFirstEvent);
    assert(0 == _state & _stateDelayHasAwaited);

    _state |= _stateDelayHasAwaited;

    _debugPrint(
      "_onAwaitedFirstEventDelay before flushing [state=$_stateAsBinaryString]",
    );

    await _flushEventBuffer();

    assert(0 != _state & _stateDelayIsWaiting);
    _state &= ~_stateDelayIsWaiting;
  }

  Future<void> _flushEventBuffer() async {
    while (true) {
      // Stop flushing if controller got paused.
      if (0 != _state & _statePaused) return;

      // Stop flushing if there are no more elements.
      if (_eventBuffer.isEmpty) return;
      final e = _eventBuffer.removeFirst();

      // Enqueue a new event in Darts' event-queue. This must be done,
      // so that the user may pause their subscription after each time an
      // event is received by their listener.
      //
      // For an explanation of Darts' event-queue see
      // https://web.archive.org/web/20170704074724/https://webdev.dartlang.org/articles/performance/event-loop
      await Future(() {
        if (0 != _state & _statePaused && 0 != _state & _stateSubscribed) {
          _eventBuffer.addFirst(e);
          return;
        }

        _forwardEvent(e);
      });
    }
  }

  void _bufferEvent(_StreamEvent<T> event) {
    assert(
      0 != _state & _stateDelayIsWaiting || 0 != _state & _statePaused,
      "Must be waiting for initial delay, or the subscription "
      "must be paused to buffer events.",
    );
    _eventBuffer.add(event);
  }

  /// Synchronously forwards [event] to [_ctrl], if [_state] is not
  /// [_stateCtrlClosed] or [_stateCanceled].
  ///
  /// Calls [_close], if [event] is a [_StreamDoneEvent].
  void _forwardEvent(_StreamEvent<T> event) {
    _debugPrint("_forwardEvent [state=$_stateAsBinaryString]: $event");

    // Subscription got cancelled, no need to forward any events.
    if (0 != _state & _stateCanceled) return;

    assert(0 != _state & _stateSubscribed);
    assert(0 == _state & _statePaused);

    switch (event) {
      case _StreamValueEvent<T>(:final value):
        if (0 != _state & _stateCtrlClosed) return;
        _ctrl.add(value);
        break;

      case _StreamErrorEvent<T>(:final error, :final stackTrace):
        if (0 != _state & _stateCtrlClosed) return;
        _ctrl.addError(error, stackTrace);
        break;

      case _StreamDoneEvent<T>():
        if (0 != _state & _stateCtrlClosed) return;
        _close();
        break;
    }
  }

  /// Closes [_ctrl] and updates [_state] accordingly.
  Future<void> _close() async {
    if (0 != _state & _stateCtrlClosed) return;

    _debugPrint(
      "Starting to close controller [state=$_stateAsBinaryString]",
    );

    _delayTimer?.cancel();
    _delayTimer = null;

    if (0 != _state & _stateSubscribed) {
      _state &= ~_stateSubscribed;
      await _sourceSub.cancel();
    }

    assert(0 == _state & _stateCtrlClosed);
    _state |= _stateCtrlClosed;
    await _ctrl.close();

    _eventBuffer.clear();

    _debugPrint("Closed controller [state=$_stateAsBinaryString]");
  }
}

//

sealed class _StreamEvent<T> {
  _StreamEvent();
}

final class _StreamValueEvent<T> extends _StreamEvent<T> {
  _StreamValueEvent(this.value);

  final T value;

  @override
  String toString() => "_StreamValueEvent<$T>($value)";
}

final class _StreamErrorEvent<T> extends _StreamEvent<T> {
  _StreamErrorEvent(this.error, this.stackTrace);

  final Object error;
  final StackTrace? stackTrace;

  @override
  String toString() => "_StreamErrorEvent<$T>(...)";
}

final class _StreamDoneEvent<T> extends _StreamEvent<T> {
  _StreamDoneEvent();

  @override
  String toString() => "_StreamDoneEvent<T>()";
}
