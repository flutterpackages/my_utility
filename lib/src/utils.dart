import "package:universal_io/io.dart";
import "package:version/version.dart";

import "../dartx.dart";

export "platform_agnostic/stub/utils.dart"
    if (dart.library.io) "platform_agnostic/io/utils.dart"
    if (dart.library.js_interop) "platform_agnostic/web/utils.dart";

/// Prepare the path of this [FileSystemEntity] for use with the
/// [FileSystemEntity.resolveSymbolicLinks] or
/// [FileSystemEntity.resolveSymbolicLinksSync] method.
///
/// See [FileSystemEntity.resolveSymbolicLinksSync]
String preparePathForResolve(FileSystemEntity entity) {
  // The implementation is copied from the commend of
  // the resolveSymbolicLinksSync method.

  final resolved = Uri.parse(".").resolveUri(entity.absolute.uri);

  var uriPath = resolved.toFilePath();

  if (uriPath.isEmpty) {
    uriPath = ".";
  }

  return uriPath;
}

Version getElixirCliToolVersion(String rawVersion) {
  // Erlang/OTP 25 [erts-13.1.5] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [jit:ns]
  //
  // Elixir 1.14.0 (compiled with Erlang/OTP 24)
  final versionStr = rawVersion.trim().split("\n").third;

  // Get "major.minor.patch"
  final regex = RegExp(r"\d+\.\d+\.\d+");
  return Version.parse(regex.stringMatch(versionStr)!);
}
