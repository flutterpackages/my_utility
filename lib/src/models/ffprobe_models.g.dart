// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ffprobe_models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FFProbeDataModel _$FFProbeDataModelFromJson(Map<String, dynamic> json) =>
    FFProbeDataModel._(
      streams: const _FFProbeStreamModelListConverter()
          .fromJson(json['streams'] as List),
      format:
          FFProbeFormatModel._fromJson(json['format'] as Map<String, dynamic>),
    );

FFProbeFormatModel _$FFProbeFormatModelFromJson(Map<String, dynamic> json) =>
    FFProbeFormatModel._(
      filename: json['filename'] as String,
      nbStreams: (json['nb_streams'] as num).toInt(),
      nbPrograms: (json['nb_programs'] as num).toInt(),
      formatName: json['format_name'] as String,
      formatLongName: json['format_long_name'] as String,
      startTime: const _FFProbeModelDurationConverter()
          .fromJson(json['start_time'] as String),
      duration: const _FFProbeModelDurationConverter()
          .fromJson(json['duration'] as String),
      size: _parseInt(json['size'] as String),
      bitRate: _parseInt(json['bit_rate'] as String),
      probeScore: (json['probe_score'] as num).toInt(),
      tags: FFProbeFormatTagsModel._fromJson(
          json['tags'] as Map<String, dynamic>),
    );

FFProbeFormatTagsModel _$FFProbeFormatTagsModelFromJson(
        Map<String, dynamic> json) =>
    FFProbeFormatTagsModel._(
      encoder: json['encoder'] as String?,
    );

FFProbeStreamModel _$FFProbeStreamModelFromJson(Map<String, dynamic> json) =>
    FFProbeStreamModel._(
      index: (json['index'] as num).toInt(),
      codecName: json['codec_name'] as String,
      codecLongName: json['codec_long_name'] as String,
      profile: json['profile'] as String?,
      codecType: json['codec_type'] as String,
      codecTagString: json['codec_tag_string'] as String,
      codecTag: json['codec_tag'] as String,
      rFrameRate: json['r_frame_rate'] as String,
      avgFrameRate: json['avg_frame_rate'] as String,
      timeBase: json['time_base'] as String,
      startPts: (json['start_pts'] as num).toInt(),
      startTime: const _FFProbeModelDurationConverter()
          .fromJson(json['start_time'] as String),
      durationTs: (json['duration_ts'] as num?)?.toInt(),
      duration: _$JsonConverterFromJson<String, Duration>(
          json['duration'], const _FFProbeModelDurationConverter().fromJson),
      bitRate: _parseNullableInt(json['bit_rate'] as String?),
      nbFrames: json['nb_frames'] as String?,
      disposition: FFProbeStreamDispositionModel._fromJson(
          json['disposition'] as Map<String, dynamic>),
      tags: FFProbeStreamTagsModel._fromJson(
          json['tags'] as Map<String, dynamic>),
      extradataSize: (json['extradata_size'] as num?)?.toInt(),
    );

Value? _$JsonConverterFromJson<Json, Value>(
  Object? json,
  Value? Function(Json json) fromJson,
) =>
    json == null ? null : fromJson(json as Json);

FFProbeAudioStreamModel _$FFProbeAudioStreamModelFromJson(
        Map<String, dynamic> json) =>
    FFProbeAudioStreamModel._(
      sampleFmt: json['sample_fmt'] as String,
      sampleRate: _parseInt(json['sample_rate'] as String),
      channels: (json['channels'] as num).toInt(),
      channelLayout: json['channel_layout'] as String,
      bitsPerSample: (json['bits_per_sample'] as num).toInt(),
      index: (json['index'] as num).toInt(),
      codecName: json['codec_name'] as String,
      codecLongName: json['codec_long_name'] as String,
      profile: json['profile'] as String?,
      codecType: json['codec_type'] as String,
      codecTagString: json['codec_tag_string'] as String,
      codecTag: json['codec_tag'] as String,
      rFrameRate: json['r_frame_rate'] as String,
      avgFrameRate: json['avg_frame_rate'] as String,
      timeBase: json['time_base'] as String,
      startPts: (json['start_pts'] as num).toInt(),
      startTime: const _FFProbeModelDurationConverter()
          .fromJson(json['start_time'] as String),
      durationTs: (json['duration_ts'] as num?)?.toInt(),
      duration: _$JsonConverterFromJson<String, Duration>(
          json['duration'], const _FFProbeModelDurationConverter().fromJson),
      bitRate: _parseNullableInt(json['bit_rate'] as String?),
      nbFrames: json['nb_frames'] as String?,
      disposition: FFProbeStreamDispositionModel._fromJson(
          json['disposition'] as Map<String, dynamic>),
      tags: FFProbeStreamTagsModel._fromJson(
          json['tags'] as Map<String, dynamic>),
      extradataSize: (json['extradata_size'] as num?)?.toInt(),
    );

FFProbeVideoStreamModel _$FFProbeVideoStreamModelFromJson(
        Map<String, dynamic> json) =>
    FFProbeVideoStreamModel._(
      width: (json['width'] as num).toInt(),
      height: (json['height'] as num).toInt(),
      codedWidth: (json['coded_width'] as num).toInt(),
      codedHeight: (json['coded_height'] as num).toInt(),
      closedCaptions: (json['closed_captions'] as num).toInt(),
      hasBFrames: (json['has_b_frames'] as num).toInt(),
      sampleAspectRatio: json['sample_aspect_ratio'] as String,
      displayAspectRatio: json['display_aspect_ratio'] as String,
      pixFmt: json['pix_fmt'] as String,
      level: (json['level'] as num).toInt(),
      colorRange: json['color_range'] as String?,
      colorSpace: json['color_space'] as String?,
      colorTransfer: json['color_transfer'] as String?,
      colorPrimaries: json['color_primaries'] as String?,
      chromaLocation: json['chroma_location'] as String,
      refs: (json['refs'] as num).toInt(),
      isAvc: json['is_avc'] as String?,
      nalLengthSize: json['nal_length_size'] as String?,
      bitsPerRawSample: json['bits_per_raw_sample'] as String,
      index: (json['index'] as num).toInt(),
      codecName: json['codec_name'] as String,
      codecLongName: json['codec_long_name'] as String,
      profile: json['profile'] as String?,
      codecType: json['codec_type'] as String,
      codecTagString: json['codec_tag_string'] as String,
      codecTag: json['codec_tag'] as String,
      rFrameRate: json['r_frame_rate'] as String,
      avgFrameRate: json['avg_frame_rate'] as String,
      timeBase: json['time_base'] as String,
      startPts: (json['start_pts'] as num).toInt(),
      startTime: const _FFProbeModelDurationConverter()
          .fromJson(json['start_time'] as String),
      durationTs: (json['duration_ts'] as num?)?.toInt(),
      duration: _$JsonConverterFromJson<String, Duration>(
          json['duration'], const _FFProbeModelDurationConverter().fromJson),
      bitRate: _parseNullableInt(json['bit_rate'] as String?),
      nbFrames: json['nb_frames'] as String?,
      disposition: FFProbeStreamDispositionModel._fromJson(
          json['disposition'] as Map<String, dynamic>),
      tags: FFProbeStreamTagsModel._fromJson(
          json['tags'] as Map<String, dynamic>),
      extradataSize: (json['extradata_size'] as num?)?.toInt(),
    );

FFProbeStreamDispositionModel _$FFProbeStreamDispositionModelFromJson(
        Map<String, dynamic> json) =>
    FFProbeStreamDispositionModel._(
      defaultValue: (json['default'] as num).toInt(),
      dub: (json['dub'] as num).toInt(),
      original: (json['original'] as num).toInt(),
      comment: (json['comment'] as num).toInt(),
      lyrics: (json['lyrics'] as num).toInt(),
      karaoke: (json['karaoke'] as num).toInt(),
      forced: (json['forced'] as num).toInt(),
      hearingImpaired: (json['hearing_impaired'] as num).toInt(),
      visualImpaired: (json['visual_impaired'] as num).toInt(),
      cleanEffects: (json['clean_effects'] as num).toInt(),
      attachedPic: (json['attached_pic'] as num).toInt(),
      timedThumbnails: (json['timed_thumbnails'] as num).toInt(),
      captions: (json['captions'] as num).toInt(),
      descriptions: (json['descriptions'] as num).toInt(),
      metadata: (json['metadata'] as num).toInt(),
      dependent: (json['dependent'] as num).toInt(),
      stillImage: (json['still_image'] as num).toInt(),
    );

FFProbeStreamTagsModel _$FFProbeStreamTagsModelFromJson(
        Map<String, dynamic> json) =>
    FFProbeStreamTagsModel._(
      language: json['language'] as String?,
      handlerName: json['handler_name'] as String?,
      vendorId: json['vendor_id'] as String?,
    );
