// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'flutter_verison.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FlutterVersion _$FlutterVersionFromJson(Map<String, dynamic> json) =>
    FlutterVersion(
      frameworkVersion: const VersionJsonConverter()
          .fromJson(json['frameworkVersion'] as String),
      channel: json['channel'] as String,
      repositoryUrl: json['repositoryUrl'] as String,
      frameworkRevision: json['frameworkRevision'] as String,
      frameworkCommitDate: FlutterVersion._commitDateFromJson(
          json['frameworkCommitDate'] as String),
      engineRevision: json['engineRevision'] as String,
      dartSdkVersion: const VersionJsonConverter()
          .fromJson(json['dartSdkVersion'] as String),
      devToolsVersion: const VersionJsonConverter()
          .fromJson(json['devToolsVersion'] as String),
      flutterRoot: json['flutterRoot'] as String,
    );

Map<String, dynamic> _$FlutterVersionToJson(FlutterVersion instance) =>
    <String, dynamic>{
      'frameworkVersion':
          const VersionJsonConverter().toJson(instance.frameworkVersion),
      'channel': instance.channel,
      'repositoryUrl': instance.repositoryUrl,
      'frameworkRevision': instance.frameworkRevision,
      'frameworkCommitDate': instance.frameworkCommitDate.toIso8601String(),
      'engineRevision': instance.engineRevision,
      'dartSdkVersion':
          const VersionJsonConverter().toJson(instance.dartSdkVersion),
      'devToolsVersion':
          const VersionJsonConverter().toJson(instance.devToolsVersion),
      'flutterRoot': instance.flutterRoot,
    };
