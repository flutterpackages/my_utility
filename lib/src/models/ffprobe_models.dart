import "package:dartx/dartx.dart";
import "package:equatable/equatable.dart";
import "package:json_annotation/json_annotation.dart";

import "../../json_converters/json_list_converter.dart";

part "ffprobe_models.g.dart";

final class _FFProbeModel extends JsonSerializable {
  const _FFProbeModel()
      : super(
          constructor: "_",
          createToJson: false,
          createFactory: true,
          fieldRename: FieldRename.snake,
          // Set this to false for 2 reasons:
          //  1. FFProbeStreamTagsModel may contains custom metadata key, so
          //     it's practically impossible to know them all
          //
          //  2. All models may get extra metadata, because of some changes in
          //     the Youtube API and if that happens the entire logic breaks
          //     because unrecognized keys would trigger an error.
          disallowUnrecognizedKeys: false,
          converters: const [
            _FFProbeModelDurationConverter(),
            _FFProbeStreamModelListConverter(),
          ],
        );
}

int _parseInt(String json) => int.parse(json);

int? _parseNullableInt(String? json) {
  if (null == json) return null;
  return _parseInt(json);
}

//

@_FFProbeModel()
final class FFProbeDataModel extends Equatable {
  const FFProbeDataModel._({
    required this.streams,
    required this.format,
  });

  factory FFProbeDataModel.fromJson(Map<String, dynamic> json) =>
      _$FFProbeDataModelFromJson(json);

  final List<FFProbeStreamModel> streams;

  @JsonKey(fromJson: FFProbeFormatModel._fromJson)
  final FFProbeFormatModel format;

  @override
  List<Object?> get props => [streams, format];
}

// format

@_FFProbeModel()
final class FFProbeFormatModel extends Equatable {
  const FFProbeFormatModel._({
    required this.filename,
    required this.nbStreams,
    required this.nbPrograms,
    required this.formatName,
    required this.formatLongName,
    required this.startTime,
    required this.duration,
    required this.size,
    required this.bitRate,
    required this.probeScore,
    required this.tags,
  });

  factory FFProbeFormatModel._fromJson(Map<String, dynamic> json) =>
      _$FFProbeFormatModelFromJson(json);

  final String filename;

  final int nbStreams;

  final int nbPrograms;

  final String formatName;

  final String formatLongName;

  final Duration startTime;

  final Duration duration;

  @JsonKey(fromJson: _parseInt)
  final int size;

  @JsonKey(fromJson: _parseInt)
  final int bitRate;

  final int probeScore;

  @JsonKey(fromJson: FFProbeFormatTagsModel._fromJson)
  final FFProbeFormatTagsModel tags;

  @override
  List<Object?> get props => [
        filename,
        nbStreams,
        nbPrograms,
        formatName,
        formatLongName,
        startTime,
        duration,
        size,
        bitRate,
        probeScore,
        tags,
      ];
}

@_FFProbeModel()
final class FFProbeFormatTagsModel extends Equatable {
  const FFProbeFormatTagsModel._({
    required this.encoder,
  });

  factory FFProbeFormatTagsModel._fromJson(Map<String, dynamic> json) =>
      _$FFProbeFormatTagsModelFromJson(json);

  final String? encoder;

  @override
  List<Object?> get props => [encoder];
}

// streams

@_FFProbeModel()
final class FFProbeStreamModel extends Equatable {
  const FFProbeStreamModel._({
    required this.index,
    required this.codecName,
    required this.codecLongName,
    required this.profile,
    required this.codecType,
    required this.codecTagString,
    required this.codecTag,
    required this.rFrameRate,
    required this.avgFrameRate,
    required this.timeBase,
    required this.startPts,
    required this.startTime,
    required this.durationTs,
    required this.duration,
    required this.bitRate,
    required this.nbFrames,
    required this.disposition,
    required this.tags,
    required this.extradataSize,
  });

  factory FFProbeStreamModel._fromJson(Map<String, dynamic> json) {
    final codecType = json["codec_type"] as String;

    if ("audio" == codecType) return _$FFProbeAudioStreamModelFromJson(json);
    if ("video" == codecType) return _$FFProbeVideoStreamModelFromJson(json);

    throw UnimplementedError(
      "There is no FFProbeStreamModel for the "
      "codec type '$codecType'.",
    );
  }

  final int index;
  final String codecName;
  final String codecLongName;
  final String? profile;
  final String codecType;
  final String codecTagString;
  final String codecTag;
  final String rFrameRate;
  final String avgFrameRate;
  final String timeBase;
  final int startPts;

  final Duration startTime;

  final int? durationTs;

  final Duration? duration;

  /// Unit is `bit/s` (bit/second)
  @JsonKey(fromJson: _parseNullableInt)
  final int? bitRate;

  final String? nbFrames;

  final int? extradataSize;

  @JsonKey(fromJson: FFProbeStreamDispositionModel._fromJson)
  final FFProbeStreamDispositionModel disposition;

  @JsonKey(fromJson: FFProbeStreamTagsModel._fromJson)
  final FFProbeStreamTagsModel? tags;

  @override
  List<Object?> get props => [
        index,
        codecName,
        codecLongName,
        profile,
        codecType,
        codecTagString,
        codecTag,
        rFrameRate,
        avgFrameRate,
        timeBase,
        startPts,
        startTime,
        durationTs,
        duration,
        bitRate,
        nbFrames,
        disposition,
        tags,
        extradataSize,
      ];
}

@_FFProbeModel()
final class FFProbeAudioStreamModel extends FFProbeStreamModel {
  const FFProbeAudioStreamModel._({
    required this.sampleFmt,
    required this.sampleRate,
    required this.channels,
    required this.channelLayout,
    required this.bitsPerSample,
    required super.index,
    required super.codecName,
    required super.codecLongName,
    required super.profile,
    required super.codecType,
    required super.codecTagString,
    required super.codecTag,
    required super.rFrameRate,
    required super.avgFrameRate,
    required super.timeBase,
    required super.startPts,
    required super.startTime,
    required super.durationTs,
    required super.duration,
    required super.bitRate,
    required super.nbFrames,
    required super.disposition,
    required super.tags,
    required super.extradataSize,
  }) : super._();

  final String sampleFmt;

  /// Unit is `Hz`
  @JsonKey(fromJson: _parseInt)
  final int sampleRate;

  final int channels;

  final String channelLayout;

  final int bitsPerSample;

  @override
  List<Object?> get props => [
        ...super.props,
        sampleFmt,
        sampleRate,
        channels,
        channelLayout,
        bitsPerSample,
      ];
}

@_FFProbeModel()
final class FFProbeVideoStreamModel extends FFProbeStreamModel {
  const FFProbeVideoStreamModel._({
    required this.width,
    required this.height,
    required this.codedWidth,
    required this.codedHeight,
    required this.closedCaptions,
    required this.hasBFrames,
    required this.sampleAspectRatio,
    required this.displayAspectRatio,
    required this.pixFmt,
    required this.level,
    required this.colorRange,
    required this.colorSpace,
    required this.colorTransfer,
    required this.colorPrimaries,
    required this.chromaLocation,
    required this.refs,
    required this.isAvc,
    required this.nalLengthSize,
    required this.bitsPerRawSample,
    required super.index,
    required super.codecName,
    required super.codecLongName,
    required super.profile,
    required super.codecType,
    required super.codecTagString,
    required super.codecTag,
    required super.rFrameRate,
    required super.avgFrameRate,
    required super.timeBase,
    required super.startPts,
    required super.startTime,
    required super.durationTs,
    required super.duration,
    required super.bitRate,
    required super.nbFrames,
    required super.disposition,
    required super.tags,
    required super.extradataSize,
  }) : super._();

  final int width;
  final int height;
  final int codedWidth;
  final int codedHeight;
  final int closedCaptions;
  final int hasBFrames;
  final String sampleAspectRatio;
  final String displayAspectRatio;
  final String pixFmt;
  final int level;
  final String? colorRange;
  final String? colorSpace;
  final String? colorTransfer;
  final String? colorPrimaries;
  final String chromaLocation;
  final int refs;
  final String? isAvc;
  final String? nalLengthSize;
  final String bitsPerRawSample;

  @override
  List<Object?> get props => [
        ...super.props,
        width,
        height,
        codedWidth,
        codedHeight,
        closedCaptions,
        hasBFrames,
        sampleAspectRatio,
        displayAspectRatio,
        pixFmt,
        level,
        colorRange,
        colorSpace,
        colorTransfer,
        colorPrimaries,
        chromaLocation,
        refs,
        isAvc,
        nalLengthSize,
        bitsPerRawSample,
      ];
}

@_FFProbeModel()
final class FFProbeStreamDispositionModel extends Equatable {
  const FFProbeStreamDispositionModel._({
    required this.defaultValue,
    required this.dub,
    required this.original,
    required this.comment,
    required this.lyrics,
    required this.karaoke,
    required this.forced,
    required this.hearingImpaired,
    required this.visualImpaired,
    required this.cleanEffects,
    required this.attachedPic,
    required this.timedThumbnails,
    required this.captions,
    required this.descriptions,
    required this.metadata,
    required this.dependent,
    required this.stillImage,
  });

  factory FFProbeStreamDispositionModel._fromJson(Map<String, dynamic> json) =>
      _$FFProbeStreamDispositionModelFromJson(json);

  @JsonKey(name: "default")
  final int defaultValue;
  final int dub;
  final int original;
  final int comment;
  final int lyrics;
  final int karaoke;
  final int forced;
  final int hearingImpaired;
  final int visualImpaired;
  final int cleanEffects;
  final int attachedPic;
  final int timedThumbnails;
  final int captions;
  final int descriptions;
  final int metadata;
  final int dependent;
  final int stillImage;

  @override
  List<Object?> get props => [
        defaultValue,
        dub,
        original,
        comment,
        lyrics,
        karaoke,
        forced,
        hearingImpaired,
        visualImpaired,
        cleanEffects,
        attachedPic,
        timedThumbnails,
        captions,
        descriptions,
        metadata,
        dependent,
        stillImage,
      ];
}

@_FFProbeModel()
final class FFProbeStreamTagsModel extends Equatable {
  const FFProbeStreamTagsModel._({
    required this.language,
    required this.handlerName,
    required this.vendorId,
  });

  factory FFProbeStreamTagsModel._fromJson(Map<String, dynamic> json) =>
      _$FFProbeStreamTagsModelFromJson(json);

  final String? language;
  final String? handlerName;
  final String? vendorId;

  @override
  List<Object?> get props => [
        language,
        handlerName,
        vendorId,
      ];
}

//

final class _FFProbeStreamModelListConverter
    extends JsonListConverter<FFProbeStreamModel, Map<String, dynamic>> {
  const _FFProbeStreamModelListConverter();

  @override
  FFProbeStreamModel singleFromJson(Map<String, dynamic> json) =>
      FFProbeStreamModel._fromJson(json);

  @override
  Map<String, dynamic> singleToJson(FFProbeStreamModel object) =>
      throw UnsupportedError(
        "Converting a FFProbeStreamModel to json is not supported",
      );
}

final class _FFProbeModelDurationConverter
    extends JsonConverter<Duration, String> {
  const _FFProbeModelDurationConverter();

  static final _regex = RegExp(
    "(?<minus>-)?"
    r"(?<seconds>\d+)\."
    r"(?<milliseconds>\d{3})"
    r"(?<microseconds>\d{3})",
  );

  @override
  Duration fromJson(String json) {
    final match = _regex.firstMatch(json);
    if (null == match)
      throw FormatException(
        "The provided json data for a Duration must be "
        "in the format S.mmmuuu (e.g.: 0.000000, -123.549000)",
        json,
      );

    final isNegative = null != match.namedGroup("minus");

    final seconds = match.namedGroup("seconds")?.toInt();
    if (null == seconds)
      throw FormatException(
        "The provided data for the duration does not "
        "contain any data for the seconds",
        json,
      );

    final milliseconds = match.namedGroup("milliseconds")?.toInt();
    if (null == milliseconds)
      throw FormatException(
        "The provided data for the duration does not "
        "contain any data for the milliseconds",
        json,
      );

    final microseconds = match.namedGroup("microseconds")?.toInt();
    if (null == microseconds)
      throw FormatException(
        "The provided data for the duration does not "
        "contain any data for the microseconds",
        json,
      );

    final duration = Duration(
      seconds: seconds,
      milliseconds: milliseconds,
      microseconds: microseconds,
    );

    return isNegative ? -duration : duration;
  }

  @override
  String toJson(Duration object) {
    final res = object.inMicroseconds / 1000000;
    return res.toStringAsFixed(6);
  }
}
