import "package:json_annotation/json_annotation.dart";
import "package:version/version.dart";

import "../../json_converters/version.dart";

part "flutter_verison.g.dart";

@JsonSerializable(
  disallowUnrecognizedKeys: false,
  converters: [
    VersionJsonConverter(),
  ],
)
final class FlutterVersion extends Version {
  FlutterVersion({
    required Version frameworkVersion,
    required this.channel,
    required this.repositoryUrl,
    required this.frameworkRevision,
    required this.frameworkCommitDate,
    required this.engineRevision,
    required this.dartSdkVersion,
    required this.devToolsVersion,
    required this.flutterRoot,
  }) : super(
          frameworkVersion.major,
          frameworkVersion.minor,
          frameworkVersion.patch,
          build: frameworkVersion.build,
          preRelease: frameworkVersion.preRelease,
        );

  factory FlutterVersion.fromJson(Map<String, dynamic> json) =>
      _$FlutterVersionFromJson(json);

  static DateTime _commitDateFromJson(String json) => DateTime.parse(json);

  Version get frameworkVersion => this;

  final String channel;

  final String repositoryUrl;

  final String frameworkRevision;

  @JsonKey(fromJson: _commitDateFromJson)
  final DateTime frameworkCommitDate;

  final String engineRevision;

  final Version dartSdkVersion;

  final Version devToolsVersion;

  final String flutterRoot;

  Map<String, dynamic> toJson() => _$FlutterVersionToJson(this);
}
