import "dart:math" as math;

import "package:path/path.dart" as lib_path;
import "package:universal_io/io.dart";

import "../exceptions/out_of_tries.dart";
import "../extensions/file_system_entity.dart";
import "../extensions/random.dart";
import "io_typedefs.dart";

Future<List<String>> getNamesInDir({
  required Directory directory,
  required lib_path.Context pathContext,
}) {
  return directory
      .list(
        recursive: false,
        followLinks: false,
      )
      .map((e) => e.getName(pathContext: pathContext))
      .toList();
}

List<String> getNamesInDirSync({
  required Directory directory,
  required lib_path.Context pathContext,
}) {
  return directory
      .listSync(
        recursive: false,
        followLinks: false,
      )
      .map((e) => e.getName(pathContext: pathContext))
      .toList();
}

/// {@template my_utility.src.get_unique_file.getUniqueFilename}
/// * [length] - The length of the unique filename. The total length of the
///   name of the returned file is
///   `length + (prefix?.length ?? 0) + (extension?.length ?? 0)`.
///
/// * [maxTries] - How many times a new filename should be generated
///   until a unique one is found in the [Directory]. If this parameter is
///   set to null, then new filenames will be generated until a unique one is
///   found. If this parameter is set to a fixed number and no unique name
///   can be found, then an [OutOfTriesException] gets thrown.
///
/// * [prefix] - The prefix for the unique filename.
///
/// * [extension] - The file extension.
/// {@endtemplate}
String getUniqueFilename({
  required Iterable<String> namesInDir,
  required Directory currentDir,
  int length = 30,
  int? maxTries,
  String? prefix,
  String? extension,
}) {
  String createName() {
    var name = math.Random().nextAlphaNumericString(length);
    name = "${prefix ?? ""}$name";
    if (null == extension) return name;
    return "$name.$extension";
  }

  var uniqueName = createName();

  if (null == maxTries) {
    while (namesInDir.contains(uniqueName)) {
      uniqueName = createName();
    }
  } else {
    var containsUniqueName = namesInDir.contains(uniqueName);

    for (var i = 0; i < maxTries && containsUniqueName; i++) {
      uniqueName = createName();
      containsUniqueName = namesInDir.contains(uniqueName);
    }

    if (containsUniqueName)
      throw OutOfTriesException(
        tries: maxTries,
        message: "Could not find a unique filename "
            "in the directory '${currentDir.path}'",
      );
  }

  return uniqueName;
}

File getUniqueFile({
  required Iterable<String> namesInDir,
  required Directory currentDir,
  required FileFactory factory,
  required lib_path.Context pathContext,
  int length = 30,
  int? maxTries,
  String? prefix,
  String? extension,
}) {
  final uniqueName = getUniqueFilename(
    currentDir: currentDir,
    extension: extension,
    length: length,
    maxTries: maxTries,
    namesInDir: namesInDir,
    prefix: prefix,
  );
  return factory(pathContext.join(currentDir.path, uniqueName));
}
