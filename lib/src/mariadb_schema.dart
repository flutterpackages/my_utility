import "dart:collection";

import "package:dartx/dartx.dart";

import "../extensions.dart";
import "../mixins.dart";
import "../typedefs.dart";

final class MariaDbDatabase extends MapBase<String, MariaDbTable>
    with DeepCopyWithMixin<MariaDbDatabase> {
  MariaDbDatabase({
    required this.name,
  }) : _tables = [];

  final List<MariaDbTable> _tables;

  /// The name of the database.
  final String name;

  @override
  MariaDbTable operator [](Object? key) {
    return _tables.firstWhere(
      (element) => element.name == key,
      orElse: () => throw Exception(
        "The mariadb database `$name` has no table "
        "with the name `$key`",
      ),
    );
  }

  @override
  Iterable<String> get keys => _tables.map((e) => e.name);

  /// check that there are no duplicate table names
  void _checkTableNames() {
    final names = <String>{};
    for (var i = 0; i < _tables.length; i++) {
      final item = _tables[i];

      if (names.contains(item.name))
        throw ArgumentError.value(
          item,
          "tables[$i]",
          "Each table must have a unique name, but the name ${item.name} "
              "occures more than once",
        );

      names.add(item.name);
    }
  }

  int? _getIndexFromKey(Object? key) {
    final idx = _tables.indexWhere((e) => e.name == key);
    return 0 > idx ? null : idx;
  }

  void checkTables() {
    for (final item in _tables) {
      item.checkColumns();
    }
  }

  void setTable(MariaDbTable value) => this[value.name] = value;

  @override
  void operator []=(String key, MariaDbTable value) {
    final idx = _getIndexFromKey(key);
    if (null == idx) {
      _tables.add(value);
    } else {
      _tables.insert(idx, value);
      _tables.removeAt(idx + 1);
    }

    _checkTableNames();
  }

  @override
  void clear() {
    _tables.clear();
  }

  @override
  MariaDbTable remove(Object? key) {
    final idx = _getIndexFromKey(key);
    if (null == idx)
      return throw Exception(
        "The mariadb database `$name` has no table "
        "with the name `$key`",
      );

    return _tables.removeAt(idx);
  }

  void write(
    StringSink sink, {
    bool lockTables = true,
  }) {
    checkTables();

    sink.write("""
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

""");

    for (final tbl in _tables) {
      sink.writeln("""

--
-- Dumping data for table `${tbl.name}`
--""");

      if (lockTables) {
        sink.writeln("LOCK TABLES `${tbl.name}` WRITE;");
      }

      if (tbl.hasColumnsWithRows) {
        // insert data
        final colNames = tbl.keys.toList();
        sink.write("INSERT INTO `${tbl.name}` (");
        sink.write(colNames.joinToString(transform: (e) => "`$e`"));
        sink.writeln(") VALUES");

        // ([\\'"]{1})
        final strEscapeRegex = RegExp("([\\\\'\"]{1})");

        final rowCount = tbl.first.value.length;
        for (var row = 0; row < rowCount; row++) {
          sink.write("(");
          for (final name in colNames) {
            final col = tbl[name];
            final val = col[row];

            if (null == val) {
              sink.write("NULL");
            } else {
              final escaped = val.replaceAllMapped(
                strEscapeRegex,
                (match) => "\\${match[0]!}",
              );
              sink.write(col.encloseInSingleQuotes ? "'$escaped'" : escaped);
            }

            if (name != colNames.last) {
              sink.write(", ");
            }
          }
          sink.write(")");

          if (row == rowCount - 1) {
            sink.writeln(";");
          } else {
            sink.writeln(",");
          }
        }
      }

      if (lockTables) {
        sink.writeln("UNLOCK TABLES;");
      }
    }

    sink.write("""

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
""");
  }

  @override
  MariaDbDatabase deepCopyWith({
    String? name,
  }) {
    final db = MariaDbDatabase(name: name ?? this.name);
    for (final tbl in _tables) {
      db._tables.add(tbl.deepCopyWith());
    }
    return db;
  }
}

typedef MariaDbTableRow = Map<String, String?>;

final class MariaDbTable extends MapBase<String, MariaDbColumn>
    with DeepCopyWithMixin<MariaDbTable> {
  MariaDbTable({
    required this.name,
  }) : _columns = [];

  final List<MariaDbColumn> _columns;

  /// The name of the table in the database.
  final String name;

  @override
  MariaDbColumn operator [](Object? key) {
    return _columns.firstWhere(
      (element) => element.name == key,
      orElse: () => throw Exception(
        "This mariadb table `$name` has no column "
        "with the name `$key`",
      ),
    );
  }

  @override
  Iterable<String> get keys => _columns.map((e) => e.name);

  MapEntry<String, MariaDbColumn> get first => entries.first;

  bool get hasColumnsWithRows {
    if (isEmpty) return false;
    return first.value.isNotEmpty;
  }

  MariaDbTableRows get rows {
    checkColumns();
    return MariaDbTableRows._(this);
  }

  /// check that there are no duplicate column names
  void _checkColumnNames() {
    final names = <String>{};
    for (var i = 0; i < _columns.length; i++) {
      final item = _columns[i];

      if (names.contains(item.name))
        throw ArgumentError.value(
          item,
          "columns[$i]",
          "Each column must have a unique name, but the name "
              "${item.name} occures more than once",
        );

      names.add(item.name);
    }
  }

  /// Check that each column has the same amount of rows
  ///
  /// Throws an [ArgumentError], if a column has a different amount of rows.
  void checkColumns() {
    if (_columns.isEmpty) return;

    final rowLen = _columns.first.length;
    for (var i = 0; i < _columns.length; i++) {
      final c = _columns[i];
      final currentRowLen = c.length;

      if (currentRowLen != rowLen)
        throw ArgumentError.value(
          currentRowLen,
          c.name,
          "Each column in the table `$name` must have the same amount of rows, "
          "which would be $rowLen, but this column has a different amount "
          "of rows",
        );
    }
  }

  /// Called whenever the rows of the column at [index] change
  void _onColumnRowsChanged(int index) {
    // verifiyColumns();
  }

  int? _getIndexFromKey(Object? key) {
    final idx = _columns.indexWhere((e) => e.name == key);
    return 0 > idx ? null : idx;
  }

  void setColumn(MariaDbColumn column) => this[column.name] = column;

  @override
  void operator []=(String key, MariaDbColumn value) {
    final idx = _getIndexFromKey(key);

    if (null == idx) {
      _columns.add(value);
      value._onRowsChanged = () => _onColumnRowsChanged(_columns.lastIndex);
    } else {
      _columns.insert(idx, value);
      final col = _columns.removeAt(idx + 1);
      value._onRowsChanged = col._onRowsChanged;
      col._onRowsChanged = null;
    }

    _checkColumnNames();
  }

  @override
  void clear() {
    for (final item in _columns) {
      item._onRowsChanged = null;
    }
    _columns.clear();
  }

  @override
  MariaDbColumn remove(Object? key) {
    final idx = _getIndexFromKey(key);
    if (null == idx)
      throw Exception(
        "This mariadb table `$name` has no column "
        "with the name `$key`",
      );

    final col = _columns.removeAt(idx);
    col._onRowsChanged = null;
    return col;
  }

  @override
  MariaDbTable deepCopyWith({
    String? name,
  }) {
    final tbl = MariaDbTable(name: name ?? this.name);
    for (final col in _columns) {
      tbl._columns.add(col.deepCopyWith());
    }
    return tbl;
  }
}

final class MariaDbColumn extends ListBase<String?>
    with DeepCopyWithMixin<MariaDbColumn> {
  MariaDbColumn({
    required this.name,
    List<String?> rows = const [],
    this.alwaysEncloseInSingleQuotes = false,
  }) : _rows = List.of(rows);

  final List<String?> _rows;

  VoidCallback? _onRowsChanged;

  /// The name of the column in the table.
  final String name;

  /// Whether or not a value of this column should always be enclosed in
  /// single quotes in the [MariaDbDatabase.write] method.
  ///
  /// Defaults to `false`.
  final bool alwaysEncloseInSingleQuotes;

  bool get encloseInSingleQuotes {
    if (alwaysEncloseInSingleQuotes) return true;

    // If there is an empty string value, then it must be enclosed
    // in single quotes and therefore all other values should be
    // enclosed as well.
    if (contains("")) return true;

    // A number that starts with 0, but not only '0'
    if (indexWhere((e) => null != e && e.length > 1 && e.startsWith("0")) >= 0)
      return true;

    // Anything other than numbers
    final nonNumberRegex = RegExp(r"[^\d]+");
    if (indexWhere((e) => null != e && nonNumberRegex.hasMatch(e)) >= 0)
      return true;

    return false;
  }

  @override
  int get length => _rows.length;

  @override
  set length(int newLength) {
    if (_rows.length == newLength) return;
    _rows.length = newLength;
    _onRowsChanged?.call();
  }

  @override
  void add(String? element) {
    _rows.add(element);
    _onRowsChanged?.call();
  }

  @override
  void addAll(Iterable<String?> iterable) {
    _rows.addAll(iterable);
    _onRowsChanged?.call();
  }

  @override
  String? operator [](int index) {
    return _rows[index];
  }

  @override
  void operator []=(int index, String? value) {
    _rows[index] = value;
    _onRowsChanged?.call();
  }

  @override
  MariaDbColumn deepCopyWith({
    String? name,
  }) {
    return MariaDbColumn(name: name ?? this.name, rows: _rows);
  }
}

//

final class MariaDbTableRows extends ListBase<MariaDbTableRow> {
  MariaDbTableRows._(this._table);

  final MariaDbTable _table;

  @override
  Iterator<MariaDbTableRow> get iterator => MariaDbTableRowIterator._(this);

  @override
  int get length {
    return _table.isEmpty ? 0 : _table.first.value.length;
  }

  @override
  set length(int newLength) {
    throw UnsupportedError("Cannot change the row count of a table");
  }

  @override
  MariaDbTableRow operator [](int index) {
    RangeError.checkValidIndex(index, this);
    return _table._columns.toMap(
      key: (_, element) => element.name,
      value: (_, element) => element[index],
    );
  }

  @override
  void operator []=(int index, MariaDbTableRow value) {
    RangeError.checkValidIndex(index, this);
    _checkRow(value, "value");
    for (final col in _table._columns) {
      col[index] = value[col.name];
    }
  }

  void _checkRow(MariaDbTableRow row, String name) {
    for (final k in _table.keys) {
      if (row.containsKey(k)) continue;
      throw ArgumentError.value(
        row,
        name,
        "There must be an entry for every column in a row, "
        "but there is no entry for the column `$k`",
      );
    }
  }

  @override
  void add(MariaDbTableRow element) {
    _checkRow(element, "element");
    for (final col in _table._columns) {
      col.add(element[col.name]);
    }
  }

  @override
  void addAll(Iterable<MariaDbTableRow> iterable) {
    var i = 0;
    for (final currentRow in iterable) {
      _checkRow(currentRow, "iterable[$i]");

      for (final col in _table._columns) {
        col.add(currentRow[col.name]);
      }

      i++;
    }
  }

  @override
  void clear() {
    for (final col in _table._columns) {
      col.clear();
    }
  }
}

final class MariaDbTableRowIterator implements Iterator<MariaDbTableRow> {
  MariaDbTableRowIterator._(this._rows)
      : _rowCount = _rows.length,
        _colCount = _rows._table.length,
        _rowIndex = 0,
        _isDone = false;

  final MariaDbTableRows _rows;

  final int _rowCount;
  final int _colCount;

  int _rowIndex;
  MariaDbTableRow? _current;

  bool get isDone => _isDone;
  bool _isDone;

  @override
  MariaDbTableRow get current {
    if (isDone) throw StateError("This iterator is already done");

    if (null == _current)
      throw StateError(
        "moveNext must be called before the current element can be accessed",
      );

    return _current!;
  }

  @pragma("vm:prefer-inline")
  @override
  bool moveNext() {
    final rowCount = _rows.length;
    if (_rowCount != rowCount) throw ConcurrentModificationError(_rows);

    final colCount = _rows._table.length;
    if (_colCount != colCount) throw ConcurrentModificationError(_rows);

    if (_rowIndex >= rowCount) {
      _isDone = true;
      _current = null;
      return false;
    }

    _current = _rows._table._columns.toMap(
      key: (_, element) => element.name,
      value: (_, element) => element[_rowIndex],
    );
    _rowIndex++;

    return true;
  }
}
