final class DoubleWithTolerance {
  const DoubleWithTolerance(
    this.value, {
    this.epsilon = precisionErrorTolerance,
  });

  /// The epsilon of tolerable double precision error.
  ///
  /// This is used in various places in the framework to allow for floating
  /// point precision loss in calculations. Differences below this threshold
  /// are safe to disregard.
  static const precisionErrorTolerance = 1e-10;

  final double value;
  final double epsilon;

  @override
  int get hashCode => Object.hash(value, epsilon);

  @override
  bool operator ==(Object other) => _isNumber(other) && isEqual(other);

  bool operator <(Object other) => _isNumber(other) && isLessThan(other);

  bool operator <=(Object other) =>
      _isNumber(other) && isLessThanOrEqual(other);

  bool operator >(Object other) => _isNumber(other) && isGreaterThan(other);

  bool operator >=(Object other) =>
      _isNumber(other) && isGreaterThanOrEqual(other);

  double operator +(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value + other,
      DoubleWithTolerance() => value + other.value,
      _ => throw TypeError(),
    };
  }

  double operator -(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value - other,
      DoubleWithTolerance() => value - other.value,
      _ => throw TypeError(),
    };
  }

  double operator *(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value * other,
      DoubleWithTolerance() => value * other.value,
      _ => throw TypeError(),
    };
  }

  double operator %(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value % other,
      DoubleWithTolerance() => value % other.value,
      _ => throw TypeError(),
    };
  }

  double operator /(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value / other,
      DoubleWithTolerance() => value / other.value,
      _ => throw TypeError(),
    };
  }

  int operator ~/(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value ~/ other,
      DoubleWithTolerance() => value ~/ other.value,
      _ => throw TypeError(),
    };
  }

  @pragma("vm:prefer-inline")
  bool _isNumber(Object object) {
    return object is num || object is DoubleWithTolerance;
  }

  @pragma("vm:prefer-inline")
  void _checkArg(Object arg, String name) {
    if (!_isNumber(arg)) return;
    ArgumentError.value(
      arg,
      name,
      "Must be either of type 'num' or 'DoubleWithTolerance'",
    );
  }

  @pragma("vm:prefer-inline")
  bool isEqual(Object other) {
    _checkArg(other, "other");

    // Same DoubleWithTolerance instance
    if (identical(this, other)) return true;

    final otherValue = switch (other) {
      num() => other,
      DoubleWithTolerance() => other.value,
      _ => throw TypeError(),
    };

    // double value compares equal (double.infinity, ...)
    if (otherValue == value) return true;

    // compare the two values with the given tolerance of this
    // DoubleWithTolerance instance.
    return (otherValue - value).abs() <= epsilon;
  }

  @pragma("vm:prefer-inline")
  bool isLessThan(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value < other,
      DoubleWithTolerance() => value < other.value,
      _ => throw TypeError(),
    };
  }

  @pragma("vm:prefer-inline")
  bool isLessThanOrEqual(Object other) => isLessThan(other) || isEqual(other);

  @pragma("vm:prefer-inline")
  bool isGreaterThan(Object other) {
    _checkArg(other, "other");
    return switch (other) {
      num() => value > other,
      DoubleWithTolerance() => value > other.value,
      _ => throw TypeError(),
    };
  }

  @pragma("vm:prefer-inline")
  bool isGreaterThanOrEqual(Object other) {
    _checkArg(other, "other");
    return isGreaterThan(other) || isEqual(other);
  }

  @override
  String toString() => "$value \u00B1$epsilon";
}
