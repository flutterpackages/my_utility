@Deprecated(
  "Will be removed in version 2.0.0, use "
  "package:my_utility/extensions/string.dart instead.",
)
library;

import "extensions/string.dart"
    show
        MyUtilityExtensionStringCamelOrPascalToSnake,
        MyUtilityExtensionStringSnakeToPascal;

@Deprecated(
  "Will be removed in version 2.0.0, use "
  "'MyUtilityExtensionStringPascalToSnake.pascalToSnake()' instead.",
)
String pascalToSnake(String pascalcase) => pascalcase.camelOrPascalToSnake();

@Deprecated(
  "Will be removed in version 2.0.0, use "
  "'MyUtilityExtensionStringCamelToSnake.camelToSnake()' instead.",
)
String camelToSnake(String camelcase) => camelcase.camelOrPascalToSnake();

@Deprecated(
  "Will be removed in version 2.0.0, use "
  "'MyUtilityExtensionStringSnakeToPascal.snakeToPascal()' instead.",
)
String snakeToPascal(String snakecase) => snakecase.snakeToPascal();
