import "../src/constants.dart" show kIsWeb;

final class ProgramInfo {
  ProgramInfo._(
    this.trace, [
    this.frameOffset = 0,
  ]) {
    if (kIsWeb)
      throw UnsupportedError(
        "ProgramInfo is not supported when compiling to JavaScript.",
      );

    // The trace comes with multiple lines of strings,
    // (each line is also known as a frame), so split the trace's
    // string by lines to get all the frames
    final frames = trace.toString().split("\n");

    // The first frame is the current function
    function = _getFunctionNameFromFrame(frames[frameOffset]);
  }

  factory ProgramInfo.withFrameOffset(int offset) {
    assert(offset >= 1);
    return ProgramInfo._(
      StackTrace.current,
      offset,
    );
  }

  factory ProgramInfo.current() => ProgramInfo.withFrameOffset(2);

  final int frameOffset;

  /// The name of the current function.
  late final String function;

  final StackTrace trace;

  String _getFunctionNameFromFrame(String frame) {
    // To get rid off the #number thing, get the index of the first whitespace
    var indexOfWhiteSpace = frame.indexOf(" ");

    // Create a substring from the first whitespace index till
    // the end of the string
    var name = frame.substring(indexOfWhiteSpace);

    // Grab the function name using reg expr
    final indexOfFunction = name.indexOf(RegExp("[A-Za-z0-9_]"));

    // Create a new substring from the function name index till
    // the end of string
    name = name.substring(indexOfFunction);

    indexOfWhiteSpace = name.indexOf(" ");

    // Create a new substring from start to the first index of a whitespace.
    // This substring gives us the function name
    name = name.substring(0, indexOfWhiteSpace);

    return name;
  }
}
