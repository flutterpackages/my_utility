import "../cli.dart";
import "../io.dart";

final class NpmCliTool extends CliTool {
  NpmCliTool({
    super.executable = "npm",
    super.windowsCodePage,
  });

  /// Run `npm install`.
  ///
  /// Returns true if the process finished without any errors.
  Future<bool> getPackages({
    required String workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
    bool production = false,
    bool binLinks = true,
  }) {
    return evaluateProcess(
      [
        "install",
        if (production) "--production",
        binLinks ? "--bin-links" : "--no-bin-links",
      ],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
  }

  Future<bool> installGlobal(String packageName) {
    return evaluateProcess([
      "install",
      "--global",
      packageName,
    ]);
  }

  Future<bool> runScript({
    required String name,
    required String workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
    List<String> npmArgs = const [],
  }) {
    return evaluateProcess(
      [...npmArgs, "run", name],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // 9.5.0
    final versionStr = runSync(["--version"]).stdout.toString().trim();
    return Version.parse(versionStr);
  }
}
