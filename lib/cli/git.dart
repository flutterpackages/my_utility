import "package:path/path.dart" as lib_path;

import "../io.dart";
import "cli_tool.dart";

final class GitCliTool extends CliTool {
  GitCliTool({
    super.executable = "git",
    super.windowsCodePage,
  });

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // git version 2.39.2
    final versionStr = runSync(["--version"]).stdout.toString();

    // Get "major.minor.patch"
    final regex = RegExp(r"\d+\.\d+\.\d+");
    return Version.parse(regex.stringMatch(versionStr)!);
  }

  Future<void> updateSubmodules(
    String gitDir, {
    bool init = true,
    bool recursive = false,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final dir = checkDirectoryExists(gitDir, "gitDir");

    final res = await runAsync(
      [
        "submodule",
        "update",
        if (init) "--init",
        if (recursive) "--recursive",
      ],
      workingDirectory: dir.path,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: "Failed to update the submodules in the git "
            "directory at '$gitDir'",
      );
  }

  Future<void> pull({
    required String gitDir,
    required String remote,
    required String branch,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final dir = checkDirectoryExists(gitDir, "gitDir");

    final res = await runAsync(
      [
        "pull",
        remote,
        branch,
      ],
      workingDirectory: dir.path,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: "Failed to pull updates from the branch '$branch' in the "
            "remote '$remote' into the git directory at '$gitDir'",
      );
  }

  /// Get the commit the directory at [gitDirpath] is at.
  Future<String> getCurrentCommitSha({
    required String gitDirpath,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final dir = checkDirectoryExists(gitDirpath, "gitDirpath");

    final res = await runAsync(
      ["rev-parse", "HEAD"],
      workingDirectory: dir.path,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: "Failed to get the current commit sha from "
            "the git directory at '$gitDirpath'",
      );

    return res.stdout.toString().trim();
  }

  /// Get a chronologically sorted list of all commits in the
  /// git repository at [gitDirpath].
  ///
  /// The first element in the list is the most recent commit.
  Future<List<String>> getCommitShas({
    required String gitDirpath,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final dir = checkDirectoryExists(gitDirpath, "gitDirpath");

    final res = await runAsync(
      ["log", "--format=%H"],
      workingDirectory: dir.path,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: "Failed to get the commit shas from "
            "the git directory at '$gitDirpath'",
      );

    return res.stdout
        .toString()
        .split("\n")
        .where((e) => e.isNotEmpty)
        .toList();
  }

  /// Get a chronologically sorted list of all commits from the file
  /// at [relativeFilepath] in the git repository at [gitDirpath].
  ///
  /// The first element in the list is the most recent commit.
  Future<List<String>> getCommitShasForFile({
    required String gitDirpath,
    required String relativeFilepath,
    bool followName = true,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final dir = checkDirectoryExists(gitDirpath, "gitDirpath");
    final file = checkFileExists(lib_path.join(dir.path, relativeFilepath));

    final res = await runAsync(
      [
        "log",
        "--follow",
        "--format=%H",
        "--",
        lib_path.relative(file.path, from: dir.path),
      ],
      workingDirectory: dir.path,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: "Failed to get the commit shas from "
            "the git directory at '$gitDirpath'",
      );

    return res.stdout
        .toString()
        .split("\n")
        .where((e) => e.isNotEmpty)
        .toList();
  }

  Future<Uri> getRemoteUrl({
    required String gitDirpath,
    required String remote,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final dir = checkDirectoryExists(gitDirpath, "gitDirpath");

    final res = await runAsync(
      ["remote", "get-url", remote],
      workingDirectory: dir.path,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: "Failed to get the url for the remote '$remote' "
            "int the git directory at '$gitDirpath'",
      );

    return Uri.parse(res.stdout.toString().trim());
  }
}
