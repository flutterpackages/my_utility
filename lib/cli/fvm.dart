import "dart:convert";

import "package:universal_io/io.dart";

import "../extensions/directory.dart";
import "../io/utils.dart";
import "cli_tool.dart";
import "flutter.dart";

final class FvmCliTool extends CliTool {
  FvmCliTool({
    super.executable = "fvm",
    super.windowsCodePage,
  });

  FlutterCliTool? getFlutterCliToolSync({
    String? workingDirectory,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
    bool preferProjectConfig = true,
  }) {
    if (preferProjectConfig) {
      return _getProjectFlutterCliToolSync(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          ) ??
          _getGlobalFlutterCliToolSync(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          );
    } else {
      return _getGlobalFlutterCliToolSync(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          ) ??
          _getProjectFlutterCliToolSync(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          );
    }
  }

  Future<FlutterCliTool?> getFlutterCliTool({
    String? workingDirectory,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
    bool preferProjectConfig = true,
  }) async {
    if (preferProjectConfig) {
      return (await _getProjectFlutterCliTool(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          )) ??
          (await _getGlobalFlutterCliTool(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          ));
    } else {
      return (await _getGlobalFlutterCliTool(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          )) ??
          (await _getProjectFlutterCliTool(
            workingDirectory: workingDirectory,
            includeParentEnvironment: includeParentEnvironment,
            environment: environment,
          ));
    }
  }

  Future<FlutterCliTool?> _getGlobalFlutterCliTool({
    String? workingDirectory,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final ctxResult = await runAsync(
      ["api", "context"],
      workingDirectory: workingDirectory,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );
    if (0 != ctxResult.exitCode)
      throw CliResultException(
        exitCode: ctxResult.exitCode,
        stderr: ctxResult.stderr,
        message: "Failed to retrieve the global FVM context.",
      );

    var ctx = jsonDecode(ctxResult.stdout) as Map<String, dynamic>;
    ctx = ctx["context"] as Map<String, dynamic>;
    final globalCacheBinDir = Directory(ctx["globalCacheBinPath"] as String);

    if (!await globalCacheBinDir.exists()) return null;

    return FlutterCliTool(
      executable: globalCacheBinDir
          .file("flutter${Platform.isWindows ? ".bat" : ""}")
          .path,
    );
  }

  FlutterCliTool? _getGlobalFlutterCliToolSync({
    String? workingDirectory,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) {
    final ctxResult = runSync(
      ["api", "context"],
      workingDirectory: workingDirectory,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );
    if (0 != ctxResult.exitCode)
      throw CliResultException(
        exitCode: ctxResult.exitCode,
        stderr: ctxResult.stderr,
        message: "Failed to retrieve the global FVM context.",
      );

    var ctx = jsonDecode(ctxResult.stdout) as Map<String, dynamic>;
    ctx = ctx["context"] as Map<String, dynamic>;
    final globalCacheBinDir = Directory(ctx["globalCacheBinPath"] as String);

    if (!globalCacheBinDir.existsSync()) return null;

    return FlutterCliTool(
      executable: globalCacheBinDir
          .file("flutter${Platform.isWindows ? ".bat" : ""}")
          .path,
    );
  }

  Future<FlutterCliTool?> _getProjectFlutterCliTool({
    String? workingDirectory,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final cwd = null == workingDirectory
        ? Directory.current
        : Directory(workingDirectory);

    if (!await cwd.file(".fvmrc").exists()) return null;

    final ctxResult = await runAsync(
      ["api", "project"],
      workingDirectory: workingDirectory,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );
    if (0 != ctxResult.exitCode)
      throw CliResultException(
        exitCode: ctxResult.exitCode,
        stderr: ctxResult.stderr,
        message: "Failed to retrieve the project local FVM context.",
      );

    var ctx = jsonDecode(ctxResult.stdout) as Map<String, dynamic>;
    ctx = ctx["project"] as Map<String, dynamic>;
    final flutterVersionDir = Directory(
      Link(ctx["localVersionSymlinkPath"] as String).resolveSymbolicLinksSync(),
    );

    if (!await flutterVersionDir.exists()) return null;

    return FlutterCliTool(
      executable: flutterVersionDir
          .directory("bin")
          .file("flutter${Platform.isWindows ? ".bat" : ""}")
          .path,
    );
  }

  FlutterCliTool? _getProjectFlutterCliToolSync({
    String? workingDirectory,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) {
    final cwd = null == workingDirectory
        ? Directory.current
        : Directory(workingDirectory);

    if (!cwd.file(".fvmrc").existsSync()) return null;

    final ctxResult = runSync(
      ["api", "project"],
      workingDirectory: workingDirectory,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );
    if (0 != ctxResult.exitCode)
      throw CliResultException(
        exitCode: ctxResult.exitCode,
        stderr: ctxResult.stderr,
        message: "Failed to retrieve the project local FVM context.",
      );

    var ctx =
        jsonDecode(ctxResult.stdout.toString().trim()) as Map<String, dynamic>;
    ctx = ctx["project"] as Map<String, dynamic>;
    final flutterVersionDir = Directory(
      Link(ctx["localVersionSymlinkPath"] as String).resolveSymbolicLinksSync(),
    );

    if (!flutterVersionDir.existsSync()) return null;

    return FlutterCliTool(
      executable: flutterVersionDir
          .directory("bin")
          .file("flutter${Platform.isWindows ? ".bat" : ""}")
          .path,
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    final versionStr = runSync(["--version"]).stdout.toString().trim();
    return Version.parse(versionStr);
  }
}
