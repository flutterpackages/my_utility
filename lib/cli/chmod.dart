import "../extensions/file_stat_permission.dart";
import "../io/file_stat_permission.dart";
import "../io/utils.dart";
import "cli_tool.dart";

final class ChmodCliTool extends CliTool {
  ChmodCliTool({
    super.executable = "chmod",
    super.windowsCodePage,
  });

  Future<void> setPermissions({
    required String filepath,
    required Iterable<FileStatPermission> owner,
    required Iterable<FileStatPermission> group,
    required Iterable<FileStatPermission> other,
  }) async {
    final octalMode =
        owner.toOctalMode() + group.toOctalMode() + other.toOctalMode();

    final res = await runAsync([octalMode, filepath]);
    if (0 != res.exitCode)
      throw CliResultException(
        message: "Failed to set the permissions of '$filepath' to $octalMode",
        exitCode: res.exitCode,
        stderr: res.stderr,
      );
  }

  void setPermissionsSync({
    required String filepath,
    required Iterable<FileStatPermission> owner,
    required Iterable<FileStatPermission> group,
    required Iterable<FileStatPermission> other,
  }) {
    final octalMode =
        owner.toOctalMode() + group.toOctalMode() + other.toOctalMode();

    final res = runSync([octalMode, filepath]);
    if (0 != res.exitCode)
      throw CliResultException(
        message: "Failed to set the permissions of '$filepath' to $octalMode",
        exitCode: res.exitCode,
        stderr: res.stderr,
      );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // chmod (GNU coreutils) 9.1
    // Copyright (C) 2022 Free Software Foundation, Inc.
    // License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
    // This is free software: you are free to change and redistribute it.
    // There is NO WARRANTY, to the extent permitted by law.
    //
    // Written by David MacKenzie and Jim Meyering.

    final versionStr =
        runSync(["--version"]).stdout.toString().trim().split("\n").first;
    final regex = RegExp(r"\d+\.\d+");
    return Version.parse(regex.stringMatch(versionStr)!);
  }
}
