import "dart:io";

import "../extensions/directory.dart";
import "../io.dart";
import "../src/utils.dart";
import "chmod.dart";
import "cli_tool.dart";
import "elixir_mix.dart";

final class ElixirCliTool extends CliTool {
  ElixirCliTool({
    super.executable = "elixir",
    super.windowsCodePage,
  });

  String _getMixCliToolStartArgs() {
    return [
      "--eval", '"Mix.start(); Mix.CLI.main()"', //
    ].join(" ");
  }

  ElixirMixCliTool getMixCliToolSync() {
    final exe = Directory.systemTemp.getUniqueFileSync(
      prefix: "elixir_mix",
      extension: Platform.isWindows ? "bat" : "sh",
    );

    if (Platform.isWindows) {
      writeWindowsBatchScriptSync(
        file: exe,
        contents: """
REM Forward arguments to the actual executable
$executable ${_getMixCliToolStartArgs()} %*
""",
      );
    } else {
      exe.writeAsStringSync(
        """
#! /bin/sh
exec $executable ${_getMixCliToolStartArgs()} "\$@"
""",
        flush: true,
        mode: FileMode.writeOnly,
        encoding: systemEncoding,
      );
      ChmodCliTool().setPermissionsSync(
        filepath: exe.path,
        owner: {read, write, execute},
        group: {read, execute},
        other: {read, execute},
      );
    }

    return ElixirMixCliTool(
      executable: exe.path,
    );
  }

  Future<ElixirMixCliTool> getMixCliTool() async {
    final exe = await Directory.systemTemp.getUniqueFile(
      prefix: "elixir_mix",
      extension: Platform.isWindows ? "bat" : "sh",
    );

    if (Platform.isWindows) {
      await writeWindowsBatchScript(
        file: exe,
        contents: """
REM Forward arguments to the actual executable
$executable ${_getMixCliToolStartArgs()} %*
""",
      );
    } else {
      await exe.writeAsString(
        """
#! /bin/sh
exec $executable ${_getMixCliToolStartArgs()} "\$@"
""",
        flush: true,
        mode: FileMode.writeOnly,
        encoding: systemEncoding,
      );
      await ChmodCliTool().setPermissions(
        filepath: exe.path,
        owner: {read, write, execute},
        group: {read, execute},
        other: {read, execute},
      );
    }

    return ElixirMixCliTool(
      executable: exe.path,
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    final versionStr = runSync(["--version"]).stdout.toString();
    return getElixirCliToolVersion(versionStr);
  }
}
