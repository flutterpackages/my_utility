import "dart:convert";

import "package:path/path.dart" as lib_path;
import "package:universal_io/io.dart";

import "../io.dart";
import "../src/models/flutter_verison.dart";
import "cli_tool.dart";
import "dart.dart";

export "../src/models/flutter_verison.dart" show FlutterVersion;

final class FlutterCliTool extends CliTool {
  FlutterCliTool({
    super.executable = "flutter",
    super.windowsCodePage,
  });

  /// Get the [DartCliTool] that is associated with this flutter tool.
  DartCliTool getDartCliToolSync() {
    final version = getVersionSync();
    final dartExe = lib_path.join(
      version.flutterRoot,
      "bin",
      "dart${Platform.isWindows ? ".bat" : ""}",
    );
    return DartCliTool(executable: dartExe);
  }

  /// Get the [DartCliTool] that is associated with this flutter tool.
  Future<DartCliTool> getDartCliTool() async {
    final version = await getVersion();
    final dartExe = lib_path.join(
      version.flutterRoot,
      "bin",
      "dart${Platform.isWindows ? ".bat" : ""}",
    );
    return DartCliTool(executable: dartExe);
  }

  /// Run `flutter pub get`.
  ///
  /// Returns true if the process finished without any errors.
  Future<bool> getPackages({
    required String workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) {
    return evaluateProcess(
      ["pub", "get"],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
  }

  Future<bool> runPubPackage({
    required String workingDirectory,
    required String package,
    List<String> packageArgs = const [],
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) {
    return evaluateProcess(
      ["pub", "run", package, ...packageArgs],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
  }

  Future<bool> build({
    required String workingDirectory,
    required String target,
    CliStreamConsumer consumer = const CliStreamConsumer(),
    String? entrypoint,
  }) {
    return evaluateProcess(
      [
        "build",
        target,
        if (null != entrypoint) ...["-t", entrypoint],
      ],
      consumer: consumer,
      workingDirectory: workingDirectory,
    );
  }

  Future<bool> clean({
    required String workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) {
    return evaluateProcess(
      ["clean"],
      consumer: consumer,
      workingDirectory: workingDirectory,
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  FlutterVersion getVersionSync() {
    // {
    //   "frameworkVersion": "3.10.6",
    //   "channel": "unknown",
    //   "repositoryUrl": "unknown source",
    //   "frameworkRevision": "f468f3366c26a5092eb964a230ce7892fda8f2f8",
    //   "frameworkCommitDate": "2023-07-12 15:19:05 -0700",
    //   "engineRevision": "cdbeda788a293fa29665dc3fa3d6e63bd221cb0d",
    //   "dartSdkVersion": "3.0.6",
    //   "devToolsVersion": "2.23.1",
    //   "flutterRoot": "/home/emanuel/programs/flutter"
    // }
    final versionStr =
        runSync(["--version", "--machine"]).stdout.toString().trim();

    final json = jsonDecode(versionStr);
    return FlutterVersion.fromJson(json);
  }

  Future<FlutterVersion> getVersion() async {
    final versionStr =
        (await runAsync(["--version", "--machine"])).stdout.toString().trim();

    final json = jsonDecode(versionStr);
    return FlutterVersion.fromJson(json);
  }
}
