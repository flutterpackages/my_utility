part of "cli_tool.dart";

sealed class CliException implements Exception {
  const CliException({
    this.message,
  });

  final String? message;
}

class CliArgumentException extends CliException {
  const CliArgumentException(
    this.argument, {
    this.name,
    super.message,
  });

  final dynamic argument;
  final String? name;

  @override
  String toString() {
    final buf = StringBuffer();

    buf.write("The provided argument ");

    if (null != name) {
      buf.write("($name)");
    }

    buf.write(" is invalid for the cli tool");

    if (null != message) {
      buf.write(": ");
      buf.write(message);
    }

    buf.write(": ");
    buf.writeln(
      switch (argument) {
        String() => '"$argument"',
        _ => argument,
      },
    );

    return buf.toString();
  }
}

class CliResultException extends CliException {
  const CliResultException({
    required this.exitCode,
    required this.stderr,
    super.message,
  });

  final int exitCode;
  final String stderr;

  @override
  String toString() {
    final buf = StringBuffer();

    buf.writeln("Exitcode: $exitCode");
    if (null != message) {
      buf.writeln(message);
    }

    buf.writeln();
    buf.writeln("STDERR:");
    buf.writeln(stderr);

    return buf.toString();
  }
}
