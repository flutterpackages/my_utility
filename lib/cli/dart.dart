import "../cli.dart";
import "../io.dart";

final class DartCliTool extends CliTool {
  DartCliTool({
    super.executable = "dart",
    super.windowsCodePage,
  });

  /// Run `dart pub get`.
  ///
  /// Returns true if the process finished without any errors.
  Future<bool> getPackages({
    required String workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) {
    return evaluateProcess(
      ["pub", "get"],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
  }

  Future<bool> runPubPackage({
    required String workingDirectory,
    required String package,
    List<String> packageArgs = const [],
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) {
    return evaluateProcess(
      ["pub", "run", package, ...packageArgs],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
  }

  Future<int> runScript({
    required String scriptPath,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) async {
    checkFileExists(scriptPath, "scriptPath");

    return consumeProcess(
      [scriptPath],
      consumer: consumer,
    ).then((value) => value);
  }

  Future<bool> compileExe({
    required String entryPoint,
    String? outputFilepath,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) {
    return evaluateProcess(
      [
        "compile",
        "exe",
        if (null != outputFilepath) ...["--output", outputFilepath],
        entryPoint,
      ],
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // ignore: lines_longer_than_80_chars
    // Dart SDK version: 3.0.6 (stable) (Tue Jul 11 18:49:07 2023 +0000) on "linux_x64"
    final versionStr = runSync(["--version"]).stdout.toString().trim();

    // Get "major.minor.patch"
    final regex = RegExp(r"\d+\.\d+\.\d+");
    return Version.parse(regex.stringMatch(versionStr)!);
  }
}
