import "package:universal_io/io.dart";

import "../cli.dart";
import "../extensions.dart";
import "../io.dart";

final class NodeCliTool extends CliTool {
  NodeCliTool({
    super.executable = "node",
    super.windowsCodePage,
  });

  Future<Process> startScript({
    required String scriptPath,
    List<String> args = const [],
    String? workingDirectory,
    bool inheritStdio = false,
  }) async {
    final script = await checkFileExists(scriptPath, "scriptPath").resolveUri();
    return startProcess(
      [script.path, ...args],
      workingDirectory: workingDirectory,
      mode: inheritStdio
          ? ProcessStartMode.inheritStdio
          : ProcessStartMode.normal,
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // v18.15.0
    final versionStr = runSync(["--version"]).stdout.toString().trim();

    // remove 'v'
    return Version.parse(versionStr.substring(1));
  }
}
