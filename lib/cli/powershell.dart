import "package:dartx/dartx.dart";
import "package:universal_io/io.dart";

import "../cli.dart";
import "../extensions/directory.dart";
import "../io.dart";

final class PowershellCliTool extends CliTool {
  PowershellCliTool({
    super.executable = "powershell",
    super.windowsCodePage,
  });

  Future<int> runScriptCodeAsync({
    required String script,
    String? workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) async {
    final file = Directory.systemTemp
        .file("${DateTime.now().microsecondsSinceEpoch}.ps1");

    await file.writeAsString(script, mode: FileMode.writeOnly, flush: true);

    return runScriptAsync(
      scriptPath: file.path,
      consumer: consumer,
      workingDirectory: workingDirectory,
    );
  }

  Future<int> runScriptAsync({
    required String scriptPath,
    String? workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) {
    checkFileExists(scriptPath, "scriptPath");

    return consumeProcess(
      [
        "-File", scriptPath, //
      ],
      workingDirectory: workingDirectory,
      consumer: consumer,
    ).then((value) => value);
  }

  ProcessResult runCommandSync(
    List<String> args, {
    String? workingDirectory,
  }) {
    return runSync(args, workingDirectory: workingDirectory);
  }

  Future<ProcessResult> runCommandAsync(
    List<String> args, {
    String? workingDirectory,
  }) {
    return runAsync(args, workingDirectory: workingDirectory);
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["-Help"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    //
    // Major  Minor  Build  Revision
    // -----  -----  -----  --------
    // 5      1      19041  3803
    //
    //

    final parts = runSync([r"$PSVersionTable.PSVersion"])
        .stdout
        .toString()
        .split("\n")[3]
        .split(RegExp(r"\s+"))
        .toList();

    return Version(
      parts[0].toInt(),
      parts[1].toInt(),
      0,
      build: parts[2],
    );
  }
}
