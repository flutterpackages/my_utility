part of "cli_tool.dart";

sealed class CliResult {
  const CliResult();

  factory CliResult.fromResult(io.ProcessResult result) {
    if (0 != result.exitCode) return FailedCliResult._(result);
    return SuccessfulCliResult._(result);
  }

  bool get hasSucceeded => this is SuccessfulCliResult;

  bool get hasFailed => this is FailedCliResult;
}

class SuccessfulCliResult extends CliResult {
  SuccessfulCliResult._(io.ProcessResult result)
      : stdout = result.stdout.toString();

  final String stdout;

  @override
  String toString() {
    const msg = "Successfull CLI result!";
    if (stdout.isEmpty) return msg;
    return "$msg\nCLI stdout:\n$stdout";
  }
}

class FailedCliResult extends CliResult {
  FailedCliResult._(io.ProcessResult result)
      : exitCode = result.exitCode,
        stderr = result.stderr.toString(),
        stdout = result.stdout.toString();

  final int exitCode;
  final String stderr;
  final String stdout;

  @override
  String toString() {
    final buf = StringBuffer();

    buf.writeln("Failed CLI result!");
    buf.writeln("CLI exitcode: $exitCode");
    buf.writeln("CLI stdout:");
    buf.writeln(stdout);
    buf.writeln("-----------------------------------");
    buf.writeln("CLI stderr:");
    buf.writeln(stderr);

    return buf.toString();
  }
}
