import "../io/utils.dart";
import "../src/utils.dart";
import "cli_tool.dart";

final class ElixirMixCliTool extends CliTool {
  ElixirMixCliTool({
    super.executable = "mix",
    super.windowsCodePage,
  });

  Future<void> format(String glob) async {
    final res = await runAsync(["format", glob]);
    if (0 != res.exitCode)
      throw CliResultException(
        message: "Failed to format '$glob'",
        exitCode: res.exitCode,
        stderr: res.stderr,
      );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    final versionStr = runSync(["--version"]).stdout.toString();
    return getElixirCliToolVersion(versionStr);
  }
}
