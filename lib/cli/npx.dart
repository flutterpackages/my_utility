import "../cli.dart";
import "../io.dart";

final class NpxCliTool extends CliTool {
  NpxCliTool({
    super.executable = "npx",
    super.windowsCodePage,
  });

  Future<bool> runPackage({
    required String package,
    required List<String> args,
    required String workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) async {
    return evaluateProcess(
      [package, ...args],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // 9.5.0
    final versionStr = runSync(["--version"]).stdout.toString().trim();
    return Version.parse(versionStr);
  }
}
