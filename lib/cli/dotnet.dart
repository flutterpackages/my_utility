import "../io/utils.dart";
import "cli_tool.dart";

final class DotNetCliTool extends CliTool {
  DotNetCliTool({
    super.executable = "dotnet",
    super.windowsCodePage,
  });

  Future<bool> restorePackages({
    String? workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
  }) async {
    final code = await consumeProcess(
      ["restore"],
      workingDirectory: workingDirectory,
      consumer: consumer,
    );
    return 0 == code;
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--help"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // .NET SDK:
    //  Version:           8.0.404
    //  Commit:            7b190310f2
    //  Workload version:  8.0.400-manifests.d73e769d
    //  MSBuild version:   17.11.9+a69bbaaf5
    //
    // Laufzeitumgebung:
    //  OS Name:     Windows
    //  OS Version:  10.0.19045
    //  OS Platform: Windows
    //  RID:         win-x64
    //  Base Path:   C:\Program Files\dotnet\sdk\8.0.404\
    //
    // Installierte .NET-Workloads:
    // Konfiguriert für die Verwendung loose manifests beim Installieren neuer
    // Manifeste.
    //  [aspire]
    //    Installationsquelle: VS 17.10.35027.167
    //    Manifestversion:    8.0.0/8.0.100
    //    Manifestpfad:       C:\Program Files\dotnet\sdk-manifests\8.0.100\microsoft.net.sdk.aspire\8.0.0\WorkloadManifest.json
    //    Installationstyp:        FileBased
    //
    //  [wasm-tools-net6]
    //    Installationsquelle: VS 17.10.35027.167
    //    Manifestversion:    8.0.11/8.0.100
    //    Manifestpfad:       C:\Program Files\dotnet\sdk-manifests\8.0.100\microsoft.net.workload.mono.toolchain.net6\8.0.11\WorkloadManifest.json
    //    Installationstyp:        FileBased
    //
    //  [wasm-tools-net7]
    //    Installationsquelle: VS 17.10.35027.167
    //    Manifestversion:    8.0.11/8.0.100
    //    Manifestpfad:       C:\Program Files\dotnet\sdk-manifests\8.0.100\microsoft.net.workload.mono.toolchain.net7\8.0.11\WorkloadManifest.json
    //    Installationstyp:        FileBased
    //
    //
    // Host:
    //   Version:      8.0.11
    //   Architecture: x64
    //   Commit:       9cb3b725e3
    //
    // .NET SDKs installed:
    //   8.0.404 [C:\Program Files\dotnet\sdk]
    //
    // .NET runtimes installed:
    //   Microsoft.AspNetCore.App 5.0.17 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
    //   Microsoft.AspNetCore.App 6.0.32 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
    //   Microsoft.AspNetCore.App 7.0.20 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
    //   Microsoft.AspNetCore.App 8.0.7 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
    //   Microsoft.AspNetCore.App 8.0.11 [C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App]
    //   Microsoft.NETCore.App 5.0.17 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
    //   Microsoft.NETCore.App 6.0.29 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
    //   Microsoft.NETCore.App 6.0.32 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
    //   Microsoft.NETCore.App 7.0.20 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
    //   Microsoft.NETCore.App 8.0.7 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
    //   Microsoft.NETCore.App 8.0.11 [C:\Program Files\dotnet\shared\Microsoft.NETCore.App]
    //   Microsoft.WindowsDesktop.App 5.0.17 [C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App]
    //   Microsoft.WindowsDesktop.App 6.0.29 [C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App]
    //   Microsoft.WindowsDesktop.App 6.0.32 [C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App]
    //   Microsoft.WindowsDesktop.App 7.0.20 [C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App]
    //   Microsoft.WindowsDesktop.App 8.0.7 [C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App]
    //   Microsoft.WindowsDesktop.App 8.0.11 [C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App]
    //
    // Other architectures found:
    //   x86   [C:\Program Files (x86)\dotnet]
    //     registered at [HKLM\SOFTWARE\dotnet\Setup\InstalledVersions\x86\InstallLocation]
    //
    // Environment variables:
    //   Not set
    //
    // global.json file:
    //   Not found
    //
    // Learn more:
    //   https://aka.ms/dotnet/info
    //
    // Download .NET:
    //   https://aka.ms/dotnet/download
    final infoLines = runSync(["--info"]).stdout.toString().split("\n");

    // Only interested in 'Host:' section
    final hostLineIdx = infoLines.indexWhere(
      RegExp(
        r"^Host:$",
        multiLine: true,
      ).hasMatch,
    );

    final rawVersion = RegExp(r"Version:\s*(\d+\.\d+\.\d+)\s*")
        .firstMatch(infoLines[hostLineIdx + 1])![1]!;

    return Version.parse(rawVersion);
  }

  Future<bool> createProject({
    required String type,
    String? framework,
    bool useProgramMain = true,
    CliStreamConsumer consumer = const CliStreamConsumer(),
    String? workingDirectory,
  }) {
    return evaluateProcess(
      [
        "new",
        type,
        if (null != framework) ...["--framework", framework], //
        if (useProgramMain) "--use-program-main", //
      ],
      consumer: consumer,
      workingDirectory: workingDirectory,
    );
  }

  Future<void> addNugetPackage({
    required String package,
    Version? version,
    String? workingDirectory,
  }) async {
    final res = await runAsync(
      [
        "add",
        "package",
        package,
        if (null != version) ...[
          "--version",
          "${version.major}.${version.minor}.${version.patch}",
        ],
      ],
      workingDirectory: workingDirectory,
    );

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: "Failed to add package '$package'"
            "${null == version ? "" : " (version $version)"}",
      );
  }
}
