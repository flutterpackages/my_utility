part of "cli_tool.dart";

@Deprecated("Will be removed in version 2.0.0, use 'VoidConsumer' instead.")
typedef NullConsumer = VoidConsumer;

/// A [StreamConsumer], that implements empty methods for the [StreamConsumer]
/// interface.
///
/// A [VoidConsumer] does not register any listeners for a [Stream].
final class VoidConsumer implements StreamConsumer<List<int>> {
  const VoidConsumer();

  static final _nullFuture = Zone.root.run(() => Future.value(null));

  @override
  Future<void> addStream(Stream<List<int>> stream) => _nullFuture;

  @override
  Future<void> close() => _nullFuture;
}

/// A [StreamConsumer], that listents to the events of a [Stream] and throws
/// them away.
///
/// A [FakeConsumer] does register listeners for a [Stream], but all events
/// will be thrown away.
final class FakeConsumer implements StreamConsumer<List<int>> {
  FakeConsumer() : _isClosed = false;

  Completer<void>? _completer;

  bool _isClosed;
  bool get isClosed => _isClosed;

  @override
  Future<void> addStream(Stream<List<int>> stream) async {
    if (isClosed)
      throw StateError(
        "This consumer has already been closed",
      );

    if (null != _completer)
      throw StateError(
        "The previous stream is not done yet",
      );

    _completer = Completer<void>();
    stream.listen(
      (_) {},
      cancelOnError: true,
      onDone: _completer!.completeSafe,
      onError: _completer!.completeErrorSafe,
    );

    try {
      await _completer!.future;
    } on Exception catch (_) {
      rethrow;
    } finally {
      _completer = null;
    }
  }

  @override
  Future<void> close() async {
    if (isClosed) return;
    _isClosed = true;
    return _completer?.future;
  }
}

class StringConsumer implements StreamConsumer<List<int>> {
  StringConsumer(
    this.sink, {
    this.encoding = CliTool.defaultCliEncoding,
  }) : _isClosed = false;

  final StringSink sink;
  final Encoding encoding;

  Completer<void>? _completer;

  bool _isClosed;
  bool get isClosed => _isClosed;

  @override
  Future<void> addStream(Stream<List<int>> stream) async {
    if (isClosed)
      throw StateError(
        "This consumer has already been closed",
      );

    if (null != _completer)
      throw StateError(
        "The previous stream is not done yet",
      );

    _completer = Completer<void>();
    encoding.decoder.bind(stream).listen(
          sink.write,
          cancelOnError: true,
          onDone: _completer!.completeSafe,
          onError: _completer!.completeErrorSafe,
        );

    try {
      await _completer!.future;
    } on Exception catch (_) {
      rethrow;
    } finally {
      _completer = null;
    }
  }

  @override
  Future<void> close() async {
    if (isClosed) return;
    _isClosed = true;
    return _completer?.future;
  }
}

extension StringSinkToConsumerExtension on StringSink {
  StringConsumer getConsumer({
    Encoding encoding = CliTool.defaultCliEncoding,
  }) =>
      StringConsumer(
        this,
        encoding: encoding,
      );
}

class CliStreamConsumer {
  /// Creates a new [CliStreamConsumer].
  ///
  /// If null, then [stdoutConsumer] and [stderrConsumer] default to [io.stdout]
  /// and [io.stderr] respectively. See the getters [stdout] and [stderr].
  const CliStreamConsumer({
    this.stdoutConsumer,
    this.stderrConsumer,
  });

  /// Creates a new [CliStreamConsumer], with default [VoidConsumer]s for
  /// [stdoutConsumer] and [stderrConsumer].
  const CliStreamConsumer.fromVoid({
    StreamConsumer<List<int>> this.stdoutConsumer = const VoidConsumer(),
    StreamConsumer<List<int>> this.stderrConsumer = const VoidConsumer(),
  });

  /// Creates a new [CliStreamConsumer], with default [FakeConsumer]s for
  /// [stdoutConsumer] and [stderrConsumer].
  CliStreamConsumer.fromFake({
    StreamConsumer<List<int>>? stdoutConsumer,
    StreamConsumer<List<int>>? stderrConsumer,
  })  : stdoutConsumer = stdoutConsumer ?? FakeConsumer(),
        stderrConsumer = stderrConsumer ?? FakeConsumer();

  @Deprecated(
    "Will be removed in version 2.0.0, use "
    "'CliStreamConsumer.fromVoid' instead.",
  )
  static const nullConsumer = voidConsumer;

  @Deprecated(
    "Will be removed in version 2.0.0, use "
    "'CliStreamConsumer.fromVoid' instead.",
  )
  static const voidConsumer = CliStreamConsumer(
    stdoutConsumer: VoidConsumer(),
    stderrConsumer: VoidConsumer(),
  );

  final StreamConsumer<List<int>>? stdoutConsumer;
  final StreamConsumer<List<int>>? stderrConsumer;

  /// Returns [stdoutConsumer] if non-null, else [io.stdout].
  StreamConsumer<List<int>> get stdout => stdoutConsumer ?? io.stdout;

  /// Returns [stderrConsumer] if non-null, else [io.stderr].
  StreamConsumer<List<int>> get stderr => stderrConsumer ?? io.stderr;

  Future<void> _consume(io.Process process) {
    return Future.wait([
      stdout.addStream(process.stdout),
      stderr.addStream(process.stderr),
    ]);
  }
}
