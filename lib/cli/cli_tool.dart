library cli;

import "dart:async";
import "dart:convert";

import "package:meta/meta.dart";
import "package:universal_io/io.dart" as io;
import "package:version/version.dart";

import "../extensions.dart";
import "../io.dart";

export "package:version/version.dart";

export "../semver.dart";

part "cli_consumer.dart";
part "cli_exception.dart";
part "cli_result.dart";

abstract class CliTool {
  CliTool({
    required String executable,
    this.windowsCodePage = WindowsCodePage.utf8,
  }) : originalExecutable = executable {
    if (io.Platform.isWindows && null != windowsCodePage) {
      final batchFile = io.Directory.systemTemp.file(
        "${DateTime.now().microsecondsSinceEpoch}.bat",
      );
      writeWindowsBatchScriptSync(
        file: batchFile,
        contents: """
@echo off
REM Change code page
chcp $windowsCodePage > nul

REM Forward arguments to the actual executable
$executable %*
""",
      );
      this.executable = batchFile.path;
    } else {
      this.executable = executable;
    }
  }

  static const defaultCliEncoding = utf8;

  /// The executable that is used whenever this [CliTool] starts a [io.Process].
  late final String executable;

  /// This is the original executable that was passed to the constructor
  /// of this [CliTool].
  ///
  /// This may be the same as [executable], except if the current platform
  /// is Windows and [windowsCodePage] is non-null.
  ///
  /// Also see [executable].
  final String originalExecutable;

  /// The code page that should be used when executing this [CliTool]
  /// on windows.
  ///
  /// Can be set to null, to use the default code page of the system.
  final int? windowsCodePage;

  Uri? get website => null;

  @protected
  void writeWindowsBatchScriptSync({
    required io.File file,
    required String contents,
    bool changeCodePage = true,
  }) {
    if (!io.Platform.isWindows)
      throw UnsupportedError(
        "This method can only be used on Windows systems.",
      );

    if (changeCodePage) {
      contents = """
@echo off
REM Change code page
chcp $windowsCodePage > nul

$contents
""";
    }

    file.writeAsStringSync(
      contents,
      mode: io.FileMode.writeOnly,
      flush: true,
      encoding: io.systemEncoding,
    );
  }

  @protected
  Future<void> writeWindowsBatchScript({
    required io.File file,
    required String contents,
    bool changeCodePage = true,
  }) async {
    if (!io.Platform.isWindows)
      throw UnsupportedError(
        "This method can only be used on Windows systems.",
      );

    if (changeCodePage) {
      contents = """
@echo off
REM Change code page
chcp $windowsCodePage > nul

$contents
""";
    }

    await file.writeAsString(
      contents,
      mode: io.FileMode.writeOnly,
      flush: true,
      encoding: io.systemEncoding,
    );
  }

  io.ProcessResult runSync(
    List<String> args, {
    String? workingDirectory,
    Encoding encoding = defaultCliEncoding,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) {
    checkExistsSync();

    if (null != workingDirectory) {
      checkDirectoryExists(workingDirectory, "workingDirectory");
    }

    return io.Process.runSync(
      executable,
      args,
      runInShell: true,
      workingDirectory: workingDirectory,
      stderrEncoding: encoding,
      stdoutEncoding: encoding,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );
  }

  Future<io.ProcessResult> runAsync(
    List<String> args, {
    String? workingDirectory,
    Encoding encoding = defaultCliEncoding,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) {
    checkExistsSync();

    if (null != workingDirectory) {
      checkDirectoryExists(workingDirectory, "workingDirectory");
    }

    return io.Process.run(
      executable,
      args,
      runInShell: true,
      workingDirectory: workingDirectory,
      stderrEncoding: encoding,
      stdoutEncoding: encoding,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );
  }

  Future<io.Process> startProcess(
    List<String> args, {
    String? workingDirectory,
    io.ProcessStartMode mode = io.ProcessStartMode.normal,
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    checkExistsSync();

    if (null != workingDirectory) {
      checkDirectoryExists(workingDirectory, "workingDirectory");
    }

    final process = await io.Process.start(
      executable,
      args,
      runInShell: true,
      workingDirectory: workingDirectory,
      mode: mode,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    return process;
  }

  /// Start a new process and consume it using [consumer].
  ///
  /// Returns the exit code of the process.
  Future<int> consumeProcess(
    List<String> args, {
    String? workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
  }) async {
    final process = await startProcess(
      args,
      workingDirectory: workingDirectory,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );

    unawaited(consumer._consume(process));

    final code = await process.exitCode;
    return code;
  }

  /// Start a process in [workingDirectory].
  ///
  /// Returns true if the exit code of the process is [expectedCode],
  /// else false.
  Future<bool> evaluateProcess(
    List<String> args, {
    String? workingDirectory,
    CliStreamConsumer consumer = const CliStreamConsumer(),
    Map<String, String>? environment,
    bool includeParentEnvironment = true,
    int expectedCode = 0,
  }) async {
    final code = await consumeProcess(
      args,
      workingDirectory: workingDirectory,
      consumer: consumer,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
    );
    return expectedCode == code;
  }

  /// Check if [executable] exists, as per [existsSync].
  ///
  /// Throws an [ArgumentError], if [executable] does not exist
  void checkExistsSync() {
    if (existsSync()) return;
    throw ArgumentError.value(
      executable,
      "executable",
      """
The executable does not exist, maybe it is not in your PATH ?
PATH: ${io.Platform.environment["PATH"]}
""",
    );
  }

  /// Synchronously check if [executable] can be found and executed without any
  /// errors.
  ///
  /// This method never throws.
  bool existsSync();

  /// Returns the version of [executable].
  Version getVersionSync();
}

sealed class WindowsCodePage {
  static const utf8 = 65001;
}
