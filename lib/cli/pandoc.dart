import "../io/utils.dart";
import "cli_tool.dart";

final class PandocCliTool extends CliTool {
  PandocCliTool({
    super.executable = "pandoc",
    super.windowsCodePage,
  });

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // pandoc 2.17.1.1
    // Compiled with pandoc-types 1.22.2.1, texmath 0.12.4,
    // skylighting 0.12.3.1, citeproc 0.6.0.1, ipynb 0.2
    // User data directory: /home/emanuel/.local/share/pandoc
    // Copyright (C) 2006-2022 John MacFarlane. Web:  https://pandoc.org
    // This is free software; see the source for copying conditions. There is no
    // warranty, not even for merchantability or fitness for a particular
    // purpose.
    final versionStr =
        runSync(["--version"]).stdout.toString().split("\n").first;

    // Get "major.minor.patch"
    final regex = RegExp(r"\d+\.\d+\.\d+");
    return Version.parse(regex.stringMatch(versionStr)!);
  }
}
