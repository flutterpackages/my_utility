import "package:meta/meta.dart";
import "package:xml/xml.dart";

import "../cli.dart";
import "../io.dart";

export "../src/mariadb_schema.dart";

class MariaDbDumpCliTool extends CliTool {
  MariaDbDumpCliTool({
    super.executable = "mariadb-dump",
    super.windowsCodePage,
  });

  /// The order of [options] matters.
  Future<CliResult> dump({
    required String user,
    required String database,
    String? host,
    int? port,
    String? password,
    Set<DumpOption> options = const {},
    bool produceXml = false,
  }) async {
    final res = await runAsync(
      [
        if (null != host) "--host=$host",
        if (null != port) "--port=$port",
        "--user=$user",
        if (null != password) "--password=$password",
        if (produceXml) "--xml",
        ...options.map((e) => e._commandLineOption),
        database,
      ],
    );
    return CliResult.fromResult(res);
  }

  Future<MariaDbDatabase?> dumpAndParse({
    required String user,
    required String database,
    String? host,
    int? port,
    String? password,
  }) async {
    final dumpRes = await dump(
      user: user,
      database: database,
      host: host,
      port: port,
      password: password,
      produceXml: true,
      options: {
        DumpOption.skipOpt,
        DumpOption.completeInsert,
        DumpOption.noCreateInfo,
        DumpOption.orderByPrimary,
        DumpOption.quick,
      },
    );

    switch (dumpRes) {
      case FailedCliResult():
        return null;

      case SuccessfulCliResult():
    }

    return parseDumpResult(result: dumpRes.stdout, database: database);
  }

  @visibleForTesting
  static MariaDbDatabase parseDumpResult({
    required String result,
    required String database,
  }) {
    final doc = XmlDocument.parse(result);

    // There can only be a single database node, because we explicitly provide
    // the name of the database
    final dbNode = doc.findAllElements("database").singleOrNull;

    if (null == dbNode)
      throw FormatException(
        "The dump result contains no 'database' element",
        result,
      );

    final tableDataNodes = dbNode.findElements("table_data");

    final parsedDb = MariaDbDatabase(name: database);

    // parse each table node
    for (final tblNode in tableDataNodes) {
      final name = tblNode.getAttribute("name");
      if (null == name)
        throw FormatException("Each table must have a name attribute", tblNode);

      final parsedTable = MariaDbTable(name: name);
      final rowNodes = tblNode.findElements("row");

      // If a field can be null, then it has an attribute with this key with
      // the value "true" if it is null.
      const nullAttributeKey = "xsi:nil";

      for (final r in rowNodes) {
        final fields = r.findElements("field");

        for (final f in fields) {
          final colName = f.getAttribute("name");
          if (null == colName)
            throw FormatException("Each field must have a column name", f);

          final isNullAttr = f.getAttribute(nullAttributeKey);
          if (null != isNullAttr &&
              bool.parse(isNullAttr, caseSensitive: false)) {
            parsedTable
                .putIfAbsent(colName, () => MariaDbColumn(name: colName))
                .add(null);
          } else {
            parsedTable
                .putIfAbsent(colName, () => MariaDbColumn(name: colName))
                .add(f.innerText);
          }
        }
      }

      parsedDb[name] = parsedTable;
    }

    return parsedDb;
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // ignore: lines_longer_than_80_chars
    // mariadb-dump  Ver 10.19 Distrib 10.11.4-MariaDB, for debian-linux-gnu (x86_64)
    final versionStr = runSync(["--version"]).stdout.toString().trim();

    // Get "major.minor.patch"
    final regex = RegExp(r"\d+\.\d+\.\d+");
    return Version.parse(regex.stringMatch(versionStr)!);
  }
}

enum DumpOption implements Comparable<DumpOption> {
  skipOpt,

  /// Do not include triggers for each dumped table in the output.
  skipTriggers,

  /// Surround each table dump with LOCK TABLES and UNLOCK TABLES statements.
  /// This results in faster inserts when the dump file is reloaded.
  addLocks,

  /// Use complete INSERT statements that include column names.
  completeInsert,

  /// Use multiple-row INSERT syntax that include several VALUES lists.
  /// This results in a smaller dump file and speeds up inserts when
  /// the file is reloaded.
  extendedInsert,

  /// Do not write CREATE TABLE statements that re-create each dumped table.
  noCreateInfo,

  /// Dump each table´s rows sorted by its primary key, or by its first unique
  /// index, if such an index exists. This is useful when dumping a MyISAM
  /// table to be loaded into an InnoDB table, but will make the dump operation
  /// take considerably longer.
  orderByPrimary,

  /// This option is useful for dumping large tables. It forces `mariadb-dump`
  /// to retrieve rows for a table from the server a row at a time rather
  /// than retrieving the entire row set and buffering it in memory before
  /// writing it out.
  quick;

  @override
  int compareTo(DumpOption other) => other.index - index;

  String get _commandLineOption {
    switch (this) {
      case DumpOption.skipOpt:
        return "--skip-opt";

      case DumpOption.skipTriggers:
        return "--skip-triggers";

      case DumpOption.addLocks:
        return "--add-locks";

      case DumpOption.completeInsert:
        return "--complete-insert";

      case DumpOption.extendedInsert:
        return "--extended-insert";

      case DumpOption.noCreateInfo:
        return "--no-create-info";

      case DumpOption.orderByPrimary:
        return "--order-by-primary";

      case DumpOption.quick:
        return "--quick";
    }
  }
}
