import "dart:async";
import "dart:convert";

import "package:universal_io/io.dart";

import "../cli.dart";
import "../io.dart";

export "../src/models/ffprobe_models.dart";

final class FFProbeCliTool extends CliTool {
  FFProbeCliTool({
    super.executable = "ffprobe",
    super.windowsCodePage,
  });

  @override
  Uri? get website => Uri.parse("https://ffmpeg.org/ffprobe.html");

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["-version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  SemVer getVersionSync() {
    //\ ffprobe version 5.1.4-0+deb12u1 Copyright (c) 2007-2023 the FFmpeg developers
    // built with gcc 12 (Debian 12.2.0-14)
    // configuration: --prefix=/usr --extra-version=0+deb12u1 --toolchain=hardened --libdir=/usr/lib/x86_64-linux-gnu --incdir=/usr/include/x86_64-linux-gnu --arch=amd64 --enable-gpl --disable-stripping --enable-gnutls --enable-ladspa --enable-libaom --enable-libass --enable-libbluray --enable-libbs2b --enable-libcaca --enable-libcdio --enable-libcodec2 --enable-libdav1d --enable-libflite --enable-libfontconfig --enable-libfreetype --enable-libfribidi --enable-libglslang --enable-libgme --enable-libgsm --enable-libjack --enable-libmp3lame --enable-libmysofa --enable-libopenjpeg --enable-libopenmpt --enable-libopus --enable-libpulse --enable-librabbitmq --enable-librist --enable-librubberband --enable-libshine --enable-libsnappy --enable-libsoxr --enable-libspeex --enable-libsrt --enable-libssh --enable-libsvtav1 --enable-libtheora --enable-libtwolame --enable-libvidstab --enable-libvorbis --enable-libvpx --enable-libwebp --enable-libx265 --enable-libxml2 --enable-libxvid --enable-libzimg --enable-libzmq --enable-libzvbi --enable-lv2 --enable-omx --enable-openal --enable-opencl --enable-opengl --enable-sdl2 --disable-sndio --enable-libjxl --enable-pocketsphinx --enable-librsvg --enable-libmfx --enable-libdc1394 --enable-libdrm --enable-libiec61883 --enable-chromaprint --enable-frei0r --enable-libx264 --enable-libplacebo --enable-librav1e --enable-shared
    // libavutil      57. 28.100 / 57. 28.100
    // libavcodec     59. 37.100 / 59. 37.100
    // libavformat    59. 27.100 / 59. 27.100
    // libavdevice    59.  7.100 / 59.  7.100
    // libavfilter     8. 44.100 /  8. 44.100
    // libswscale      6.  7.100 /  6.  7.100
    // libswresample   4.  7.100 /  4.  7.100
    // libpostproc    56.  6.100 / 56.  6.100
    final versionStr = runSync(["-version"]).stdout.toString().trim();

    // only first line is interesting:
    //\ ffprobe version 5.1.4-0+deb12u1 Copyright (c) 2007-2023 the FFmpeg developers
    final versionLine = versionStr.split("\n").first;

    return SemVer.find(versionLine).single;
  }

  Future<FFProbeDataModel> getModel(String filepath) async {
    final resolvedPath = await checkFileExists(filepath).resolveSymbolicLinks();

    final res = await runAsync([
      "-i", resolvedPath,
      "-print_format", "json", //
      "-loglevel", "info", // Printed to stderr
      "-show_streams",
      "-show_format",
    ]);

    if (0 != res.exitCode)
      throw CliResultException(
        exitCode: res.exitCode,
        stderr: res.stderr,
        message: switch (res.exitCode) {
          1 => "The probing probably failed, because the "
              "file at '$resolvedPath' is not a media file",
          _ => null,
        },
      );

    final json = jsonDecode(res.stdout) as Map<String, dynamic>;
    return FFProbeDataModel.fromJson(json);
  }

  /// Get all [MediaFormat]s contained by the [File] at [filepath].
  Future<List<MediaFormat>> getMediaFormats(String filepath) async {
    final model = await getModel(filepath);
    return model.streams.map(MediaFormat.fromStreamModel).toList();
  }
}

/// Audio and Video formats, not all are included.
///
/// See https://en.wikipedia.org/wiki/Video_file_format#List_of_video_file_formats
/// See https://en.wikipedia.org/wiki/Audio_file_format#List_of_formats
enum MediaFormat implements Comparable<MediaFormat> {
  threeGP(
    "Third Generation Partnership Project (3GPP)",
    "https://en.wikipedia.org/wiki/3GP_and_3G2",
    MediaFormatType.muxed,
  ),
  gif(
    "Graphics Interchange Format (GIF)",
    "https://en.wikipedia.org/wiki/Graphics_Interchange_Format",
    MediaFormatType.video,
  ),
  m4a(
    "MPEG-4 Part 14 (Audio-only)",
    "https://en.wikipedia.org/wiki/MPEG-4_Part_14#.MP4_versus_.M4A",
    MediaFormatType.audio,
  ),
  mp3(
    "MPEG-1 Audio Layer III / MPEG-2 Audio Layer III",
    "https://en.wikipedia.org/wiki/MP3",
    MediaFormatType.audio,
  ),
  mp4(
    "MPEG-4 Part 14",
    "https://en.wikipedia.org/wiki/MPEG-4_Part_14",
    MediaFormatType.muxed,
  ),
  oggVorbis(
    "Ogg Vorbis",
    "https://en.wikipedia.org/wiki/Vorbis",
    MediaFormatType.audio,
  ),
  opus(
    "OPUS",
    "https://en.wikipedia.org/wiki/Opus_(audio_format)",
    MediaFormatType.audio,
  ),
  webm(
    "WebM",
    "https://en.wikipedia.org/wiki/WebM",
    MediaFormatType.muxed,
  ),
  eac3(
    "Enhanced AC-3",
    "https://en.wikipedia.org/wiki/Dolby_Digital_Plus",
    MediaFormatType.audio,
  );

  const MediaFormat(
    this.longName,
    this.website,
    this.type,
  );

  factory MediaFormat.fromStreamModel(FFProbeStreamModel model) {
    // TODO(obemu): Implement cases for every MediaFormat.

    if ("0x6134706d" == model.codecTag.toLowerCase()) return MediaFormat.m4a;

    if ("0x31637661" == model.codecTag.toLowerCase()) return MediaFormat.mp4;

    if ("opus" == model.codecName.toLowerCase()) return MediaFormat.opus;

    if ("vorbis" == model.codecName.toLowerCase()) return MediaFormat.oggVorbis;

    if ("0x332d6365" == model.codecTag.toLowerCase() ||
        "eac3" == model.codecName.toLowerCase() ||
        "ATSC A/52B (AC-3, E-AC-3)" == model.codecLongName.toUpperCase())
      return MediaFormat.eac3;

    throw UnimplementedError(
      "There is no MediaFormat for the codec '${model.codecLongName}'",
    );
  }

  final String longName;
  final String website;
  final MediaFormatType type;

  /// Get the file extension for this [MediaFormat].
  String get fileExtension {
    switch (this) {
      case MediaFormat.gif:
      case MediaFormat.m4a:
      case MediaFormat.mp3:
      case MediaFormat.mp4:
      case MediaFormat.webm:
      case MediaFormat.opus:
      case MediaFormat.eac3:
        return name;

      case MediaFormat.threeGP:
        return "3gp";

      case MediaFormat.oggVorbis:
        return "ogg";
    }
  }

  bool get isAudioOnly => MediaFormatType.audio == type;

  bool get isVideoOnly => MediaFormatType.video == type;

  bool get isMuxed => MediaFormatType.muxed == type;

  @override
  int compareTo(MediaFormat other) => index - other.index;
}

enum MediaFormatType {
  video,
  audio,
  muxed,
}
