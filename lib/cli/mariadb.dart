import "package:universal_io/io.dart";

import "../cli.dart";
import "../extensions.dart";
import "../io.dart";

final class MariaDbCliTool extends CliTool {
  MariaDbCliTool({
    super.executable = "mariadb",
    super.windowsCodePage,
  });

  Future<CliResult> _execute({
    required String statement,
    required String user,
    required String database,
    String? host,
    int? port,
    String? password,
    String? workingDirectory,
    List<String> extraArgs = const [],
  }) async {
    final res = await runAsync(
      [
        if (null != host) "--host=$host",
        if (null != port) "--port=$port",
        "--user=$user",
        if (null != password) "--password=$password",
        ...extraArgs,
        "--execute=$statement",
        database,
      ],
      workingDirectory: workingDirectory,
    );
    return CliResult.fromResult(res);
  }

  Future<Process> connect({
    required String user,
    required String database,
    String? host,
    int? port,
    String? password,
    bool inheritStdio = false,
  }) {
    return startProcess(
      [
        if (null != host) "--host=$host",
        if (null != port) "--port=$port",
        "--user=$user",
        if (null != password) "--password=$password",
        database,
      ],
      mode: inheritStdio
          ? ProcessStartMode.inheritStdio
          : ProcessStartMode.normal,
    );
  }

  Future<CliResult> restore({
    required String user,
    required String database,
    required String backupFilepath,
    String? host,
    int? port,
    String? password,
  }) async {
    final backupFile =
        await checkFileExists(backupFilepath, "backupFilepath").resolveUri();

    final lines = await backupFile.readAsLines();

// -- MariaDB dump 10.19  Distrib 10.11.4-MariaDB, for debian-linux-gnu (x86_64)
// --
// -- Host: localhost    Database: dev_database
// -- ------------------------------------------------------
    if (!lines[0].startsWith("-- MariaDB dump"))
      throw ArgumentError.value(
        backupFile.path,
        "backupFilepath",
        "The backup file must be a MariaDB dump file",
      );

    final l = lines[2];
    final hostMatch =
        RegExp("Host: (?<host>[A-z0-9.]*)").firstMatch(l)?.namedGroup("host");
    final dbNameMatch = RegExp("Database: (?<dbName>[A-z0-9.]*)")
        .firstMatch(l)
        ?.namedGroup("dbName");

    if (null == hostMatch)
      throw ArgumentError.value(
        backupFile.path,
        "backupFilepath",
        "The backup file must be a MariaDB dump file that has "
            "host value",
      );

    if (null == dbNameMatch)
      throw ArgumentError.value(
        backupFile.path,
        "backupFilepath",
        "The backup file must be a MariaDB dump file that has "
            "database value",
      );

    if (database != dbNameMatch)
      throw ArgumentError.value(
        backupFile.path,
        "backupFilepath",
        "The backup file is not a backup of the database '$database' but of "
            "another database called '$dbNameMatch'",
      );

    return runSqlScript(
      host: host,
      port: port,
      user: user,
      database: database,
      scriptFilepath: backupFilepath,
      password: password,
    );
  }

  Future<CliResult> runSqlScript({
    required String user,
    required String database,
    required String scriptFilepath,
    String? host,
    int? port,
    String? password,
  }) async {
    // The path seperator must be '/' for the "source filename" statement.
    final posixScriptFilepath =
        checkFileExists(scriptFilepath, "scriptFilepath").absolutePosixPath;

    return _execute(
      // We do not need to encapsulate the script filepath in double/single
      // quotes. Actually doing that only causes errors.
      statement: "source $posixScriptFilepath;",
      user: user,
      database: database,
      password: password,
      host: host,
      port: port,
      extraArgs: [
        "--abort-source-on-error",
      ],
    );
  }

  Future<CliResult> execute({
    required String statement,
    required String user,
    required String database,
    String? host,
    int? port,
    String? password,
    String? workingDirectory,
    bool produceXml = false,
  }) {
    return _execute(
      statement: statement,
      user: user,
      database: database,
      password: password,
      workingDirectory: workingDirectory,
      host: host,
      port: port,
      extraArgs: [
        if (produceXml) "--xml",
      ],
    );
  }

  @override
  bool existsSync() {
    final res = tryProcessRunSync(
      executable,
      ["--version"],
      runInShell: true,
    );
    return 0 == res?.exitCode;
  }

  @override
  Version getVersionSync() {
    // ignore: lines_longer_than_80_chars
    // mariadb  Ver 15.1 Distrib 10.11.4-MariaDB, for debian-linux-gnu (x86_64) using  EditLine wrapper
    final versionStr = runSync(["--version"]).stdout.toString().trim();

    // Get "major.minor.patch"
    final regex = RegExp(r"\d+\.\d+\.\d+");
    return Version.parse(regex.stringMatch(versionStr)!);
  }
}
