class OutOfTriesException implements Exception {
  const OutOfTriesException({
    required this.tries,
    this.message,
  });

  /// Description of the cause why there are no more tries.
  final String? message;

  /// The amount of tries that were exceeded.
  final int tries;

  @override
  String toString() {
    var result = "Ran out of tries after $tries ";
    result += (1 == tries) ? "time" : "times";

    if (message != null) result = "$result: $message";

    return result;
  }
}
