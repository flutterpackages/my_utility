import "dart:async";

import "package:ansi_colorizer/ansi_colorizer.dart";
import "package:universal_io/io.dart";

import "io.dart";
import "typedefs/result_callback.dart";

bool _shouldColorize = true;

const infoColor = AnsiColorizer(foreground: Ansi8BitColor(46)); // green
const warningColor = AnsiColorizer(foreground: Ansi8BitColor(208)); // orange
const errorColor = AnsiColorizer(foreground: Ansi8BitColor(196)); // red

void setPrintersColorUsage(bool enable) {
  _shouldColorize = enable;
}

String _getMsg(AnsiColorizer color, String tag, Object? object) {
  final msg = "[$tag]: $object";
  if (!_shouldColorize) return msg;
  return color(msg);
}

//

void printInfo(Object? object, [String end = "\n"]) {
  final msg = _getMsg(infoColor, "INFO", object);
  printOut(msg, end);
}

void printWarning(Object? object, [String end = "\n"]) {
  final msg = _getMsg(warningColor, "WARNING", object);
  printErr(msg, end);
}

void printError(Object? object, [String end = "\n"]) {
  final msg = _getMsg(errorColor, "ERROR", object);
  printErr(msg, end);
}

/// Write a progess animation to [output] while waiting for [workload].
///
/// [workload] must not write to [output], otherwise the animation will
/// be interrupted.
///
/// [output] defaults to [stdout].
Future<T> printProgessAnimation<T>(
  FutureOrResultCallback<T> workload, {
  Duration animationSpeed = const Duration(milliseconds: 150),
  StringSink? output,
}) async {
  const positions = ["|", "/", "-", r"\", "|", "/", "-", r"\"];
  var idx = 0;

  // ignore: close_sinks
  final oSink = output ?? stdout;

  final timer = Timer.periodic(animationSpeed, (_) {
    oSink.write("${positions[idx]} \r");

    idx++;
    if (idx >= positions.length) {
      idx = 0;
    }
  });

  T res;
  try {
    res = await workload();
  } finally {
    timer.cancel();
  }

  return res;
}
