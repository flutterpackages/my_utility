import "dart:async";

import "package:collection/collection.dart";
import "package:test/expect.dart";

export "package:test/expect.dart";

void expectIterable<T>(
  Iterable<T>? actual,
  Iterable<T>? matcher, {
  bool checkOrder = true,
  dynamic skip,
  Equality<T>? baseEquality,
}) {
  expect(
    actual?.length,
    matcher?.length,
    reason: "Not the same length",
  );

  final theEquality = checkOrder
      ? DeepCollectionEquality(baseEquality ?? DefaultEquality<T>())
      : DeepCollectionEquality.unordered(baseEquality ?? DefaultEquality<T>());

  var reason = "The iterables do not have the same elements";
  if (checkOrder) {
    reason += " in the same order";
  }

  expectTrue(
    theEquality.equals(actual, matcher),
    skip: skip,
    reason: reason,
  );
}

void expectTrue(
  bool expression, {
  String? reason,
  dynamic skip, // true or a String
}) {
  expect(
    expression,
    isTrue,
    reason: reason,
    skip: skip,
  );
}

void expectFalse(
  bool expression, {
  String? reason,
  dynamic skip, // true or a String
}) {
  expect(
    expression,
    isFalse,
    reason: reason,
    skip: skip,
  );
}

void expectNull(
  dynamic nullable, {
  String? reason,
  dynamic skip, // true or a String
}) {
  expect(
    nullable,
    isNull,
    reason: reason,
    skip: skip,
  );
}

void expectNonNull(
  dynamic nullable, {
  String? reason,
  dynamic skip, // true or a String
}) {
  expect(
    nullable,
    isNotNull,
    reason: reason,
    skip: skip,
  );
}

void expectError<T>(
  FutureOr<void> Function() closure, {
  String? reason,
  dynamic skip, // true or a String
}) {
  expect(
    closure,
    throwsA(isA<T>()),
    reason: reason,
    skip: skip,
  );
}

void expectDoubleWithinBounds(
  double actual,
  double lower,
  double upper, {
  String? reason,
  dynamic skip, // true or a String
}) {
  expect(
    actual,
    greaterThanOrEqualTo(lower),
    reason: reason,
    skip: skip,
  );
  expect(
    actual,
    lessThanOrEqualTo(upper),
    reason: reason,
    skip: skip,
  );
}

void expectType<T>(
  dynamic actual, {
  String? reason,
  dynamic skip, // true or a String
}) {
  expect(actual, isA<T>());
}

void expectRegexMatch(
  String value,
  RegExp regex, {
  dynamic skip, // true or a String
}) {
  final matcher = predicate<String>((obj) => regex.hasMatch(obj));
  expect(
    value,
    matcher,
    skip: skip,
    reason: "Does not match the regular expression: ${regex.pattern}",
  );
}
