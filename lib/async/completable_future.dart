import "dart:async";

import "../typedefs/result_callback.dart";

@Deprecated(
  "CompletableFuture will be removed in 2.0.0. "
  "Use ResultFuture instead.\n"
  "See package:my_utility/async/result_future.dart",
)
final class CompletableFuture<T> implements Future<T> {
  @Deprecated(
    "CompletableFuture will be removed in 2.0.0. "
    "Use ResultFuture instead.\n"
    "See package:my_utility/async/result_future.dart",
  )
  factory CompletableFuture(FutureOrResultCallback<T> computation) {
    return CompletableFuture.fromFuture(Future(computation));
  }

  /// Same as [Future.value].
  @Deprecated(
    "CompletableFuture will be removed in 2.0.0. "
    "Use ResultFuture with Future.value instead.\n"
    "See package:my_utility/async/result_future.dart",
  )
  factory CompletableFuture.value([FutureOr<T>? value]) {
    return CompletableFuture.fromFuture(Future.value(value));
  }

  /// Same as [Future.delayed].
  @Deprecated(
    "CompletableFuture will be removed in 2.0.0. "
    "Use ResultFuture with Future.delayed instead.\n"
    "See package:my_utility/async/result_future.dart",
  )
  factory CompletableFuture.delayed(
    Duration duration, [
    FutureOrResultCallback<T>? computation,
  ]) {
    return CompletableFuture.fromFuture(Future.delayed(duration, computation));
  }

  @Deprecated(
    "CompletableFuture will be removed in 2.0.0. "
    "Use ResultFuture instead.\n"
    "See package:my_utility/async/result_future.dart",
  )
  CompletableFuture.fromFuture(this._future) {
    _future.then(
      (v) => _result = CompletableFutureValue._(v),
      onError: (err, st) => _result = CompletableFutureError._(err, st),
    );
  }

  final Future<T> _future;

  /// The result of this [CompletableFuture].
  ///
  /// Trying to get the result, while [isCompleted] is false
  /// will throw a [StateError].
  CompletableFutureResult<T> get result {
    if (!isCompleted)
      throw StateError("This CompletableFuture is not completed");
    return _result!;
  }

  CompletableFutureResult<T>? _result;

  /// Whether this [CompletableFuture] has completed with
  /// either a value or an error.
  bool get isCompleted => null != _result;

  /// True if this [CompletableFuture] has completed with a
  /// value and not an error.
  ///
  /// If this is true, then [result] is of type [CompletableFutureValue].
  ///
  /// This is the opposite of [hasCompletedWithError].
  bool get hasCompletedWithValue => _result is CompletableFutureValue<T>;

  /// True if this [CompletableFuture] has completed with an
  /// error and not a value.
  ///
  /// If this is true, then [result] is of type [CompletableFutureError].
  ///
  /// This is the opposite of [hasCompletedWithValue].
  bool get hasCompletedWithError => _result is CompletableFutureError<T>;

  /// The value result of this [CompletableFuture].
  ///
  /// Trying to get the result, while [isCompleted] or [hasCompletedWithValue]
  /// is false will throw a [StateError].
  T get value {
    switch (result) {
      case final CompletableFutureValue<T> v:
        return v.value;

      case CompletableFutureError<T>():
        throw StateError(
          "This CompletableFuture completed with an error "
          "and not a value.",
        );
    }
  }

  @override
  Stream<T> asStream() => _future.asStream();

  @override
  Future<T> catchError(Function onError, {bool Function(Object error)? test}) {
    return _future.catchError(onError, test: test);
  }

  @override
  Future<R> then<R>(
    FutureOr<R> Function(T value) onValue, {
    Function? onError,
  }) {
    return _future.then(onValue, onError: onError);
  }

  @override
  Future<T> timeout(Duration timeLimit, {FutureOr<T> Function()? onTimeout}) {
    return _future.timeout(timeLimit, onTimeout: onTimeout);
  }

  @override
  Future<T> whenComplete(FutureOr<void> Function() action) {
    return _future.whenComplete(action);
  }
}

//

@Deprecated(
  "CompletableFuture and CompletableFutureResult will be removed in 2.0.0. "
  "Use ResultFuture instead.\n"
  "See package:my_utility/async/result_future.dart",
)
sealed class CompletableFutureResult<T> {
  @Deprecated(
    "CompletableFuture and CompletableFutureResult will be removed in 2.0.0. "
    "Use ResultFuture instead.\n"
    "See package:my_utility/async/result_future.dart",
  )
  const CompletableFutureResult();
}

@Deprecated(
  "CompletableFuture and CompletableFutureResult will be removed in 2.0.0. "
  "Use ResultFuture instead.\n"
  "See package:my_utility/async/result_future.dart",
)
final class CompletableFutureValue<T> extends CompletableFutureResult<T> {
  @Deprecated(
    "CompletableFuture and CompletableFutureResult will be removed in 2.0.0. "
    "Use ResultFuture instead.\n"
    "See package:my_utility/async/result_future.dart",
  )
  CompletableFutureValue._(this.value);

  final T value;
}

@Deprecated(
  "CompletableFuture and CompletableFutureResult will be removed in 2.0.0. "
  "Use ResultFuture instead.\n"
  "See package:my_utility/async/result_future.dart",
)
final class CompletableFutureError<T> extends CompletableFutureResult<T> {
  @Deprecated(
    "CompletableFuture and CompletableFutureResult will be removed in 2.0.0. "
    "Use ResultFuture instead.\n"
    "See package:my_utility/async/result_future.dart",
  )
  CompletableFutureError._(this.error, this.stackTrace);

  final Object error;
  final StackTrace stackTrace;
}

//

@Deprecated(
  "CompletableFuture will be removed in 2.0.0. "
  "Use ResultFuture instead.\n"
  "See package:my_utility/async/result_future.dart",
)
extension CompletableFutureExtension<T> on Future<T> {
  CompletableFuture<T> asCompletableFuture() =>
      CompletableFuture.fromFuture(this);
}
