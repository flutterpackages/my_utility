import "dart:async";

import "package:dartx/dartx.dart";
import "package:meta/meta.dart";

import "../extensions/symbol.dart";

/// Executes the provided [Function] exactly once, even if called concurrently.
final class CallOnce<R> {
  CallOnce(this._closure)
      : _wasCalled = false,
        _completer = Completer();

  final Function _closure;

  final Completer<R> _completer;

  bool _wasCalled;

  /// True if this [CallOnce] has already been called.
  ///
  /// This flag does not take into account if the provided
  /// closure finished without errors, or if it finished
  /// at all.
  bool get wasCalled => _wasCalled;

  /// True if the asynchronous call to this [CallOnce] is
  /// already completed.
  bool get isCompleted => _completer.isCompleted;

  /// The cached result from [call].
  Future<R> get result => _completer.future;

  /// Call the closure, which was provided as an argument to the [CallOnce]
  /// constructor, with the specified arguments.
  ///
  /// Returns the result of calling [_closure]. The result is cached,
  /// so any subsequent calls return the cached result.
  ///
  /// ### Errors
  ///
  /// * Throws a [CallOnceInvalidArgumentsError] if the provided arguments
  ///   do not match the signature of [_closure].
  ///
  /// * Throws a [CallOnceClosureResultError] if the result returned
  ///   by [_closure] is not of type `R` or `Future<R>`.
  Future<R> call([
    List<dynamic>? positionalArguments,
    Map<Symbol, dynamic>? namedArguments,
  ]) async {
    if (_wasCalled) return _completer.future;
    _wasCalled = true;

    final dynamic closureResult;

    try {
      closureResult = Function.apply(
        _closure,
        positionalArguments,
        namedArguments,
      );
      // ignore: avoid_catching_errors
    } on NoSuchMethodError catch (_) {
      throw CallOnceInvalidArgumentsError._(
        _closure,
        positionalArguments,
        namedArguments,
      );
    }

    // Ensure that closureResult is either R or Future<R>
    if (closureResult is! R && closureResult is! Future<R>)
      throw CallOnceClosureResultError<R>._(
        closure: _closure,
        namedArguments: namedArguments,
        positionalArguments: positionalArguments,
        result: closureResult,
      );

    final R res = await closureResult;
    _completer.complete(res);

    return res;
  }
}

sealed class CallOnceError extends Error {
  CallOnceError._(this._closure);

  @protected
  final Function _closure;

  @protected
  static String _argsToString(
    List<dynamic>? positionalArguments,
    Map<Symbol, dynamic>? namedArguments,
  ) {
    String posToString(List<dynamic> args) {
      return args.mapIndexed((i, e) => "\t$i: $e").join("\n");
    }

    String namedToString(Map<Symbol, dynamic> args) {
      return args.entries.map((e) => "${e.key.name}: ${e.value}").join("\n");
    }

    return switch ((positionalArguments, namedArguments)) {
      (null, null) => "No arguments were provided",
      (final List<dynamic> posArgs, null) =>
        "The following positional arguments were provided:\n"
            "${posToString(posArgs)}",
      (null, final Map<Symbol, dynamic> namedArgs) =>
        "The following named arguments were provided:\n"
            "${namedToString(namedArgs)}",
      (final List<dynamic> posArgs, final Map<Symbol, dynamic> namedArgs) =>
        "The following positional and named arguments were provided:\n"
            "${posToString(posArgs)}\n\n"
            "${namedToString(namedArgs)}",
    };
  }
}

class CallOnceClosureResultError<R> extends CallOnceError implements TypeError {
  CallOnceClosureResultError._({
    required dynamic result,
    required Function closure,
    required List<dynamic>? positionalArguments,
    required Map<Symbol, dynamic>? namedArguments,
  })  : _result = result,
        _positionalArguments = positionalArguments,
        _namedArguments = namedArguments,
        super._(closure);

  final dynamic _result;
  final List<dynamic>? _positionalArguments;
  final Map<Symbol, dynamic>? _namedArguments;

  @override
  String toString() {
    final buf = StringBuffer("CallOnceResultTypeError: ");

    buf.write("The closure did not return a result of type '$R', ");
    buf.writeln("but of type ${_result.runtimeType}.");

    buf.writeln("The closure is: '${_closure.runtimeType}'\n");

    buf.writeln(
      CallOnceError._argsToString(_positionalArguments, _namedArguments),
    );

    return buf.toString();
  }
}

class CallOnceInvalidArgumentsError extends CallOnceError {
  CallOnceInvalidArgumentsError._(
    super._closure,
    this._positionalArguments,
    this._namedArguments,
  ) : super._();

  final List<dynamic>? _positionalArguments;
  final Map<Symbol, dynamic>? _namedArguments;

  @override
  String toString() {
    final buf = StringBuffer("CallOnceInvalidArgumentsError: ");

    buf.writeln("The closure was called with mismatched arguments.");

    buf.writeln("The closure is: '${_closure.runtimeType}'\n");

    buf.writeln(
      CallOnceError._argsToString(_positionalArguments, _namedArguments),
    );

    return buf.toString();
  }
}
