import "dartx.dart";

export "package:dartx/dartx.dart";

final class ConstRange<T extends num> extends Range<T> {
  const ConstRange(this.start, this.endInclusive);

  const ConstRange.single(T value)
      : start = value,
        endInclusive = value;

  static const zero = ConstRange.single(0);

  @override
  final T start;

  @override
  final T endInclusive;
}
