import "package:collection/collection.dart";

import "../dartx.dart";
import "../extensions/int.dart";
import "../functions/is_nullable_type.dart";
import "../test.dart";

final class ArgumentErrorUtils extends ArgumentError {
  ArgumentErrorUtils._value(
    value, {
    String? name,
    String? message,
    String? extraMessage,
  })  : _extraMessage = extraMessage,
        super.value(value, name, message);

  final String? _extraMessage;

  /// Throws if [value] is `null`, or if [value] is not an unsigned
  /// 32 bit integer.
  ///
  /// If [name] is supplied, it is used as the parameter name
  /// in the error message.
  ///
  /// Returns the [value] if it is not null and if it is an
  /// unsigned 32 bit integer.
  static int checkUint32(Object? value, [String? name]) {
    final _value = ArgumentErrorUtils.checkType<int>(value, name: name);
    if (_value.isUint32) return _value;
    throw ArgumentError.value(
      _value,
      name,
      "Must be an unsigned 32 bit integer.",
    );
  }

  /// Throws if [value] is `null`, or if [value] is not an unsigned
  /// integer of [size].
  ///
  /// If [name] is supplied, it is used as the parameter name
  /// in the error message.
  ///
  /// Returns the [value] if it is not null and if it is an
  /// unsigned integer of [size].
  static int checkUnsignedIntegerOfSize(
    Object? value,
    int size, [
    String? name,
  ]) {
    final integer = ArgumentErrorUtils.checkType<int>(value, name: name);
    if (integer.isUnsigned(size)) return integer;
    throw ArgumentError.value(
      value,
      name,
      "Must be an unsigned integer of size $size",
    );
  }

  /// Check that every integer in [iterable] is unsigned and of size [size].
  static T checkIterableOfUnsignedIntegerOfSize<T extends Iterable<int>>(
    T iterable,
    int size, [
    String? name,
  ]) {
    var i = 0;
    for (final item in iterable) {
      if (!item.isUnsigned(size))
        throw ArgumentError.value(
          iterable,
          "${name ?? ""}[$i]",
          "The element must be an unsigned integer of size $size",
        );

      i++;
    }

    return iterable;
  }

  /// Check that every integer in [iterable] is unsigned and of size `4`.
  ///
  /// See [checkIterableOfUnsignedIntegerOfSize].
  static T checkIterableOfUint32<T extends Iterable<int>>(
    T iterable, [
    String? name,
  ]) {
    return checkIterableOfUnsignedIntegerOfSize<T>(iterable, 4, name);
  }

  /// Throws if [iterable] is `null`, or if the content of [iterable]
  /// does not equal [match].
  ///
  /// If [name] is supplied, it is used as the parameter name
  /// in the error message.
  ///
  /// Returns the [iterable] if it is not null and if its
  /// content equals [match].
  static T checkIterableEqual<T extends Iterable<dynamic>>(
    T? iterable,
    T match, {
    bool checkOrder = false,
    String? name,
  }) {
    final _iterable = ArgumentError.checkNotNull(iterable);

    final equality = checkOrder
        ? const DeepCollectionEquality()
        : const DeepCollectionEquality.unordered();
    if (equality.equals(_iterable, match)) return _iterable;

    throw ArgumentErrorUtils._value(
      _iterable,
      name: name,
      message:
          "${name ?? "The iterable"} does not have the same content as match",
      extraMessage: "\nmatch:\n${match.join("\n")}\n\n"
          "${name ?? "iterable"}:\n${_iterable.join("\n")}",
    );
  }

  /// Throws if [iterable] is `null`, or if [iterable] does not contain
  /// every element of [requiredElements].
  ///
  /// If [name] is supplied, it is used as the parameter name
  /// in the error message.
  ///
  /// Returns the [iterable] if it is not null and if it contains
  /// all elements of [requiredElements].
  static T checkIterableContains<T extends Iterable<dynamic>>(
    T? iterable,
    T requiredElements, {
    String? name,
  }) {
    final _iterable = ArgumentError.checkNotNull(iterable);

    if (_iterable.containsAll(requiredElements)) return _iterable;

    throw ArgumentErrorUtils._value(
      _iterable,
      name: name,
      message:
          "${name ?? "The iterable"} does not contain all required elements",
      extraMessage: "\nrequired elements:\n${requiredElements.join("\n")}\n\n"
          "${name ?? "iterable"}:\n${_iterable.join("\n")}",
    );
  }

  /// Check that [iterable] is not empty.
  static T checkIterableNotEmpty<T extends Iterable<dynamic>>(
    T iterable, [
    String? name,
  ]) {
    if (iterable.isNotEmpty) return iterable;
    throw ArgumentError.value(iterable, name, "Must not be empty");
  }

  /// Check if [value] is of type `T`.
  ///
  /// If [matcher] is non-null, then it will be used to check the type,
  /// else a new [TypeMatcher] returned by calling `isA<T>()` gets used.
  ///
  /// See:
  ///   - [isA]
  static T checkType<T>(
    Object? value, {
    TypeMatcher<T>? matcher,
    String? name,
  }) {
    if (isNullableType<T>()) {
      if (null == value) return null as T;
    } else {
      // ignore: parameter_assignments
      value = ArgumentError.checkNotNull(value, name);
    }

    final _matcher = matcher ?? isA<T>();

    if (_matcher.matches(value, {})) return value as T;

    final desc = _matcher.describeMismatch(
      value,
      StringDescription(),
      {},
      false,
    ) as StringDescription;
    throw ArgumentError.value(
      value,
      name,
      desc.toString(),
    );
  }

  /// Check that [value] is null.
  static void checkNull(Object? value, [String? name]) {
    if (null == value) return;
    throw ArgumentError.value(value, name, "Must be null");
  }

  /// Throws if [argument] is `null`.
  ///
  /// If [name] is supplied, it is used as the parameter name
  /// in the error message.
  ///
  /// Returns the [argument] if it is not null.
  static T checkNotNull<T extends Object>(
    T? argument, {
    String? name,
    String? message,
  }) {
    if (null != argument) return argument;
    throw ArgumentErrorUtils._value(
      argument,
      name: name,
      message: "Must not be null",
      extraMessage: message,
    );
  }

  /// Check if `value.runtimeType` is equal to [type].
  static void checkRuntimeType(
    Object? value,
    Type type, {
    String? name,
  }) {
    if (value.runtimeType == type) return;

    throw ArgumentError.value(
      value,
      name,
      "Runtime type is not $type",
    );
  }

  @override
  String toString() {
    var s = super.toString();
    if (null != _extraMessage) {
      s += "\n$_extraMessage";
    }
    return s;
  }
}
