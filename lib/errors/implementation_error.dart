import "../reflection/program_info.dart";

final class ImplementationError extends Error {
  ImplementationError(
    this.method, [
    this.message,
  ]);

  ImplementationError.current([
    this.message,
  ]) : method = ProgramInfo.withFrameOffset(2).function;

  final String method;
  final String? message;

  @override
  String toString() {
    final s = "The implementation of the method <$method> caused an error";
    if (null == message) return s;
    return "$s: $message";
  }
}
