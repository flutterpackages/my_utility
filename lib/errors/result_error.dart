/// Error thrown when a function returns an unacceptable result.
class ResultError extends Error {
  ResultError(
    this.invalidResult, {
    this.functionName,
    this.description,
  });

  /// The name of the function that returned the invalid result.
  final String? functionName;

  /// The invalid result.
  final dynamic invalidResult;

  /// An explanation as to why [invalidResult] is invalid.
  final String? description;

  @override
  String toString() {
    final msg = (null != functionName)
        ? "The function '$functionName' returned an invalid result"
        : "A function returned an invalid result";

    if (null == description) return "$msg: $invalidResult";

    return "$msg.\n$description:\n\n$invalidResult";
  }
}
