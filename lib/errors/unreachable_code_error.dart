final class UnreachableCodeError extends Error {
  @override
  String toString() =>
      "There should be no way to arrive at this position in the code.";
}
