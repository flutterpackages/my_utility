import "package:universal_io/io.dart";

import "../extensions/directory.dart";
import "../extensions/file_system_entity.dart";

/// Copy the file at [path] into [target].
///
/// Returns null if [path] does not lead to a [File],
/// else the newly created [File] gets returned.
Future<File?> copyFile(String path, Directory target) async {
  // ignore: avoid_slow_async_io
  if (!await FileSystemEntity.isFile(path)) return null;
  final source = File(path).absolute;
  final actualTargetFilepath = target.file(source.name).absolute.path;
  return source.copy(actualTargetFilepath);
}

/// Copy the file at [path] into [target].
///
/// Returns null if [path] does not lead to a [File],
/// else the newly created [File] gets returned.
File? copyFileSync(String path, Directory target) {
  if (!FileSystemEntity.isFileSync(path)) return null;
  final source = File(path).absolute;
  final actualTargetFilepath = target.file(source.name).absolute.path;
  return source.copySync(actualTargetFilepath);
}

//

/// Copy the directory at [path] recursively into [target].
///
/// Returns null if [path] does not lead to a [Directory],
/// else the newly created [Directory] gets returned.
Future<Directory?> copyDirectory(String path, Directory target) async {
  // ignore: avoid_slow_async_io
  if (!await FileSystemEntity.isDirectory(path)) return null;
  final source = Directory(path).absolute;
  final actualTargetDir = target.directory(source.name).absolute;
  await source.copyRecursively(actualTargetDir);
  return actualTargetDir;
}

/// Copy the directory at [path] recursively into [target].
///
/// Returns null if [path] does not lead to a [Directory],
/// else the newly created [Directory] gets returned.
Directory? copyDirectorySync(String path, Directory target) {
  if (!FileSystemEntity.isDirectorySync(path)) return null;
  final source = Directory(path).absolute;
  final actualTargetDir = target.directory(source.name).absolute;
  source.copyRecursivelySync(actualTargetDir);
  return actualTargetDir;
}
