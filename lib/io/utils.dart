import "dart:convert";

import "package:universal_io/io.dart";

/// Tries to synchronously run the [executable] with the provided [args].
///
/// Returns null if [Process.runSync] throws a [ProcessException].
ProcessResult? tryProcessRunSync(
  String executable,
  List<String> args, {
  String? workingDirectory,
  Map<String, String>? environment,
  bool includeParentEnvironment = true,
  bool runInShell = false,
  Encoding? stdoutEncoding = systemEncoding,
  Encoding? stderrEncoding = systemEncoding,
  void Function(
    ProcessException exception,
    StackTrace stackTrace,
  )? onProcessException,
}) {
  try {
    return Process.runSync(
      executable,
      args,
      workingDirectory: workingDirectory,
      environment: environment,
      includeParentEnvironment: includeParentEnvironment,
      runInShell: runInShell,
      stdoutEncoding: stdoutEncoding,
      stderrEncoding: stderrEncoding,
    );
  } on ProcessException catch (exp, st) {
    onProcessException?.call(exp, st);
    return null;
  }
}

//

/// Returns true if any file or directory exists at [path].
bool existsSync(String path) {
  return FileSystemEntity.typeSync(path) != FileSystemEntityType.notFound;
}

void deleteSync(String path) {
  final type = FileSystemEntity.typeSync(path);
  switch (type) {
    case FileSystemEntityType.directory:
      Directory(path).deleteSync(recursive: true);
      break;

    case FileSystemEntityType.file:
    case FileSystemEntityType.link:
    case FileSystemEntityType.pipe:
    case FileSystemEntityType.unixDomainSock:
      File(path).deleteSync();
      break;

    case FileSystemEntityType.notFound:
      break;
  }
}
