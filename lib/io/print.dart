import "package:universal_io/io.dart";

/// Prints a string representation of [object] to [stdout].
///
/// Appends [end] to the string representation.
void printOut(Object? object, [String end = "\n"]) {
  stdout.write("$object$end");
}

/// Prints a string representation of [object] to [stderr].
///
/// Appends [end] to the string representation.
void printErr(Object? object, [String end = "\n"]) {
  stderr.write("$object$end");
}
