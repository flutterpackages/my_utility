final class IndentedStringBuffer extends StringBuffer {
  IndentedStringBuffer({
    Object content = "",
    this.indent = "",
  }) : super(content);

  /// The indent, that is used by the various `write*Indented()` methods.
  String indent;

  void _writeIndent(int n) {
    for (var i = 0; i < n; i++) {
      write(indent);
    }
  }

  /// Same as [write], but prepends [indent] to [object].
  void writeIndented(Object? object, [int indentFactor = 1]) {
    _writeIndent(indentFactor);
    write(object);
  }

  /// Same as [writeAll], but prepends [indent] to every value from [objects].
  void writeAllIndented(
    Iterable<dynamic> objects, [
    String separator = "",
    int indentFactor = 1,
  ]) {
    var i = 0;
    final length = objects.length;

    for (final item in objects) {
      _writeIndent(indentFactor);
      write(item);
      if (i < length - 1) {
        write(separator);
      }

      i++;
    }
  }

  /// Same as [writeCharCode], but prepends [indent] to the character.
  void writeCharCodeIndented(int charCode, [int indentFactor = 1]) {
    _writeIndent(indentFactor);
    writeCharCode(charCode);
  }

  /// Same as [writeln], but prepends [indent] to [object].
  void writelnIndented([Object? object = "", int indentFactor = 1]) {
    _writeIndent(indentFactor);
    writeln(object);
  }
}
