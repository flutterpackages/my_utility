import "package:universal_io/io.dart";

import "../io.dart";

/// Check if the [File] at [path] exists, if not
/// throw an [ArgumentError].
File checkFileExists(
  String path, [
  String? name,
]) {
  final type = FileSystemEntity.typeSync(path);

  if (FileSystemEntityType.notFound == type)
    throw ArgumentError.value(
      path,
      name,
      "The file does not exist",
    );

  if (FileSystemEntityType.file != type)
    throw ArgumentError.value(
      path,
      name,
      "The provided path does not lead to a file, but to a $type",
    );

  return File(path);
}

/// Check if the [Directory] at [path] exists, if not
/// throw an [ArgumentError].
Directory checkDirectoryExists(
  String path, [
  String? name,
]) {
  final type = FileSystemEntity.typeSync(path);

  if (FileSystemEntityType.notFound == type)
    throw ArgumentError.value(
      path,
      name,
      "The directory does not exist",
    );

  if (FileSystemEntityType.directory != type)
    throw ArgumentError.value(
      path,
      name,
      "The provided path does not lead to a directory, but to a $type",
    );

  return Directory(path);
}

/// Check that [path] leads to a [FileSystemEntity] of [type].
///
/// Exits the program if [path] does not lead to a
/// [FileSystemEntity] of [type].
void checkLeadsToFiletype(String path, FileSystemEntityType type) {
  final filetype = FileSystemEntity.typeSync(path);

  if (filetype == type) return;

  final message = switch (filetype) {
    FileSystemEntityType.notFound =>
      "Aborting because the path '$path' does not exist",
    _ => "Aborting because the path '$path' does not lead to a $type "
        "but to a $filetype."
  };

  printErr(message);
  exit(1);
}
