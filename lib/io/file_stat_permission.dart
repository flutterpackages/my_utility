enum FileStatPermission implements Comparable<FileStatPermission> {
  execute(0x01),
  write(0x02),
  read(0x04);

  const FileStatPermission(this.mask);

  final int mask;

  @override
  int compareTo(FileStatPermission other) => other.index - index;
}

const read = FileStatPermission.read;
const write = FileStatPermission.write;
const execute = FileStatPermission.execute;
