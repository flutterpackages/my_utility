export "extensions/binary_prefix.dart";

/// A [binary prefix](https://en.wikipedia.org/wiki/Binary_prefix).
///
/// This is not the same as a [metric prefix](https://en.wikipedia.org/wiki/Metric_prefix).
enum BinaryPrefix implements Comparable<BinaryPrefix> {
  /// No prefix
  ///
  /// [factor] is 1
  none,

  /// Kibibytes (`KiB`)
  ///
  /// [factor] is 1 << 10
  kibi,

  /// Mebibytes (`MiB`)
  ///
  /// [factor] is 1 << 20
  mebi,

  /// Gibibytes (`GiB`)
  ///
  /// [factor] is  1 << 30
  gibi,

  /// Tebibytes (`TiB`)
  ///
  /// [factor] is 1 << 40
  tebi,

  /// Pebibytes (`PiB`)
  ///
  /// [factor] is 1 << 50
  pebi,

  /// Exbibytes (`EiB`)
  ///
  /// [factor] is 1 << 60
  exbi,

  /// Zebibytes (`ZiB`)
  ///
  /// [factor] is 1 << 70
  zebi,

  /// Yobibytes (`YiB`)
  ///
  /// [factor] is 1 << 80
  yobi,

  /// Robibytes (`RiB`)
  ///
  /// [factor] is 1 << 90
  robi,

  /// Quebibytes (`QiB`)
  ///
  /// [factor] is 1 << 100
  quebi;

  @override
  int compareTo(BinaryPrefix other) => other.index - index;

  BigInt get factor => switch (this) {
        BinaryPrefix.none => BigInt.one,
        BinaryPrefix.kibi => BigInt.parse("0x400"),
        BinaryPrefix.mebi => BigInt.parse("0x100000"),
        BinaryPrefix.gibi => BigInt.parse("0x40000000"),
        BinaryPrefix.tebi => BigInt.parse("0x10000000000"),
        BinaryPrefix.pebi => BigInt.parse("0x4000000000000"),
        BinaryPrefix.exbi => BigInt.parse("0x1000000000000000"),
        BinaryPrefix.zebi => BigInt.parse("0x400000000000000000"),
        BinaryPrefix.yobi => BigInt.parse("0x100000000000000000000"),
        BinaryPrefix.robi => BigInt.parse("0x40000000000000000000000"),
        BinaryPrefix.quebi => BigInt.parse("0x10000000000000000000000000"),
      };

  String get symbol => switch (this) {
        BinaryPrefix.none => "",
        BinaryPrefix.kibi => "Ki",
        BinaryPrefix.mebi => "Mi",
        BinaryPrefix.gibi => "Gi",
        BinaryPrefix.tebi => "Ti",
        BinaryPrefix.pebi => "Pi",
        BinaryPrefix.exbi => "Ei",
        BinaryPrefix.zebi => "Zi",
        BinaryPrefix.yobi => "Yi",
        BinaryPrefix.robi => "Ri",
        BinaryPrefix.quebi => "Qi"
      };
}
