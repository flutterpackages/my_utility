import "dart:async";

typedef VoidCallback = void Function();

typedef FutureVoidCallback = Future<void> Function();

typedef FutureOrVoidCallback = FutureOr<void> Function();
