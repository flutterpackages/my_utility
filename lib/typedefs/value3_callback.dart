import "dart:async";

typedef Value3Callback<A, B, C> = void Function(A value1, B value2);

typedef FutureValue3Callback<A, B, C> = Future<void> Function(
  A value1,
  B value2,
  C value3,
);

typedef FutureOrValue3Callback<A, B, C> = FutureOr<void> Function(
  A value1,
  B value2,
  C value3,
);
