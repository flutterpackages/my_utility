import "dart:async";

typedef ValueCallback<T> = void Function(T value);

typedef FutureValueCallback<T> = Future<void> Function(T value);

typedef FutureOrValueCallback<T> = FutureOr<void> Function(T value);
