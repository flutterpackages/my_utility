import "dart:async";

typedef ResultCallback<T> = T Function();

typedef FutureResultCallback<T> = Future<T> Function();

typedef FutureOrResultCallback<T> = FutureOr<T> Function();
