import "dart:async";

typedef Value2Callback<A, B> = void Function(A value1, B value2);

typedef FutureValue2Callback<A, B> = Future<void> Function(A value1, B value2);

typedef FutureOrValue2Callback<A, B> = FutureOr<void> Function(
  A value1,
  B value2,
);
