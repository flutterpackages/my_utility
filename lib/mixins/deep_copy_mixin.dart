mixin DeepCopyWithMixin<T extends DeepCopyWithMixin<T>> {
  T deepCopyWith();
}
