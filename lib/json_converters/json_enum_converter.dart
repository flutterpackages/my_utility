import "package:json_annotation/json_annotation.dart";
import "package:meta/meta.dart";

import "../dartx.dart";
import "../extensions.dart";

abstract class JsonEnumConverter<E extends Enum, S>
    extends JsonConverter<E, S> {
  const JsonEnumConverter() : _useCache = false;

  /// Creates a new [JsonEnumConverter], holds an internal cache that maps
  /// each enum value to a its converted json value, as per [toJson].
  ///
  /// Using a cached [JsonEnumConverter] may be more efficient, if the enum [E]
  /// has many values, or if there are **a lot** of conversions.
  ///
  /// The cache is shared by all [JsonEnumConverter] of type [E].
  const JsonEnumConverter.withCache() : _useCache = true;

  static final Map<Type, Map<Object?, Enum>> _fromJsonCache = {};

  final bool _useCache;

  List<E> get enumValues;

  @override
  @nonVirtual
  E fromJson(S json) {
    return _useCache ? _fromJsonWithCache(json) : _fromJsonWithoutCache(json);
  }

  E _fromJsonWithoutCache(S json) {
    for (final item in enumValues) {
      if (toJson(item) == json) return item;
    }

    _throwInvalidJson(json);
  }

  E _fromJsonWithCache(S json) {
    final cached = _fromJsonCache
        .putIfAbsent(
          E,
          () => enumValues.toMap(
            key: (_, e) => toJson(e),
            value: (_, e) => e,
          ),
        )
        .cast<S, E>();

    return cached.getOrElse(
      json,
      () => _throwInvalidJson(json),
    );
  }

  Never _throwInvalidJson(S json) {
    throw ArgumentError.value(
      json,
      "json",
      "No value from the enum '$E' maps to the provided json value",
    );
  }
}
