import "package:json_annotation/json_annotation.dart";
import "package:meta/meta.dart";

abstract class JsonListConverter<T, S>
    extends JsonConverter<List<T>, List<dynamic>> {
  const JsonListConverter();

  T singleFromJson(S json);

  S singleToJson(T object);

  @override
  @nonVirtual
  List<T> fromJson(List<dynamic> json) {
    final res = <T>[];

    var i = 0;
    for (final item in json) {
      if (item is! S)
        throw FormatException(
          "Each item in the json list must be of type '$S', but there is one "
          "of type '${item.runtimeType}'",
          json,
          i,
        );

      res.add(singleFromJson(item));

      i++;
    }

    return res;
  }

  @override
  @nonVirtual
  List<S> toJson(List<T> object) => object.map(singleToJson).toList();
}
