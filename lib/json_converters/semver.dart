import "package:json_annotation/json_annotation.dart";

import "../semver.dart";

final class SemVerJsonConverter extends JsonConverter<SemVer, String> {
  const SemVerJsonConverter();

  @override
  SemVer fromJson(String json) => SemVer.parse(json);

  @override
  String toJson(SemVer object) => object.toString();
}
