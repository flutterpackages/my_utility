import "package:dartx/dartx.dart";
import "package:json_annotation/json_annotation.dart";

final class DurationNumberJsonConverter extends JsonConverter<Duration, int> {
  const DurationNumberJsonConverter({
    this.unit = DurationUnit.milliseconds,
  });

  /// The unit of the json number.
  final DurationUnit unit;

  @override
  Duration fromJson(int json) {
    switch (unit) {
      case DurationUnit.days:
        return Duration(days: json);

      case DurationUnit.hours:
        return Duration(hours: json);

      case DurationUnit.minutes:
        return Duration(minutes: json);

      case DurationUnit.seconds:
        return Duration(seconds: json);

      case DurationUnit.milliseconds:
        return Duration(milliseconds: json);

      case DurationUnit.microseconds:
        return Duration(microseconds: json);
    }
  }

  @override
  int toJson(Duration object) {
    switch (unit) {
      case DurationUnit.days:
        return object.inDays;

      case DurationUnit.hours:
        return object.inHours;

      case DurationUnit.minutes:
        return object.inMinutes;

      case DurationUnit.seconds:
        return object.inSeconds;

      case DurationUnit.milliseconds:
        return object.inMilliseconds;

      case DurationUnit.microseconds:
        return object.inMicroseconds;
    }
  }
}

enum DurationUnit {
  days,
  hours,
  minutes,
  seconds,
  milliseconds,
  microseconds,
}

//

final class DurationStringJsonConverter
    extends JsonConverter<Duration, String> {
  const DurationStringJsonConverter();

  // Expected formats
  //
  // 0:00:00.000000
  // 0:03:30.337950
  // 0:00:00
  // 0:00:00.0
  // 0:00:00.00
  // 0:00:00.000
  // 0:00:00.0000
  // 0:00:00.00000
  // 0:00:00.000000
  // 12345:00:00.000000
  // -5:00:00.000000
  static final _regex = RegExp(
    "(?<minus>-)?"
    r"^(?<hours>\d+):"
    r"(?<minutes>\d{2}):"
    r"(?<seconds>\d{2})"
    r"(\.(?<milliseconds>\d{1,3})(?<microseconds>\d{1,3})?)?$",
  );

  @override
  Duration fromJson(String json) {
    final match = _regex.firstMatch(json);
    if (null == match)
      throw FormatException(
        "The provided json data for a duration must be "
        "in the format HH:MM:SS.mmmuuu (e.g.: 00:00:00.000000, "
        "-04:10:02.000103)",
        json,
      );

    final isNegative = null != match.namedGroup("minus");

    final hours = match.namedGroup("hours")?.toInt();
    if (null == hours)
      throw FormatException(
        "The provided data for a duration does not "
        "contain any data for the hour",
        json,
      );

    final minutes = match.namedGroup("minutes")?.toInt();
    if (null == minutes)
      throw FormatException(
        "The provided data for a duration does not "
        "contain any data for the minute",
        json,
      );

    final seconds = match.namedGroup("seconds")?.toInt();
    if (null == seconds)
      throw FormatException(
        "The provided data for a duration does not "
        "contain any data for the second",
        json,
      );

    final milliseconds = match.namedGroup("milliseconds")?.toInt();
    final microseconds = match.namedGroup("microseconds")?.toInt();

    final duration = Duration(
      hours: hours,
      minutes: minutes,
      seconds: seconds,
      milliseconds: milliseconds ?? 0,
      microseconds: microseconds ?? 0,
    );

    return isNegative ? -duration : duration;
  }

  @override
  String toJson(Duration object) => object.toString();
}
