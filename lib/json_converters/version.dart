import "package:json_annotation/json_annotation.dart";
import "package:version/version.dart";

final class VersionJsonConverter extends JsonConverter<Version, String> {
  const VersionJsonConverter();

  @override
  Version fromJson(String json) => Version.parse(json);

  @override
  String toJson(Version object) => object.toString();
}
