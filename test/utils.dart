import "package:my_utility/cli/cli_tool.dart";
import "package:my_utility/src/constants.dart";
import "package:my_utility/typedefs/result_callback.dart";
import "package:my_utility/typedefs/value_callback.dart";

String? initCliTool<T extends CliTool>({
  required ResultCallback<T> cliConstructor,
  ValueCallback<T>? onCliConstructed,
}) {
  if (kIsWeb) return "CLI tools are not supported on platform web.";
  final cli = cliConstructor();
  onCliConstructed?.call(cli);
  return cli.existsSync()
      ? null
      : "The ${cli.originalExecutable} CLI tool does not exist.";
}
