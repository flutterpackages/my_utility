import "package:my_utility/cli/mariadb_dump.dart";
import "package:my_utility/test.dart";

void main() {
  group("MariaDbDumpCliTool:", () {
    group("VM & JavaScript:", () {
      test("parseDumpResult", () {
        final res = MariaDbDumpCliTool.parseDumpResult(
          result: _sampleDumpData,
          database: "example_db",
        );

        expectTrue(res.containsKey("person"));
        expectTrue(res.containsKey("address"));

        final tblPerson = res["person"];
        final tblAddress = res["address"];

        expectTrue(tblPerson.hasColumnsWithRows);
        expectTrue(tblAddress.hasColumnsWithRows);

        // Amount of columns
        expect(tblPerson.length, 4);
        expect(tblAddress.length, 5);

        expectIterable(
          tblPerson.rows,
          [
            _getPersonRow(1, "John", "Doe", "1990-01-01"),
            _getPersonRow(2, "Jane", "Smith", "1985-05-12"),
            _getPersonRow(3, "Emily", "Johnson", "2000-09-23"),
          ],
          checkOrder: false,
        );
        expectIterable(
          tblAddress.rows,
          [
            _getAddressRow(1, 1, "123 Elm St", "Springfield", "IL"),
            _getAddressRow(2, 2, "456 Oak St", "Metropolis", "NY"),
            _getAddressRow(3, 1, "789 Maple Ave", "Springfield", "IL"),
            _getAddressRow(4, 3, "101 Pine St", "Gotham", "NJ"),
          ],
          checkOrder: false,
        );
      });
    });
  });
}

MariaDbTableRow _getAddressRow(
  int id,
  int personId,
  String address,
  String city,
  String state,
) {
  return {
    "id": id.toString(),
    "person_id": personId.toString(),
    "address": address,
    "city": city,
    "state": state,
  };
}

MariaDbTableRow _getPersonRow(
  int id,
  String firstName,
  String lastName,
  String birthday,
) {
  return {
    "id": id.toString(),
    "first_name": firstName,
    "last_name": lastName,
    "birthday": birthday,
  };
}

/// ## Example data
///
/// ```sql
/// -- Create the example database
/// CREATE DATABASE IF NOT EXISTS example_db;
///
/// -- Switch to the example database
/// USE example_db;
///
/// -- Create the 'person' table
/// CREATE TABLE IF NOT EXISTS person (
///     id INT AUTO_INCREMENT PRIMARY KEY,
///     first_name VARCHAR(50),
///     last_name VARCHAR(50),
///     birthday DATE
/// );
///
/// -- Insert sample data into the 'person' table
/// INSERT INTO person (first_name, last_name, birthday) VALUES
/// ('John', 'Doe', '1990-01-01'),
/// ('Jane', 'Smith', '1985-05-12'),
/// ('Emily', 'Johnson', '2000-09-23');
///
/// -- Create the 'address' table
/// CREATE TABLE IF NOT EXISTS address (
///     id INT AUTO_INCREMENT PRIMARY KEY,
///     person_id INT,
///     address VARCHAR(255),
///     city VARCHAR(100),
///     state VARCHAR(50),
///     FOREIGN KEY (person_id) REFERENCES person(id)
/// );
///
/// -- Insert sample data into the 'address' table
/// INSERT INTO address (person_id, address, city, state) VALUES
/// (1, '123 Elm St', 'Springfield', 'IL'),
/// (2, '456 Oak St', 'Metropolis', 'NY'),
/// (1, '789 Maple Ave', 'Springfield', 'IL'),
/// (3, '101 Pine St', 'Gotham', 'NJ');
/// ```
const _sampleDumpData = """
<?xml version="1.0"?>
<mysqldump xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<database name="example_db">
	<table_structure name="address">
		<field Field="id" Type="int(11)" Null="NO" Key="PRI" Extra="auto_increment" Comment="" />
		<field Field="person_id" Type="int(11)" Null="YES" Key="MUL" Default="NULL" Extra="" Comment="" />
		<field Field="address" Type="varchar(255)" Null="YES" Key="" Default="NULL" Extra="" Comment="" />
		<field Field="city" Type="varchar(100)" Null="YES" Key="" Default="NULL" Extra="" Comment="" />
		<field Field="state" Type="varchar(50)" Null="YES" Key="" Default="NULL" Extra="" Comment="" />
		<key Table="address" Non_unique="0" Key_name="PRIMARY" Seq_in_index="1" Column_name="id" Collation="A" Cardinality="4" Null="" Index_type="BTREE" Comment="" Index_comment="" Ignored="NO" />
		<key Table="address" Non_unique="1" Key_name="person_id" Seq_in_index="1" Column_name="person_id" Collation="A" Cardinality="4" Null="YES" Index_type="BTREE" Comment="" Index_comment="" Ignored="NO" />
		<options Name="address" Engine="InnoDB" Version="10" Row_format="Dynamic" Rows="4" Avg_row_length="4096" Data_length="16384" Max_data_length="0" Index_length="16384" Data_free="0" Auto_increment="5" Create_time="2024-06-10 13:18:13" Update_time="2024-06-10 13:18:13" Collation="utf8mb4_general_ci" Create_options="" Comment="" Max_index_length="0" Temporary="N" />
	</table_structure>
	<table_data name="address">
	<row>
		<field name="id">1</field>
		<field name="person_id">1</field>
		<field name="address">123 Elm St</field>
		<field name="city">Springfield</field>
		<field name="state">IL</field>
	</row>
	<row>
		<field name="id">2</field>
		<field name="person_id">2</field>
		<field name="address">456 Oak St</field>
		<field name="city">Metropolis</field>
		<field name="state">NY</field>
	</row>
	<row>
		<field name="id">3</field>
		<field name="person_id">1</field>
		<field name="address">789 Maple Ave</field>
		<field name="city">Springfield</field>
		<field name="state">IL</field>
	</row>
	<row>
		<field name="id">4</field>
		<field name="person_id">3</field>
		<field name="address">101 Pine St</field>
		<field name="city">Gotham</field>
		<field name="state">NJ</field>
	</row>
	</table_data>
	<table_structure name="person">
		<field Field="id" Type="int(11)" Null="NO" Key="PRI" Extra="auto_increment" Comment="" />
		<field Field="first_name" Type="varchar(50)" Null="YES" Key="" Default="NULL" Extra="" Comment="" />
		<field Field="last_name" Type="varchar(50)" Null="YES" Key="" Default="NULL" Extra="" Comment="" />
		<field Field="birthday" Type="date" Null="YES" Key="" Default="NULL" Extra="" Comment="" />
		<key Table="person" Non_unique="0" Key_name="PRIMARY" Seq_in_index="1" Column_name="id" Collation="A" Cardinality="3" Null="" Index_type="BTREE" Comment="" Index_comment="" Ignored="NO" />
		<options Name="person" Engine="InnoDB" Version="10" Row_format="Dynamic" Rows="3" Avg_row_length="5461" Data_length="16384" Max_data_length="0" Index_length="0" Data_free="0" Auto_increment="4" Create_time="2024-06-10 13:18:13" Update_time="2024-06-10 13:18:13" Collation="utf8mb4_general_ci" Create_options="" Comment="" Max_index_length="0" Temporary="N" />
	</table_structure>
	<table_data name="person">
	<row>
		<field name="id">1</field>
		<field name="first_name">John</field>
		<field name="last_name">Doe</field>
		<field name="birthday">1990-01-01</field>
	</row>
	<row>
		<field name="id">2</field>
		<field name="first_name">Jane</field>
		<field name="last_name">Smith</field>
		<field name="birthday">1985-05-12</field>
	</row>
	<row>
		<field name="id">3</field>
		<field name="first_name">Emily</field>
		<field name="last_name">Johnson</field>
		<field name="birthday">2000-09-23</field>
	</row>
	</table_data>
</database>
</mysqldump>
""";
