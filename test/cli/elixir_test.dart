@TestOn("dart-vm")
library;

import "package:my_utility/cli/elixir.dart";
import "package:my_utility/test.dart";
import "package:test/test.dart";

import "../utils.dart";

void main() {
  group(
    "ElixirCliTool:",
    () {
      late final ElixirCliTool cli;
      final skipReason = initCliTool(
        cliConstructor: ElixirCliTool.new,
        onCliConstructed: (e) => cli = e,
      );

      test(
        "getMixCliToolSync",
        () {
          final mix = cli.getMixCliToolSync();
          expect(mix.getVersionSync(), cli.getVersionSync());
        },
        skip: skipReason,
      );

      test(
        "getMixCliTool",
        () async {
          final mix = await cli.getMixCliTool();
          expect(mix.getVersionSync(), cli.getVersionSync());
        },
        skip: skipReason,
      );
    },
  );
}
