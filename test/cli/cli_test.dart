//ignore_for_file: avoid_dynamic_calls

@TestOn("dart-vm")
library;

import "package:my_utility/cli.dart";
import "package:my_utility/extensions/symbol.dart";
import "package:my_utility/src/mirror_utils.dart";
import "package:test/test.dart";

void main() {
  group(
    "CliTool:",
    () {
      if (kIsMirrorsSupported) {
        final /* Iterable<ClassMirror> */ classes = findClassesOfType(CliTool);

        for (final item in classes) {
          final instance = item.newInstance(Symbol.empty, []).reflectee;
          final simpleName = item.simpleName as Symbol;
          test("Exists '${simpleName.name}'", () {
            _testTool(instance);
          });
        }
      }
    },
  );
}

void _testTool(CliTool tool) {
  final name = tool.runtimeType;

  if (!tool.existsSync()) {
    print("$name does not exist");
    return;
  }

  final v = tool.getVersionSync();
  print("$name version $v");
}
