@TestOn("dart-vm")
library;

import "package:my_utility/cli/fvm.dart";
import "package:test/test.dart";

import "../utils.dart";

void main() {
  group(
    "FvmCliTool:",
    () {
      late final FvmCliTool cli;
      final skipReason = initCliTool(
        cliConstructor: FvmCliTool.new,
        onCliConstructed: (e) => cli = e,
      );

      test(
        "getFlutterCliToolSync",
        () {
          final flutter = cli.getFlutterCliToolSync();
          if (null == flutter) {
            print("Found no Flutter CLI tool");
          } else {
            final version = flutter.getVersionSync();
            print("Found Flutter CLI tool: ${flutter.executable} $version");
          }
        },
        skip: skipReason,
      );

      test(
        "getFlutterCliTool",
        () async {
          final flutter = await cli.getFlutterCliTool();
          if (null == flutter) {
            print("Found no Flutter CLI tool");
          } else {
            final version = flutter.getVersionSync();
            print("Found Flutter CLI tool: ${flutter.executable} $version");
          }
        },
        skip: skipReason,
      );
    },
  );
}
