@TestOn("dart-vm")
library;

import "package:my_utility/cli.dart";
import "package:test/test.dart";

import "../utils.dart";

void main() {
  group(
    "FlutterCliTool:",
    () {
      late final FlutterCliTool cli;
      final skipReason = initCliTool(
        cliConstructor: FlutterCliTool.new,
        onCliConstructed: (e) => cli = e,
      );

      test(
        "getDartCliTool",
        () async {
          final dart = await cli.getDartCliTool();
          expect(
            dart.getVersionSync(),
            cli.getVersionSync().dartSdkVersion,
          );
        },
        skip: skipReason,
      );

      test(
        "getDartCliToolSync",
        () async {
          final dart = cli.getDartCliToolSync();
          expect(
            dart.getVersionSync(),
            cli.getVersionSync().dartSdkVersion,
          );
        },
        skip: skipReason,
      );
    },
  );
}
