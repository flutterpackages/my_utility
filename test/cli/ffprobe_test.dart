import "dart:convert";

import "package:my_utility/src/models/ffprobe_models.dart";
import "package:my_utility/test/expect.dart";
import "package:test/test.dart";

void main() {
  group("FFProbeStreamModel:", () {
    test("fromJson", () {
      final json = jsonDecode(_data1);
      final model = FFProbeDataModel.fromJson(json);

      expect(model.streams.length, 1);

      var streamModel = model.streams.single;
      expectType<FFProbeAudioStreamModel>(streamModel);
      streamModel = streamModel as FFProbeAudioStreamModel;

      // stream

      expect(streamModel.index, 0);
      expect(streamModel.codecName, "opus");
      expect(streamModel.codecLongName, "Opus (Opus Interactive Audio Codec)");
      expect(streamModel.codecType, "audio");
      expect(streamModel.codecTagString, "[0][0][0][0]");
      expect(streamModel.codecTag, "0x0000");
      expect(streamModel.sampleFmt, "fltp");
      expect(streamModel.sampleRate, 48000);
      expect(streamModel.channels, 2);
      expect(streamModel.channelLayout, "stereo");
      expect(streamModel.bitsPerSample, 0);
      expect(streamModel.rFrameRate, "0/0");
      expect(streamModel.avgFrameRate, "0/0");
      expect(streamModel.timeBase, "1/1000");
      expect(streamModel.startPts, -7);
      expect(streamModel.startTime, -const Duration(milliseconds: 7));
      expectNull(streamModel.durationTs);
      expectNull(streamModel.duration);
      expectNull(streamModel.bitRate);

      // stream disposition

      expect(streamModel.disposition.defaultValue, 1);
      expect(streamModel.disposition.dub, 0);
      expect(streamModel.disposition.original, 0);
      expect(streamModel.disposition.comment, 0);
      expect(streamModel.disposition.lyrics, 0);
      expect(streamModel.disposition.karaoke, 0);
      expect(streamModel.disposition.forced, 0);
      expect(streamModel.disposition.hearingImpaired, 0);
      expect(streamModel.disposition.visualImpaired, 0);
      expect(streamModel.disposition.cleanEffects, 0);
      expect(streamModel.disposition.attachedPic, 0);
      expect(streamModel.disposition.timedThumbnails, 0);
      expect(streamModel.disposition.captions, 0);
      expect(streamModel.disposition.descriptions, 0);
      expect(streamModel.disposition.metadata, 0);
      expect(streamModel.disposition.dependent, 0);
      expect(streamModel.disposition.stillImage, 0);

      // stream tags

      expect(streamModel.tags?.language, "eng");

      // format

      expect(model.format.filename, "test file.webm");
      expect(model.format.nbStreams, 1);
      expect(model.format.nbPrograms, 0);
      expect(model.format.formatName, "matroska,webm");
      expect(model.format.formatLongName, "Matroska / WebM");
      expect(model.format.startTime, -const Duration(milliseconds: 7));
      expect(
        model.format.duration,
        const Duration(seconds: 44, milliseconds: 521),
      );
      expect(model.format.size, 759624);
      expect(model.format.bitRate, 136497);
      expect(model.format.probeScore, 100);
      expect(model.format.tags.encoder, "google/video-file");
    });
  });
}

const _data1 = """
{
    "streams": [
        {
            "index": 0,
            "codec_name": "opus",
            "codec_long_name": "Opus (Opus Interactive Audio Codec)",
            "codec_type": "audio",
            "codec_tag_string": "[0][0][0][0]",
            "codec_tag": "0x0000",
            "sample_fmt": "fltp",
            "sample_rate": "48000",
            "channels": 2,
            "channel_layout": "stereo",
            "bits_per_sample": 0,
            "r_frame_rate": "0/0",
            "avg_frame_rate": "0/0",
            "time_base": "1/1000",
            "start_pts": -7,
            "start_time": "-0.007000",
            "extradata_size": 19,
            "disposition": {
                "default": 1,
                "dub": 0,
                "original": 0,
                "comment": 0,
                "lyrics": 0,
                "karaoke": 0,
                "forced": 0,
                "hearing_impaired": 0,
                "visual_impaired": 0,
                "clean_effects": 0,
                "attached_pic": 0,
                "timed_thumbnails": 0,
                "captions": 0,
                "descriptions": 0,
                "metadata": 0,
                "dependent": 0,
                "still_image": 0
            },
            "tags": {
                "language": "eng"
            }
        }
    ],
    "format": {
        "filename": "test file.webm",
        "nb_streams": 1,
        "nb_programs": 0,
        "format_name": "matroska,webm",
        "format_long_name": "Matroska / WebM",
        "start_time": "-0.007000",
        "duration": "44.521000",
        "size": "759624",
        "bit_rate": "136497",
        "probe_score": 100,
        "tags": {
            "encoder": "google/video-file"
        }
    }
}
""";
