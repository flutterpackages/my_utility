import "package:my_utility/json_converters.dart";
import "package:my_utility/test/test.dart";

void main() {
  group("JsonEnumConverter", () {
    test("without cache", () {
      const conv = MyEnumJsonConverter();
      for (final item in MyEnum.values) {
        expect(conv.toJson(item), item.name);
      }

      for (final item in MyEnum.values) {
        expect(item, conv.fromJson(item.name));
      }
    });

    test("with cache", () {
      const conv = MyEnumJsonConverter.withCache();
      for (final item in MyEnum.values) {
        expect(conv.toJson(item), item.name);
      }

      for (final item in MyEnum.values) {
        expect(item, conv.fromJson(item.name));
      }
    });
  });
}

enum MyEnum {
  foo,
  bar,
  barfoo,
}

final class MyEnumJsonConverter extends JsonEnumConverter<MyEnum, String> {
  const MyEnumJsonConverter();

  const MyEnumJsonConverter.withCache() : super.withCache();

  @override
  List<MyEnum> get enumValues => MyEnum.values;

  @override
  String toJson(MyEnum object) {
    return object.name;
  }
}
