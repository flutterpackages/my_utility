// ignore_for_file: unrelated_type_equality_checks

import "package:my_utility/extensions.dart";
import "package:test/test.dart";

void main() {
  group("DoubleWithTolerance", () {
    group("operator==", () {
      test("0.75 \u{00B1}1e-10 == 0.75 \u{00B1}1e-10", () {
        final a = 0.75.withTolerance();
        final b = 0.75.withTolerance();
        expect(a == b, true);
      });

      test("0.75 \u{00B1}1e-10 == 0.75", () {
        final a = 0.75.withTolerance();
        const b = 0.75;
        expect(a == b, true);
      });

      test("0.751 \u{00B1}1e-2 == 0.750", () {
        final a = 0.751.withTolerance(epsilon: 1e-2);
        const b = 0.750;
        expect(a == b, true);
      });

      test("0.751 \u{00B1}1e-3 == 0.750", () {
        final a = 0.751.withTolerance(epsilon: 1e-3);
        const b = 0.750;
        expect(a == b, false);
      });
    });

    group("isEqual", () {
      test("0.75 \u{00B1}1e-10.isEqual(0.75 \u{00B1}1e-10)", () {
        final a = 0.75.withTolerance();
        final b = 0.75.withTolerance();
        expect(a.isEqual(b), true);
      });

      test("0.75 \u{00B1}1e-10.isEqual(0.75)", () {
        final a = 0.75.withTolerance();
        const b = 0.75;
        expect(a.isEqual(b), true);
      });

      test("0.751 \u{00B1}1e-2.isEqual(0.750)", () {
        final a = 0.751.withTolerance(epsilon: 1e-2);
        const b = 0.750;
        expect(a.isEqual(b), true);
      });

      test("0.751 \u{00B1}1e-3.isEqual(0.750)", () {
        final a = 0.751.withTolerance(epsilon: 1e-3);
        const b = 0.750;
        expect(a.isEqual(b), false);
      });
    });

    group("operator<", () {
      test("0.74 \u{00B1}1e-10 < 0.75", () {
        final a = 0.74.withTolerance();
        const b = 0.75;
        expect(a < b, true);
      });

      test("0.74 \u{00B1}1e-10 < 0.75 \u{00B1}1e-10", () {
        final a = 0.74.withTolerance();
        final b = 0.75.withTolerance();
        expect(a < b, true);
      });
    });

    group("isLessThan", () {
      test("0.74 \u{00B1}1e-10.isLessThan(0.75)", () {
        final a = 0.74.withTolerance();
        const b = 0.75;
        expect(a.isLessThan(b), true);
      });

      test("0.74 \u{00B1}1e-10.isLessThan(0.75 \u{00B1}1e-10)", () {
        final a = 0.74.withTolerance();
        final b = 0.75.withTolerance();
        expect(a.isLessThan(b), true);
      });
    });

    group("operator<=", () {
      test("0.74 \u{00B1}1e-10 <= 0.75", () {
        final a = 0.74.withTolerance();
        const b = 0.75;
        expect(a <= b, true);
      });

      test("0.74 \u{00B1}1e-10 <= 0.75 \u{00B1}1e-10", () {
        final a = 0.74.withTolerance();
        final b = 0.75.withTolerance();
        expect(a <= b, true);
      });

      test("0.75 \u{00B1}1e-10 <= 0.75 \u{00B1}1e-10", () {
        final a = 0.75.withTolerance();
        final b = 0.75.withTolerance();
        expect(a <= b, true);
      });

      test("0.76 \u{00B1}1e-10 <= 0.75 \u{00B1}1e-10", () {
        final a = 0.76.withTolerance();
        final b = 0.75.withTolerance();
        expect(a <= b, false);
      });
    });

    group("isLessThanOrEqual", () {
      test("0.74 \u{00B1}1e-10.isLessThanOrEqual(0.75)", () {
        final a = 0.74.withTolerance();
        const b = 0.75;
        expect(a.isLessThanOrEqual(b), true);
      });

      test("0.74 \u{00B1}1e-10.isLessThanOrEqual(0.75 \u{00B1}1e-10)", () {
        final a = 0.74.withTolerance();
        final b = 0.75.withTolerance();
        expect(a.isLessThanOrEqual(b), true);
      });

      test("0.75 \u{00B1}1e-10.isLessThanOrEqual(0.75 \u{00B1}1e-10)", () {
        final a = 0.75.withTolerance();
        final b = 0.75.withTolerance();
        expect(a.isLessThanOrEqual(b), true);
      });

      test("0.76 \u{00B1}1e-10.isLessThanOrEqual(0.75 \u{00B1}1e-10)", () {
        final a = 0.76.withTolerance();
        final b = 0.75.withTolerance();
        expect(a.isLessThanOrEqual(b), false);
      });
    });

    group("operator>", () {
      test("0.75 \u{00B1}1e-10 > 0.74", () {
        final a = 0.75.withTolerance();
        const b = 0.74;
        expect(a > b, true);
      });

      test("0.75 \u{00B1}1e-10 > 0.74 \u{00B1}1e-10", () {
        final a = 0.75.withTolerance();
        final b = 0.74.withTolerance();
        expect(a > b, true);
      });
    });

    group("isGreaterThan", () {
      test("0.75 \u{00B1}1e-10.isGreaterThan(0.74)", () {
        final a = 0.75.withTolerance();
        const b = 0.74;
        expect(a.isGreaterThan(b), true);
      });

      test("0.75 \u{00B1}1e-10.isGreaterThan(0.74 \u{00B1}1e-10)", () {
        final a = 0.75.withTolerance();
        final b = 0.74.withTolerance();
        expect(a.isGreaterThan(b), true);
      });
    });

    group("operator>=", () {
      test("0.75 \u{00B1}1e-10 >= 0.74", () {
        final a = 0.75.withTolerance();
        const b = 0.74;
        expect(a >= b, true);
      });

      test("0.75 \u{00B1}1e-10 >= 0.74 \u{00B1}1e-10", () {
        final a = 0.75.withTolerance();
        final b = 0.74.withTolerance();
        expect(a >= b, true);
      });

      test("0.75 \u{00B1}1e-10 >= 0.75 \u{00B1}1e-10", () {
        final a = 0.75.withTolerance();
        final b = 0.75.withTolerance();
        expect(a >= b, true);
      });

      test("0.75 \u{00B1}1e-10 >= 0.76 \u{00B1}1e-10", () {
        final a = 0.75.withTolerance();
        final b = 0.76.withTolerance();
        expect(a >= b, false);
      });
    });

    group("isGreaterThanOrEqual", () {
      test("0.75 \u{00B1}1e-10.isGreaterThanOrEqual(0.74)", () {
        final a = 0.75.withTolerance();
        const b = 0.74;
        expect(a.isGreaterThanOrEqual(b), true);
      });

      test("0.75 \u{00B1}1e-10.isGreaterThanOrEqual(0.74 \u{00B1}1e-10)", () {
        final a = 0.75.withTolerance();
        final b = 0.74.withTolerance();
        expect(a.isGreaterThanOrEqual(b), true);
      });

      test("0.75 \u{00B1}1e-10 .isGreaterThanOrEqual(0.75 \u{00B1}1e-10)", () {
        final a = 0.75.withTolerance();
        final b = 0.75.withTolerance();
        expect(a.isGreaterThanOrEqual(b), true);
      });

      test("0.75 \u{00B1}1e-10.isGreaterThanOrEqual(0.76 \u{00B1}1e-10)", () {
        final a = 0.75.withTolerance();
        final b = 0.76.withTolerance();
        expect(a.isGreaterThanOrEqual(b), false);
      });
    });

    group("operator+", () {
      test("0.5 \u{00B1}1e-10 + 0.5 == 1", () {
        final a = 0.5.withTolerance();
        const b = 0.5;
        expect(a + b, 1);
      });
    });

    group("operator-", () {
      test("0.5 \u{00B1}1e-10 - 0.5 == 0", () {
        final a = 0.5.withTolerance();
        const b = 0.5;
        expect(a - b, 0);
      });
    });

    group("operator*", () {
      test("1.0 \u{00B1}1e-10 * 0.5 == 0.5", () {
        final a = 1.0.withTolerance();
        const b = 0.5;
        expect(a * b, 0.5);
      });
    });

    group("operator%", () {
      test("10.1 \u{00B1}1e-10 % 3.0 == 1", () {
        final a = 10.1.withTolerance();
        const b = 3.0;
        final res = a % b;
        expect(1.1.withTolerance() == res, true);
      });
    });

    group("operator/", () {
      test("9.0 \u{00B1}1e-10 / 2.0 == 4.5", () {
        final a = 9.0.withTolerance();
        const b = 2.0;
        expect(a / b, 4.5);
      });
    });

    group("operator~/", () {
      test("9.0 \u{00B1}1e-10 ~/ 2.0 == 4", () {
        final a = 9.0.withTolerance();
        const b = 2.0;
        expect(a ~/ b, 4);
      });
    });
  });
}
