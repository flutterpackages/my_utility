import "package:my_utility/io/interactive.dart";

void main() {
  final x = promptUntilValidPick<String>(
    prompt: "Pick an option",
    options: {
      "a",
      "b",
      "c",
    },
    stringify: (index, option) => (selector: index.toString(), option: option),
    // defaultValue: "a",
  );
  print("Result: $x");
}
