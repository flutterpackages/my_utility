import "dart:math";

import "package:dartx/dartx.dart";
import "package:my_utility/extensions.dart";
import "package:my_utility/test.dart";

void main() {
  test("MyUtilityExtensionRandomNextAlphaNumericString", () {
    final rnd = Random();
    final regex = RegExp(r"^[a-zA-Z0-9]+$", multiLine: true);
    for (var i = 0; i < 100000; i++) {
      final len = rnd.nextIntBetween(10, 1000);
      final str = rnd.nextAlphaNumericString(len);
      expect(str.length, len);
      expectRegexMatch(str, regex);
    }
  });

  group("MyUtilityExtensionRandomNextDateTime:", () {
    test("nextDate - yearRange", () {
      final date = Random().nextDateTime(yearRange: 2023.rangeTo(2024));
      expect(2023 == date.year || 2024 == date.year, true);
      expect(date.month, 1);
      expect(date.day, 1);
      expect(date.hour, 0);
      expect(date.minute, 0);
      expect(date.second, 0);
      expect(date.millisecond, 0);
      expect(date.microsecond, 0);
    });

    test("nextDate - yearRange & monthRange", () {
      final date = Random().nextDateTime(
        yearRange: 2023.rangeTo(2024),
        monthRange: 3.rangeTo(8),
      );
      expect(2023 == date.year || 2024 == date.year, true);
      expect(date.month >= 3 && date.month <= 8, true);
      expect(date.day, 1);
      expect(date.hour, 0);
      expect(date.minute, 0);
      expect(date.second, 0);
      expect(date.millisecond, 0);
      expect(date.microsecond, 0);
    });

    test("nextDate - yearRange & monthRange & dayRange", () {
      final date = Random().nextDateTime(
        yearRange: 2023.rangeTo(2024),
        monthRange: 10.rangeTo(12),
      );
      expect(2023 == date.year || 2024 == date.year, true);
      expect(date.month >= 10 && date.month <= 12, true);
      expect(date.day >= 1, true);
      switch (date.month) {
        case 10:
        case 12:
          expect(date.day <= 31, true);
          break;

        case 11:
          expect(date.day <= 30, true);
          break;

        default:
          throw UnimplementedError();
      }

      expect(date.hour, 0);
      expect(date.minute, 0);
      expect(date.second, 0);
      expect(date.millisecond, 0);
      expect(date.microsecond, 0);
    });
  });
}
