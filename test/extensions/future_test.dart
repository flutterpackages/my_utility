import "package:my_utility/extensions/future.dart";
import "package:my_utility/test.dart";

void main() {
  group("MyUtilityExtensionFutureWithDelay:", () {
    const secs = Duration(seconds: 3);
    final myErr = Exception("My Error Message");

    group("Check timing:", () {
      group("without error:", () {
        test("without delay", () async {
          final future = Future.value(10);
          final watch = Stopwatch();
          watch.start();
          await future;
          watch.stop();
          expect(watch.elapsed, lessThan(secs));
        });

        test(".withDelay(${secs.inSeconds}s)", () async {
          final future = Future.value(10);
          final watch = Stopwatch();
          watch.start();
          await future.withDelay(secs, threshold: Duration.zero);
          watch.stop();
          expect(watch.elapsed, greaterThanOrEqualTo(secs));
        });
      });

      group("throwing ${myErr.runtimeType}:", () {
        test("without delay", () async {
          final future = Future.error(myErr);
          final watch = Stopwatch();
          watch.start();
          expectError<Exception>(() => future);
          watch.stop();
          expect(watch.elapsed, lessThan(secs));
        });

        test(".withDelay(${secs.inSeconds}s)", () async {
          final future = Future.error(myErr);
          final watch = Stopwatch();
          watch.start();
          expectError<Exception>(
            () => future.withDelay(secs, threshold: Duration.zero),
          );
          watch.stop();
          expect(watch.elapsed, lessThan(secs));
        });
      });
    });
  });
}
