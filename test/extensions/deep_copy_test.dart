import "dart:math";

import "package:my_utility/extensions/deep_copy.dart";
import "package:my_utility/extensions/iterable.dart";
import "package:my_utility/extensions/random.dart";
import "package:my_utility/test.dart";
import "package:protobuf/protobuf.dart";

import "../protos/bigint.pb.dart";

void main() {
  final rand = Random();

  MyUtilityExtensionDeepCopyIterable.addCopyFactory<BigInt>(
    (e) => e.deepCopy(),
  );

  MyUtilityExtensionDeepCopyIterable.addCopyFactory<BigIntList>(
    (e) => e.deepCopy(),
  );

  //

  group("MyUtilityExtensionDeepCopyIterable:", () {
    test("List<int>", () {
      final list = List.generate(5, (index) => rand.nextInt(500000));
      final copy = list.deepCopy();
      expectIterable(copy, list);
      expectType<List<int>>(copy);
    });

    test("List<BigInt>", () {
      final list = List.generate(
        5,
        (index) => BigInt(value: rand.nextAsciiNumberString(20)),
      );
      final copy = list.deepCopy();
      expectIterable(copy, list);
      expectType<List<BigInt>>(copy);
      for (var i = 0; i < list.length; i++) {
        expectFalse(
          identical(list[i], copy[i]),
          reason: "Must not be identical",
        );
      }
    });

    test("List<List<int>>", () {
      final list = List.generate(
        5,
        (index) => List.generate(20, (index) => rand.nextInt(500000)),
      );
      final copy = list.deepCopy();
      expectIterable(copy, list);
      expectType<List<List<int>>>(copy);
      for (var i = 0; i < list.length; i++) {
        expectFalse(identical(list[i], copy[i]));
        for (var j = 0; j < list[i].length; j++) {
          expect(copy[i][j], list[i][j]);
        }
      }
    });

    test("List<List<BigInt>>", () {
      final list = List.generate(
        5,
        (index) => List.generate(
          20,
          (index) => BigInt(value: rand.nextInt(500000).toString()),
        ),
      );
      final copy = list.deepCopy();
      expectIterable(copy, list);
      expectType<List<List<BigInt>>>(copy);
      for (var i = 0; i < list.length; i++) {
        expectFalse(identical(list[i], copy[i]));
        for (var j = 0; j < list[i].length; j++) {
          // Expect different BigInt instances.
          expectFalse(identical(list[i][j], copy[i][j]));

          // Expect equal values.
          expect(copy[i][j], list[i][j]);
        }
      }
    });

    test("List<List<BigIntList>>", () {
      final list = List.generate(
        5,
        (index) => List.generate(
          6,
          (index) => BigIntList(
            values: List.generate(
              20,
              (index) => BigInt(value: rand.nextInt(500000).toString()),
            ),
          ),
        ),
      );
      final copy = list.deepCopy();
      expectIterable(copy, list);
      expectType<List<List<BigIntList>>>(copy);
      for (var i = 0; i < list.length; i++) {
        // Different List<BigIntList> instances.
        expectFalse(identical(list[i], copy[i]));

        for (var j = 0; j < list[i].length; j++) {
          // Different BigIntList instances.
          expectFalse(identical(list[i][j], copy[i][j]));

          for (var k = 0; k < list[i][j].values.length; k++) {
            // Expect different BigInt instances.
            expectFalse(identical(list[i][j].values[k], copy[i][j].values[k]));

            // Expect equal values.
            expect(copy[i][j].values[k], list[i][j].values[k]);
          }
        }
      }
    });
  });

  group("MyUtilityExtensionDeepCopyIterableOfMap:", () {
    test("List<Map<int, String>>", () {
      final list = List.generate(5, (_) => _genMap());
      final copy = list.deepCopy();

      expectIterable(copy, list);
      expectType<List<Map<int, String>>>(copy);
      for (var i = 0; i < list.length; i++) {
        expectFalse(
          identical(list[i], copy[i]),
          reason: "Map elements must not be identical",
        );
      }
    });
  });

  group("MyUtilityExtensionDeepCopyIterableOfMapWithKeyMap:", () {
    test("List<Map<Map<int, String>, String>>", () {
      final list = List.generate(
        5,
        (_) => Map.fromEntries(
          List.generate(
            4,
            (__) => MapEntry(_genMap(), rand.nextAsciiNumberString(10)),
          ),
        ),
      );
      final copy = list.deepCopy();

      expectIterable(copy, list);
      expectType<List<Map<Map<int, String>, String>>>(copy);
      for (var i = 0; i < list.length; i++) {
        expectFalse(
          identical(list[i], copy[i]),
          reason: "Map elements must not be identical",
        );

        for (var j = 0; j < list[i].length; j++) {
          expectFalse(
            identical(
              list[i].entries[j].key,
              copy[i].entries[j].key,
            ),
            reason: "Keys from map at $i must not be identical",
          );

          expect(
            copy[i].entries[j].value,
            list[i].entries[j].value,
          );
        }
      }
    });
  });

  group("MyUtilityExtensionDeepCopyIterableOfMapWithValueMap:", () {
    test("List<Map<int, Map<int, String>>>", () {
      final list = List.generate(
        5,
        (_) => Map.fromEntries(
          List.generate(4, (i) => MapEntry(i, _genMap())),
        ),
      );
      final copy = list.deepCopy();

      expectIterable(copy, list);
      expectType<List<Map<int, Map<int, String>>>>(copy);
      for (var i = 0; i < list.length; i++) {
        expectFalse(
          identical(list[i], copy[i]),
          reason: "Map elements must not be identical",
        );

        for (var j = 0; j < list[i].length; j++) {
          expect(
            copy[i].entries[j].key,
            list[i].entries[j].key,
          );

          expectFalse(
            identical(
              list[i].entries[j].value,
              copy[i].entries[j].value,
            ),
            reason: "Values from map at $i must not be identical",
          );
        }
      }
    });
  });

  group("MyUtilityExtensionDeepCopyIterableOfMapWithKeyAndValueMap:", () {
    test("List<Map<Map<int, String>, Map<int, String>>>", () {
      final list = List.generate(
        5,
        (_) => Map.fromEntries(
          List.generate(4, (_) => MapEntry(_genMap(), _genMap())),
        ),
      );
      final copy = list.deepCopy();

      expectIterable(copy, list);
      expectType<List<Map<Map<int, String>, Map<int, String>>>>(copy);
      for (var i = 0; i < list.length; i++) {
        expectFalse(
          identical(list[i], copy[i]),
          reason: "Map elements must not be identical",
        );

        for (var j = 0; j < list[i].length; j++) {
          expectFalse(
            identical(
              list[i].entries[j].key,
              copy[i].entries[j].key,
            ),
            reason: "Keys from map at $i must not be identical",
          );

          expectFalse(
            identical(
              list[i].entries[j].value,
              copy[i].entries[j].value,
            ),
            reason: "Values from map at $i must not be identical",
          );
        }
      }
    });
  });

  //

  group("MyUtilityExtensionDeepCopyMap:", () {
    test("Map<int, String>", () {
      final map = _genMap();
      final copy = map.deepCopy();

      expectType<Map<int, String>>(copy);
      for (var i = 0; i < map.length; i++) {
        expect(
          copy.entries[i].key,
          map.entries[i].key,
        );

        expect(
          copy.entries[i].value,
          map.entries[i].value,
        );
      }
    });
  });

  group("MyUtilityExtensionDeepCopyMapWithKeyMap:", () {
    test("Map<Map<int, String>, String>", () {
      final map = Map.fromEntries(
        List.generate(
          4,
          (i) => MapEntry(
            _genMap(),
            rand.nextAsciiNumberString(10),
          ),
        ),
      );
      final copy = map.deepCopy();

      expectType<Map<Map<int, String>, String>>(copy);
      for (var i = 0; i < map.length; i++) {
        expectFalse(
          identical(
            map.entries[i].key,
            copy.entries[i].key,
          ),
          reason: "Keys must not be identical",
        );

        expect(
          copy.entries[i].value,
          map.entries[i].value,
        );
      }
    });
  });

  group("MyUtilityExtensionDeepCopyMapWithValueMap:", () {
    test("Map<int, Map<int, String>>", () {
      final map = Map.fromEntries(
        List.generate(
          4,
          (k) => MapEntry(k, _genMap()),
        ),
      );
      final copy = map.deepCopy();

      expectType<Map<int, Map<int, String>>>(copy);
      for (var i = 0; i < map.length; i++) {
        expect(
          copy.entries[i].key,
          map.entries[i].key,
        );

        expectFalse(
          identical(
            map.entries[i].value,
            copy.entries[i].value,
          ),
          reason: "Values must not be identical",
        );
      }
    });
  });

  group("MyUtilityExtensionDeepCopyMapWithKeyAndValueMap:", () {
    test("Map<Map<int, String>, Map<int, String>>", () {
      final map = Map.fromEntries(
        List.generate(
          4,
          (i) => MapEntry(_genMap(), _genMap()),
        ),
      );
      final copy = map.deepCopy();

      expectType<Map<Map<int, String>, Map<int, String>>>(copy);
      for (var i = 0; i < map.length; i++) {
        expectFalse(
          identical(
            map.entries[i].key,
            copy.entries[i].key,
          ),
          reason: "Keys must not be identical",
        );

        expectFalse(
          identical(
            map.entries[i].value,
            copy.entries[i].value,
          ),
          reason: "Values must not be identical",
        );
      }
    });
  });
}

Map<int, String> _genMap([int length = 4]) {
  final rand = Random();
  return Map.fromEntries(
    List.generate(
      length,
      (k) => MapEntry(k, rand.nextAsciiNumberString(10)),
    ),
  );
}
