import "dart:async";
import "dart:math";

import "package:my_utility/extensions.dart";
import "package:my_utility/test/expect.dart";
import "package:test/test.dart";

void main() {
  // Uncomment for debugging purposes.
  //InitialDelayStreamTransformer.debugPrint = _myPrint;

  group("MyUtilityExtensionStreamListenAsFuture:", () {
    group("Check error behavior:", () {
      group(".listen:", () {
        test("cancelOnError=false", () async {
          final errComp = Completer();
          final doneComp = Completer();
          var eventIdx = 0;

          // 0 1 2 ERR DONE
          _gen(errIdx: 3).listen(
            (e) {
              expect(eventIdx, lessThanOrEqualTo(2));
              eventIdx++;
            },
            onError: (_) {
              expect(eventIdx, 3);
              eventIdx++;
              errComp.complete();
            },
            onDone: () {
              expect(eventIdx, 4);
              eventIdx++;
              doneComp.complete();
            },
            cancelOnError: false,
          );

          await errComp.future;
          await doneComp.future;

          expect(eventIdx, 5);
        });

        test("cancelOnError=true", () async {
          final errComp = Completer();
          final doneComp = Completer();
          var eventIdx = 0;

          // 0 1 2 ERR
          _gen(errIdx: 3).listen(
            (e) {
              expect(eventIdx, lessThanOrEqualTo(2));
              eventIdx++;
            },
            onError: (_) {
              expect(eventIdx, 3);
              eventIdx++;
              errComp.complete();
            },
            onDone: () {
              expect(eventIdx, 4);
              eventIdx++;
              doneComp.complete();
            },
            cancelOnError: true,
          );

          await errComp.future;
          expectTrue(doneComp.isNotCompleted);

          expect(eventIdx, 4);
        });
      });

      group(".listenAsFuture", () {
        test("cancelOnError=false", () async {
          final errComp = Completer();
          final doneComp = Completer();
          var eventIdx = 0;

          // 0 1 2 ERR DONE
          _gen(errIdx: 3).listenAsFuture(
            (e) {
              expect(eventIdx, lessThanOrEqualTo(2));
              eventIdx++;
            },
            onError: (_) {
              expect(eventIdx, 3);
              eventIdx++;
              errComp.complete();
            },
            onDone: () {
              expect(eventIdx, 4);
              eventIdx++;
              doneComp.complete();
            },
            cancelOnError: false,
          );

          await errComp.future;
          await doneComp.future;

          expect(eventIdx, 5);
        });

        test("cancelOnError=true", () async {
          final errComp = Completer();
          final doneComp = Completer();
          var eventIdx = 0;

          // 0 1 2 ERR
          try {
            await _gen(errIdx: 3).listenAsFuture(
              (e) {
                expect(eventIdx, lessThanOrEqualTo(2));
                eventIdx++;
              },
              onError: (_) {
                expect(eventIdx, 3);
                eventIdx++;
                errComp.complete();
              },
              onDone: () {
                expect(eventIdx, 4);
                eventIdx++;
                doneComp.complete();
              },
              cancelOnError: true,
            ).value;
          } on Exception catch (_) {}

          await errComp.future;
          expectTrue(doneComp.isNotCompleted);

          expect(eventIdx, 4);
        });
      });
    });

    test("No error", () async {
      final data = List.generate(20, (i) => i);
      var idx = 0;

      await Stream.fromIterable(data).listenAsFuture((e) {
        expect(e, data[idx++]);
      }).value;

      expect(idx, data.length);
    });

    test("Expect error", () async {
      // Should not throw, because cancelOnError is false.
      await _gen().listenAsFuture((_) {}).value;

      expectError<Exception>(
        () async {
          final res = await _gen()
              .listenAsFuture(
                (_) {},
                cancelOnError: true,
              )
              .value;
          return res;
        },
      );
    });

    test("Manual cancellation", () async {
      var isDone = false;
      var hadError = false;
      final token = _infiniteGen().listenAsFuture(
        (_) {},
        onDone: () => isDone = true,
        onError: (_) => hadError = true,
      );

      await Future.delayed(const Duration(seconds: 3));
      await token.cancel();

      expectFalse(hadError);
      expectFalse(isDone);
      expectTrue(token.isCanceled);
    });
  });

  group("MyUtilityExtensionStreamWithInitialDelay:", () {
    const secs = Duration(seconds: 2);
    const microsecs = Duration(microseconds: 1);
    final rnd = Random();

    test("Check timing", () async {
      final stream = _gen(shouldThrow: false).withInitialDelay(
        secs,
        threshold: Duration.zero,
      );

      final watch = Stopwatch();
      var isFirst = true;

      watch.start();
      await for (final _ in stream) {
        if (isFirst) {
          isFirst = false;
          watch.stop();
          expect(watch.elapsed, greaterThanOrEqualTo(secs));
          watch.reset();
        }

        watch.stop();
        expect(watch.elapsed, lessThan(secs));
        watch.reset();
        watch.start();
      }
    });

    group("Propagate error:", () {
      const timeout5 = Timeout(Duration(seconds: 5));
      final myErr = Exception("My event error");
      const intsBeforeErr = [1, 10, -5];
      const intsAfterErr = [26, 55];

      group("WITHOUT .withInitialDelay", () {
        test("cancelOnError=false", () async {
          final ctrl = StreamController<int>();

          intsBeforeErr.forEach(ctrl.add);
          ctrl.addError(myErr);
          intsAfterErr.forEach(ctrl.add);

          expect(
            ctrl.stream,
            emitsInOrder([
              ...intsBeforeErr,
              emitsError(myErr),
              ...intsAfterErr,
              emitsDone,
            ]),
          );

          await ctrl.close();
        });

        test(
          "cancelOnError=true",
          () async {
            final ctrl = StreamController<int>();

            intsBeforeErr.forEach(ctrl.add);
            ctrl.addError(myErr);
            intsAfterErr.forEach(ctrl.add);

            var eventIdx = 0;
            ctrl.stream.listen(
              (e) {
                expect(e, intsBeforeErr[eventIdx]);
                eventIdx++;
              },
              onError: (err) => expect(err, myErr),
              cancelOnError: true,
            );

            await ctrl.close();
          },
          timeout: timeout5,
        );
      });

      group(".withInitialDelay(${microsecs.inMicroseconds}us)", () {
        test("cancelOnError=false", () async {
          final ctrl = StreamController<int>();

          intsBeforeErr.forEach(ctrl.add);
          ctrl.addError(myErr);
          intsAfterErr.forEach(ctrl.add);

          expect(
            ctrl.stream.withInitialDelay(microsecs),
            emitsInOrder([
              ...intsBeforeErr,
              emitsError(myErr),
              ...intsAfterErr,
              emitsDone,
            ]),
          );

          await ctrl.close();
        });

        test(
          "cancelOnError=true",
          () async {
            final ctrl = StreamController<int>();

            intsBeforeErr.forEach(ctrl.add);
            ctrl.addError(myErr);
            intsAfterErr.forEach(ctrl.add);

            var eventIdx = 0;
            ctrl.stream.withInitialDelay(secs).listen(
              (e) {
                expect(e, intsBeforeErr[eventIdx]);
                eventIdx++;
              },
              onError: (err) => expect(err, myErr),
              cancelOnError: true,
            );

            await ctrl.close();
          },
          timeout: timeout5,
        );
      });

      //group(".withInitialDelay(${secs.inSeconds}s)", () {
      //test("cancelOnError=false", () async {
      //final ctrl = StreamController<int>();

      //intsBeforeErr.forEach(ctrl.add);
      //ctrl.addError(myErr);
      //intsAfterErr.forEach(ctrl.add);

      //expect(
      //ctrl.stream.withInitialDelay(microsecs),
      //emitsInOrder([
      //...intsBeforeErr,
      //emitsError(myErr),
      //...intsAfterErr,
      //emitsDone,
      //]),
      //);

      //await ctrl.close();
      //});

      //test(
      //"cancelOnError=true",
      //() async {
      //final ctrl = StreamController<int>();

      //intsBeforeErr.forEach(ctrl.add);
      //ctrl.addError(myErr);
      //intsAfterErr.forEach(ctrl.add);

      //var eventIdx = 0;
      //ctrl.stream.withInitialDelay(secs).listen(
      //(e) {
      //expect(e, intsBeforeErr[eventIdx]);
      //eventIdx++;
      //},
      //onError: (err) => expect(err, myErr),
      //cancelOnError: true,
      //);

      //await ctrl.close();
      //},
      //timeout: timeout5,
      //);
      //});
    });

    group("Propagate cancellation:", () {
      test("WITHOUT .withInitialDelay", () async {
        final events = List.generate(
          rnd.nextIntBetween(20, 1000),
          (_) => rnd.nextInt(1234567),
        );

        final stream = Stream.fromIterable(events);

        var hadError = false;
        var isDone = false;
        var isCancelled = false;
        final comp = Completer<void>();

        var eventIdx = 0;
        final cancelIdx = rnd.nextIntBetween(4, events.length ~/ 2);

        late final StreamSubscription<int> sub;
        sub = stream.listen(
          (e) async {
            expectFalse(isCancelled);
            expect(e, events[eventIdx]);
            eventIdx++;

            if (eventIdx == cancelIdx) {
              await sub.cancel();
              isCancelled = true;
              comp.complete();
            }
          },
          onError: (_, __) => hadError = true,
          onDone: () => isDone = true,
        );

        await comp.future;

        expect(eventIdx, cancelIdx);
        expectTrue(isCancelled);
        expectFalse(hadError);
        expectFalse(isDone);
      });

      test(".withInitialDelay(${microsecs.inMicroseconds}us)", () async {
        final events = List.generate(
          rnd.nextIntBetween(20, 1000),
          (_) => rnd.nextInt(1234567),
        );

        final stream = Stream.fromIterable(events);

        var hadError = false;
        var isDone = false;
        var isCancelled = false;
        final comp = Completer<void>();

        var eventIdx = 0;
        final cancelIdx = rnd.nextIntBetween(4, events.length ~/ 2);

        late final StreamSubscription<int> sub;
        sub = stream.withInitialDelay(microsecs).listen(
          (e) async {
            expectFalse(isCancelled);
            expect(e, events[eventIdx]);
            eventIdx++;

            if (eventIdx == cancelIdx) {
              await sub.cancel();
              isCancelled = true;
              comp.complete();
            }
          },
          onError: (_, __) => hadError = true,
          onDone: () => isDone = true,
        );

        await comp.future;

        expect(eventIdx, cancelIdx);
        expectTrue(isCancelled);
        expectFalse(hadError);
        expectFalse(isDone);
      });

      test(".withInitialDelay(${secs.inSeconds}s)", () async {
        final events = List.generate(
          rnd.nextIntBetween(20, 1000),
          (_) => rnd.nextInt(1234567),
        );

        final stream = Stream.fromIterable(events);

        var hadError = false;
        var isDone = false;
        var isCancelled = false;
        final comp = Completer<void>();

        var eventIdx = 0;
        final cancelIdx = rnd.nextIntBetween(4, events.length ~/ 2);

        late final StreamSubscription<int> sub;
        sub = stream.withInitialDelay(secs).listen(
          (e) async {
            expectFalse(isCancelled);
            expect(e, events[eventIdx]);
            eventIdx++;

            if (eventIdx == cancelIdx) {
              await sub.cancel();
              isCancelled = true;
              comp.complete();
            }
          },
          onError: (_, __) => hadError = true,
          onDone: () => isDone = true,
        );

        await comp.future;

        expect(eventIdx, cancelIdx);
        expectTrue(isCancelled);
        expectFalse(hadError);
        expectFalse(isDone);
      });
    });

    final pauseDuration = secs * 2;
    group("Propagate pause(${pauseDuration.inSeconds}s) and resume:", () {
      test("WITHOUT .withInitialDelay", () async {
        final events = List.generate(
          100,
          (_) => rnd.nextInt(1234567),
        );

        final stream = Stream.fromIterable(events);

        var hadError = false;
        var isDone = false;
        var isPaused = false;
        final doneComp = Completer<void>();

        var eventIdx = 0;
        const pauseIdx = 20;

        late final StreamSubscription<int> sub;
        sub = stream.listen(
          (e) async {
            expectFalse(isPaused);
            expect(e, events[eventIdx]);
            eventIdx++;

            if (eventIdx == pauseIdx) {
              sub.pause(
                Future.delayed(pauseDuration, () {
                  expectTrue(isPaused);
                  isPaused = false;
                }),
              );
              isPaused = true;
            }
          },
          onError: (_, __) => hadError = true,
          onDone: () {
            isDone = true;
            doneComp.complete();
          },
        );

        await doneComp.future;
        await sub.cancel();

        expectFalse(isPaused);
        expectFalse(hadError);
        expectTrue(isDone);
      });

      test(".withInitialDelay(${microsecs.inMicroseconds}us)", () async {
        final events = List.generate(
          100,
          (_) => rnd.nextInt(1234567),
        );

        final stream = Stream.fromIterable(events);

        var hadError = false;
        var isDone = false;
        var isPaused = false;
        final doneComp = Completer<void>();

        var eventIdx = 0;
        const pauseIdx = 20;

        late final StreamSubscription<int> sub;
        sub = stream.withInitialDelay(microsecs).listen(
              (e) async {
                expectFalse(isPaused);
                expect(e, events[eventIdx]);
                eventIdx++;

                if (eventIdx == pauseIdx) {
                  sub.pause(
                    Future.delayed(pauseDuration, () {
                      expectTrue(isPaused);
                      isPaused = false;
                    }),
                  );
                  isPaused = true;
                }
              },
              onError: (_, __) => hadError = true,
              onDone: () {
                isDone = true;
                doneComp.complete();
              },
            );

        await doneComp.future;
        await sub.cancel();

        expectFalse(isPaused);
        expectFalse(hadError);
        expectTrue(isDone);
      });

      test(".withInitialDelay(${secs.inSeconds}s)", () async {
        final events = List.generate(
          100,
          (_) => rnd.nextInt(1234567),
        );

        final stream = Stream.fromIterable(events);

        var hadError = false;
        var isDone = false;
        var isPaused = false;
        final doneComp = Completer<void>();

        var eventIdx = 0;
        const pauseIdx = 20;

        late final StreamSubscription<int> sub;
        sub = stream.withInitialDelay(secs).listen(
              (e) async {
                expectFalse(isPaused);
                expect(e, events[eventIdx]);
                eventIdx++;

                if (eventIdx == pauseIdx) {
                  sub.pause(
                    Future.delayed(pauseDuration, () {
                      expectTrue(isPaused);
                      isPaused = false;
                    }),
                  );
                  isPaused = true;
                }
              },
              onError: (_, __) => hadError = true,
              onDone: () {
                isDone = true;
                doneComp.complete();
              },
            );

        await doneComp.future;
        await sub.cancel();

        expectFalse(isPaused);
        expectFalse(hadError);
        expectTrue(isDone);
      });
    });

    group(
      "Propagate pause(${pauseDuration.inSeconds}s) before "
      "first event and resume:",
      () {
        test(".withInitialDelay(${secs.inSeconds}s)", () async {
          final events = List.generate(
            5,
            (_) => rnd.nextInt(1234567),
          );

          final stream = Stream.fromIterable(events);

          var hadError = false;
          var isDone = false;
          var isPaused = false;
          final doneComp = Completer<void>();

          var eventIdx = 0;

          late final StreamSubscription<int> sub;
          final firstEventWatch = Stopwatch();

          firstEventWatch.start();
          sub = stream.withInitialDelay(secs).listen(
                (e) {
                  firstEventWatch.stop();
                  expectFalse(isPaused);
                  expect(e, events[eventIdx]);
                  eventIdx++;
                },
                onError: (_, __) => hadError = true,
                onDone: () {
                  isDone = true;
                  doneComp.complete();
                },
              );

          firstEventWatch.stop();
          sub.pause(
            Future.delayed(pauseDuration, () {
              expectTrue(isPaused);
              firstEventWatch.start();
              isPaused = false;
            }),
          );
          isPaused = true;

          await doneComp.future;
          await sub.cancel();

          // Ignore microsecs for this test, because different platforms
          // (VM, web, WASM) may stop and start [firstEventWatch] a bit
          // later/faster which causes the value of [firstEventWatch.elapsed]
          // to not exactly match [secs] (by a few microseconds).
          expect(
            firstEventWatch.elapsed.inMilliseconds,
            inInclusiveRange(secs.inMilliseconds - 1, secs.inMilliseconds + 1),
          );

          expect(firstEventWatch.elapsed, lessThan(pauseDuration));

          expect(eventIdx, events.length);
          expectFalse(isPaused);
          expectFalse(hadError);
          expectTrue(isDone);
        });
      },
    );
  });
}

/// Generates 5 random integers.
Stream<int> _gen({
  bool shouldThrow = true,
  Object? error,
  int? errIdx,
}) async* {
  const count = 5;
  assert(null == errIdx || errIdx < count);

  errIdx ??= Random().nextIntBetween(0, count);
  error = Exception();
  for (var i = 0; i < count; i++) {
    // ignore: only_throw_errors
    if (shouldThrow && i == errIdx) throw error;
    yield i;
  }
}

Stream<int> _infiniteGen() async* {
  final rnd = Random();
  while (true) {
    final val = rnd.nextInt(0xFFFFFFFF);
    await Future.delayed(const Duration(milliseconds: 100));
    yield val;
  }
}

// ignore: unused_element
void _myPrint(Object? obj) {
  final time = DateTime.now().toIso8601String();
  print("[$time]: $obj");
}
