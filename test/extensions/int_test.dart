import "package:my_utility/extensions/int.dart";
import "package:my_utility/src/constants.dart";
import "package:my_utility/test.dart";

void main() {
  group("MyUtilityExtensionIntIsUnsigned:", () {
    group("VM & JavaScript:", () {
      test("isUnsigned(1)", () {
        expectTrue(0x00.isUnsigned(1));
        expectTrue(0xFF.isUnsigned(1));
        expectFalse(0xFF01.isUnsigned(1));
        expectFalse(0xFFFF.isUnsigned(1));
        expectFalse(0xFFFFFF.isUnsigned(1));
        expectFalse(0xFFFFFFFF.isUnsigned(1));
        expectFalse(0xFFFFFFFFFF.isUnsigned(1));
        expectFalse(0xFFFFFFFFFFFF.isUnsigned(1));
      });

      test("isUnsigned(2)", () {
        expectTrue(0x00.isUnsigned(2));
        expectTrue(0xFF.isUnsigned(2));
        expectTrue(0xFFFF.isUnsigned(2));
        expectFalse(0xFFFF01.isUnsigned(2));
        expectFalse(0xFFFFFF.isUnsigned(2));
        expectFalse(0xFFFFFFFF.isUnsigned(2));
        expectFalse(0xFFFFFFFFFF.isUnsigned(2));
        expectFalse(0xFFFFFFFFFFFF.isUnsigned(2));
      });

      test("isUnsigned(3)", () {
        expectTrue(0x00.isUnsigned(3));
        expectTrue(0xFF.isUnsigned(3));
        expectTrue(0xFFFF.isUnsigned(3));
        expectTrue(0xFFFFFF.isUnsigned(3));
        expectFalse(0xFFFFFF01.isUnsigned(3));
        expectFalse(0xFFFFFFFF.isUnsigned(3));
        expectFalse(0xFFFFFFFFFF.isUnsigned(3));
        expectFalse(0xFFFFFFFFFFFF.isUnsigned(3));
      });

      test("isUnsigned(4)", () {
        expectTrue(0x00.isUnsigned(4));
        expectTrue(0xFF.isUnsigned(4));
        expectTrue(0xFFFF.isUnsigned(4));
        expectTrue(0xFFFFFF.isUnsigned(4));
        expectTrue(0xFFFFFFFF.isUnsigned(4));
        expectFalse(0xFFFFFFFF01.isUnsigned(4));
        expectFalse(0xFFFFFFFFFF.isUnsigned(4));
        expectFalse(0xFFFFFFFFFFFF.isUnsigned(4));
      });

      test("isUnsigned(5)", () {
        expectTrue(0x00.isUnsigned(5));
        expectTrue(0xFF.isUnsigned(5));
        expectTrue(0xFFFF.isUnsigned(5));
        expectTrue(0xFFFFFF.isUnsigned(5));
        expectTrue(0xFFFFFFFF.isUnsigned(5));
        expectTrue(0xFFFFFFFFFF.isUnsigned(5));
        expectFalse(0xFFFFFFFFFF01.isUnsigned(5));
        expectFalse(0xFFFFFFFFFFFF.isUnsigned(5));
      });

      test("isUnsigned(6)", () {
        expectTrue(0x00.isUnsigned(6));
        expectTrue(0xFF.isUnsigned(6));
        expectTrue(0xFFFF.isUnsigned(6));
        expectTrue(0xFFFFFF.isUnsigned(6));
        expectTrue(0xFFFFFFFF.isUnsigned(6));
        expectTrue(0xFFFFFFFFFF.isUnsigned(6));
        expectTrue(0xFFFFFFFFFFFF.isUnsigned(6));
      });
    });

    group(
      "VM only:",
      () {
        test("isUnsigned(1)", () {
          expectFalse(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(1));
          expectFalse(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(1));
        });

        test("isUnsigned(2)", () {
          expectFalse(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(2));
          expectFalse(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(2));
        });

        test("isUnsigned(3)", () {
          expectFalse(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(3));
          expectFalse(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(3));
        });

        test("isUnsigned(4)", () {
          expectFalse(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(4));
          expectFalse(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(4));
        });

        test("isUnsigned(5)", () {
          expectFalse(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(5));
          expectFalse(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(5));
        });

        test("isUnsigned(6)", () {
          expectFalse(int.parse("0xFFFFFFFFFFFF01").isUnsigned(6));
          expectFalse(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(6));
          expectFalse(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(6));
        });

        test("isUnsigned(7)", () {
          expectTrue(0x00.isUnsigned(7));
          expectTrue(0xFF.isUnsigned(7));
          expectTrue(0xFFFF.isUnsigned(7));
          expectTrue(0xFFFFFF.isUnsigned(7));
          expectTrue(0xFFFFFFFF.isUnsigned(7));
          expectTrue(0xFFFFFFFFFF.isUnsigned(7));
          expectTrue(0xFFFFFFFFFFFF.isUnsigned(7));
          expectTrue(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(7));
          expectFalse(int.parse("0xFFFFFFFFFFFFFF01").isUnsigned(7));
          expectFalse(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(7));
        });

        test("isUnsigned(8)", () {
          expectTrue(0x00.isUnsigned(8));
          expectTrue(0xFF.isUnsigned(8));
          expectTrue(0xFFFF.isUnsigned(8));
          expectTrue(0xFFFFFF.isUnsigned(8));
          expectTrue(0xFFFFFFFF.isUnsigned(8));
          expectTrue(0xFFFFFFFFFF.isUnsigned(8));
          expectTrue(0xFFFFFFFFFFFF.isUnsigned(8));
          expectTrue(int.parse("0xFFFFFFFFFFFFFF").isUnsigned(8));
          expectTrue(int.parse("0xFFFFFFFFFFFFFFFF").isUnsigned(8));
        });
      },
      testOn: "dart-vm",
    );

    test("isUnsigned with invalid arguments", () {
      expectError<ArgumentError>(() => 0.isUnsigned(0));
      expectError<ArgumentError>(() => 0.isUnsigned(-1));
      expectError<ArgumentError>(() => 0.isUnsigned(9));
      expectError<ArgumentError>(() => 0.isUnsigned(10));
    });
  });

  group("MyUtilityExtensionIntIsUint8:", () {
    for (final val in kMaxIntergersForBytes.skip(1)) {
      test(
        "0x${val.toRadixStringAsUnsigned(16).toUpperCase()}.isUint8",
        () => expectFalse(val.isUint8),
      );
    }

    test("0x01FF.isUint8", () => expectFalse(0x01FF.isUint8));
    test("0xFF.isUint8", () => expectTrue(0xFF.isUint8));
    test("0x00.isUint8", () => expectTrue(0x00.isUint8));
  });

  group("MyUtilityExtensionIntIsUint16:", () {
    for (final val in kMaxIntergersForBytes.skip(2)) {
      test(
        "0x${val.toRadixStringAsUnsigned(16).toUpperCase()}.isUint16",
        () => expectFalse(val.isUint16),
      );
    }

    test("0xFFFF01.isUint16", () => expectFalse(0xFFFF01.isUint16));
    test("0xFFFF.isUint16", () => expectTrue(0xFFFF.isUint16));
    test("0xFF.isUint16", () => expectTrue(0xFF.isUint16));
    test("0x00.isUint16", () => expectTrue(0x00.isUint16));
  });

  group("MyUtilityExtensionIntIsUint32:", () {
    for (final val in kMaxIntergersForBytes.skip(4)) {
      test(
        "0x${val.toRadixStringAsUnsigned(16).toUpperCase()}.isUint32",
        () => expectFalse(val.isUint32),
      );
    }

    test("0xFFFFFFFF01.isUint32", () => expectFalse(0xFFFFFFFF01.isUint32));
    test("0xFFFFFFFF.isUint32", () => expectTrue(0xFFFFFFFF.isUint32));
    test("0xFFFFFF.isUint32", () => expectTrue(0xFFFFFF.isUint32));
    test("0xFFFF.isUint32", () => expectTrue(0xFFFF.isUint32));
    test("0xFF.isUint32", () => expectTrue(0xFF.isUint32));
    test("0x00.isUint32", () => expectTrue(0x00.isUint32));
  });

  group(
    "MyUtilityExtensionIntIsUint64:",
    () {
      for (final val in kMaxIntergersForBytes) {
        test(
          "0x${val.toRadixStringAsUnsigned(16).toUpperCase()}.isUint64",
          () => expectTrue(val.isUint64),
        );
      }
      test("0x00.isUint64", () => expectTrue(0x00.isUint64));
    },
    testOn: "dart-vm",
  );

  group("MyUtilityExtensionIntToRadixStringAsUnsigned:", () {
    void doTest(int value, String expected, int radix) {
      test(
        "$value -> $expected",
        () => expect(value.toRadixStringAsUnsigned(radix), expected),
      );
    }

    group("Binary (base 2):", () {
      doTest(12, "1100", 2);
      doTest(31, "11111", 2);
      doTest(2021, "11111100101", 2);
      doTest(
        -12,
        "11111111"
        "11111111"
        "11111111"
        "11111111"
        "11111111"
        "11111111"
        "11111111"
        "11110100",
        2,
      );

      for (var i = 0; i < kMaxIntergersForBytes.length; i++) {
        doTest(kMaxIntergersForBytes[i], "11111111" * (i + 1), 2);
      }

      doTest(
        kMaxInt,
        kIsWeb
            ? ""
                //"00000000"
                /*000*/ "11111"
                "11111111"
                "11111111"
                "11111111"
                "11111111"
                "11111111"
                "11111111"
            : ""
                /*0*/ "1111111"
                "11111111"
                "11111111"
                "11111111"
                "11111111"
                "11111111"
                "11111111"
                "11111111",
        2,
      );
      doTest(
        kMinInt,
        kIsWeb
            ? ""
                "11111111"
                "11100000"
                "00000000"
                "00000000"
                "00000000"
                "00000000"
                "00000000"
                "00000001"
            : ""
                "10000000"
                "00000000"
                "00000000"
                "00000000"
                "00000000"
                "00000000"
                "00000000"
                "00000000",
        2,
      );
    });

    group("Octal (base 8):", () {
      doTest(12, "14", 8);
      doTest(31, "37", 8);
      doTest(2021, "3745", 8);
      doTest(-12, "1777777777777777777764", 8);
    });

    group("Hexadecimal (base 16):", () {
      doTest(12, "c", 16);
      doTest(31, "1f", 16);
      doTest(2021, "7e5", 16);
      doTest(-12, "fffffffffffffff4", 16);

      for (var i = 0; i < kMaxIntergersForBytes.length; i++) {
        doTest(kMaxIntergersForBytes[i], "ff" * (i + 1), 16);
      }

      doTest(kMaxInt, kIsWeb ? "1fffffffffffff" : "7fffffffffffffff", 16);
      doTest(kMinInt, kIsWeb ? "ffe0000000000001" : "8000000000000000", 16);
    });

    group("Base 36", () {
      doTest(35 * 36 + 1, "z1", 36);
    });
  });
}
