import "package:my_utility/extensions.dart";
import "package:test/test.dart";

void main() {
  group("MyUtilityExtensionStringCamelOrPascalToSnake:", () {
    final doTest = _getTester((e) => e.camelOrPascalToSnake());

    doTest("abcDef", "abc_def");
    doTest("AbcDef", "abc_def");
  });

  group("MyUtilityExtensionStringCapitalize:", () {
    final doTest = _getTester((e) => e.capitalize());

    doTest("abcd", "Abcd");
    doTest("", "");
    doTest("Abcd", "Abcd");
    doTest("ABCD", "Abcd");
  });

  group("MyUtilityExtensionStringDecapitalize:", () {
    final doTest = _getTester((e) => e.decapitalize());

    doTest("abcd", "abcd");
    doTest("", "");
    doTest("Abcd", "abcd");
    doTest("ABCD", "aBCD");
  });

  group("MyUtilityExtensionStringSnakeToCamel:", () {
    final doTest = _getTester((e) => e.snakeToCamel());

    doTest("abc_def", "abcDef");
    doTest("ABC_DEF", "abcDef");
    doTest("ABC", "abc");
    doTest("abc", "abc");
  });

  group("MyUtilityExtensionStringSnakeToPascal:", () {
    final doTest = _getTester((e) => e.snakeToPascal());

    doTest("abc_def", "AbcDef");
    doTest("ABC_DEF", "AbcDef");
    doTest("ABC", "Abc");
    doTest("abc", "Abc");
  });

  group("MyUtilityExtensionStringSplitLine:", () {
    test("short lines", () {
      expect("I like cookies".splitLine(60), ["I like cookies"]);
      expect("I like cookies".splitLine(1), ["I", "like", "cookies"]);
    });

    test("long paragraphs", () {
      expect(
        "Hello it's me and i like cookies and cake and steak very much "
                "but i really have to emphasize how much I like steak "
                "because meat is great and meat makes me happy"
            .splitLine(60),
        [
          "Hello it's me and i like cookies and cake and steak very",
          "much but i really have to emphasize how much I like steak",
          "because meat is great and meat makes me happy",
        ],
      );
    });
  });
}

void Function(String, String) _getTester(
  String Function(String initial) transform,
) {
  return (initial, match) {
    _testStringTransform(initial, match, transform);
  };
}

void _testStringTransform(
  String initial,
  String match,
  String Function(String initial) transform,
) {
  test("$initial -> $match", () {
    expect(transform(initial), match);
  });
}
