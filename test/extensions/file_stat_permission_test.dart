import "package:my_utility/extensions/file_stat_permission.dart";
import "package:my_utility/io/file_stat_permission.dart";
import "package:my_utility/test.dart";

void main() {
  test("MyUtilityExtensionFileStatPermissionCanExecute", () {
    final perms = [FileStatPermission.read];
    expectFalse(perms.canExecute);

    perms.add(FileStatPermission.write);
    expectFalse(perms.canExecute);

    perms.add(FileStatPermission.write);
    expectFalse(perms.canExecute);

    perms.add(FileStatPermission.execute);
    expectTrue(perms.canExecute);
  });

  test("MyUtilityExtensionFileStatPermissionCanRead", () {
    final perms = [FileStatPermission.execute];
    expectFalse(perms.canRead);

    perms.add(FileStatPermission.write);
    expectFalse(perms.canRead);

    perms.add(FileStatPermission.write);
    expectFalse(perms.canRead);

    perms.add(FileStatPermission.read);
    expectTrue(perms.canExecute);
  });

  test("MyUtilityExtensionFileStatPermissionCanWrite", () {
    final perms = [FileStatPermission.execute];
    expectFalse(perms.canWrite);

    perms.add(FileStatPermission.execute);
    expectFalse(perms.canWrite);

    perms.add(FileStatPermission.read);
    expectFalse(perms.canWrite);

    perms.add(FileStatPermission.write);
    expectTrue(perms.canWrite);
  });

  test("MyUtilityExtensionFileStatPermissionToOctalMode", () {
    expect("0", <FileStatPermission>[].toOctalMode());
    expect("7", FileStatPermission.values.toOctalMode());

    expect(
      "7",
      [
        ...FileStatPermission.values,
        ...FileStatPermission.values,
        ...FileStatPermission.values,
        ...FileStatPermission.values,
        ...FileStatPermission.values,
        ...FileStatPermission.values,
        ...FileStatPermission.values,
      ].toOctalMode(),
    );

    expect(
      "6",
      [
        FileStatPermission.read,
        FileStatPermission.write,
      ].toOctalMode(),
    );

    expect(
      "3",
      [
        FileStatPermission.write,
        FileStatPermission.execute,
      ].toOctalMode(),
    );

    expect(
      "5",
      [
        FileStatPermission.read,
        FileStatPermission.execute,
      ].toOctalMode(),
    );

    expect(
      "4",
      [
        FileStatPermission.read,
      ].toOctalMode(),
    );

    expect(
      "2",
      [
        FileStatPermission.write,
      ].toOctalMode(),
    );

    expect(
      "1",
      [
        FileStatPermission.execute,
      ].toOctalMode(),
    );
  });

  group("MyUtilityExtensionFileStatPermissionOperatorPipe:", () {
    test("{} | read", () {
      final perms = <FileStatPermission>{};
      final newPerms = perms | FileStatPermission.read;

      expectFalse(identical(perms, newPerms));

      expectFalse(perms.canRead);
      expect(perms.length, 0);

      expectTrue(newPerms.canRead);
      expect(newPerms.length, 1);
    });

    test("{ read } | read", () {
      final perms = {FileStatPermission.read};
      final newPerms = perms | FileStatPermission.read;

      expectFalse(identical(perms, newPerms));

      expectTrue(perms.canRead);
      expect(perms.length, 1);

      expectTrue(newPerms.canRead);
      expect(newPerms.length, 1);
    });

    test("{ read, write } | execute ", () {
      final perms = {FileStatPermission.read, FileStatPermission.write};
      final newPerms = perms | FileStatPermission.execute;

      expectFalse(identical(perms, newPerms));

      expectTrue(perms.canRead);
      expectTrue(perms.canWrite);
      expectFalse(perms.canExecute);
      expect(perms.length, 2);

      expectTrue(newPerms.canRead);
      expectTrue(newPerms.canWrite);
      expectTrue(newPerms.canExecute);
      expect(newPerms.length, 3);
    });

    test("[ read, read, read ] | execute ", () {
      final perms = [
        FileStatPermission.read,
        FileStatPermission.read,
        FileStatPermission.read,
      ];
      final newPerms = perms | FileStatPermission.execute;

      expectFalse(identical(perms, newPerms));

      expectTrue(perms.canRead);
      expectFalse(perms.canExecute);
      expect(perms.length, 3);

      expectTrue(newPerms.canRead);
      expectTrue(newPerms.canExecute);
      expect(newPerms.length, 2);
    });
  });
}
