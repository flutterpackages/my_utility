@TestOn("dart-vm")
library;

import "package:my_utility/errors/implementation_error.dart";
import "package:my_utility/reflection/program_info.dart";
import "package:my_utility/test.dart";

void main() {
  group(
    "ProgramInfo:",
    () {
      test(
        "current",
        () {
          final info = _myAbcFunction();
          expect(info.function, "_myAbcFunction");
        },
      );

      test(
        "withFrameOffset",
        () {
          try {
            _myAbcFunction2();
            // ignore: avoid_catches_without_on_clauses
          } catch (err) {
            expect(err, isA<ImplementationError>());
            expect(
              (err as ImplementationError).method,
              "_myAbcFunction2",
            );
          }
        },
      );
    },
  );
}

ProgramInfo _myAbcFunction() {
  return ProgramInfo.current();
}

ProgramInfo _myAbcFunction2() {
  final info = ProgramInfo.withFrameOffset(1);
  throw ImplementationError(info.function);
}
