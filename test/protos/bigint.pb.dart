//
//  Generated code. Do not modify.
//  source: bigint.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class BigInt extends $pb.GeneratedMessage {
  factory BigInt({
    $core.String? value,
  }) {
    final $result = create();
    if (value != null) {
      $result.value = value;
    }
    return $result;
  }
  BigInt._() : super();
  factory BigInt.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BigInt.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'BigInt', createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'value')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BigInt clone() => BigInt()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BigInt copyWith(void Function(BigInt) updates) => super.copyWith((message) => updates(message as BigInt)) as BigInt;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static BigInt create() => BigInt._();
  BigInt createEmptyInstance() => create();
  static $pb.PbList<BigInt> createRepeated() => $pb.PbList<BigInt>();
  @$core.pragma('dart2js:noInline')
  static BigInt getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BigInt>(create);
  static BigInt? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get value => $_getSZ(0);
  @$pb.TagNumber(1)
  set value($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValue() => $_has(0);
  @$pb.TagNumber(1)
  void clearValue() => clearField(1);
}

class BigIntList extends $pb.GeneratedMessage {
  factory BigIntList({
    $core.Iterable<BigInt>? values,
  }) {
    final $result = create();
    if (values != null) {
      $result.values.addAll(values);
    }
    return $result;
  }
  BigIntList._() : super();
  factory BigIntList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory BigIntList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'BigIntList', createEmptyInstance: create)
    ..pc<BigInt>(1, _omitFieldNames ? '' : 'values', $pb.PbFieldType.PM, subBuilder: BigInt.create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  BigIntList clone() => BigIntList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  BigIntList copyWith(void Function(BigIntList) updates) => super.copyWith((message) => updates(message as BigIntList)) as BigIntList;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static BigIntList create() => BigIntList._();
  BigIntList createEmptyInstance() => create();
  static $pb.PbList<BigIntList> createRepeated() => $pb.PbList<BigIntList>();
  @$core.pragma('dart2js:noInline')
  static BigIntList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<BigIntList>(create);
  static BigIntList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<BigInt> get values => $_getList(0);
}


const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames = $core.bool.fromEnvironment('protobuf.omit_message_names');
