//
//  Generated code. Do not modify.
//  source: bigint.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use bigIntDescriptor instead')
const BigInt$json = {
  '1': 'BigInt',
  '2': [
    {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

/// Descriptor for `BigInt`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bigIntDescriptor = $convert.base64Decode(
    'CgZCaWdJbnQSFAoFdmFsdWUYASABKAlSBXZhbHVl');

@$core.Deprecated('Use bigIntListDescriptor instead')
const BigIntList$json = {
  '1': 'BigIntList',
  '2': [
    {'1': 'values', '3': 1, '4': 3, '5': 11, '6': '.BigInt', '10': 'values'},
  ],
};

/// Descriptor for `BigIntList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List bigIntListDescriptor = $convert.base64Decode(
    'CgpCaWdJbnRMaXN0Eh8KBnZhbHVlcxgBIAMoCzIHLkJpZ0ludFIGdmFsdWVz');

