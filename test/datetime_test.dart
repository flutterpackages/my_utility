import "package:my_utility/extensions.dart";
import "package:test/test.dart";

void main() {
  group("DateTimeMyUtilityExtension", () {
    test("toTimestamp", () {
      var d = DateTime.utc(
        2023,
        1,
        1,
        0,
        0,
        0,
        0,
        0,
      );
      expect(d.toTimestamp(includeFractions: false), "20230101T000000Z");
      expect(d.toTimestamp(), "20230101T000000.000Z");

      d = DateTime.utc(
        2023,
        1,
        1,
        0,
        0,
        0,
        0,
        123,
      );
      expect(d.toTimestamp(), "20230101T000000.000123Z");

      d = DateTime.utc(
        2023,
        1,
        1,
        0,
        0,
        0,
        0,
        12,
      );
      expect(d.toTimestamp(), "20230101T000000.000012Z");
    });

    test("toHumanReadableTimestamp", () {
      final date = DateTime(2024, 5, 7, 5, 9, 4);
      final timezone = date.timeZoneName;
      expect(
        date.toHumanReadableTimestamp("en_US"),
        "Tue\t7 May 05:09:04 $timezone 2024",
      );
    });
  });
}
