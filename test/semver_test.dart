import "package:my_utility/semver.dart";
import "package:my_utility/test/test.dart";

void main() {
  group("SemVer", () {
    test("find", () {
      final res = SemVer.find("NOTE: This rule is removed in Dart 3.3.0");
      expect(res, [SemVer(3, 3, 0)]);
    });
  });
}
