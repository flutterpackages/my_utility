// ignore_for_file: deprecated_member_use_from_same_package

import "dart:math";

import "package:my_utility/async/completable_future.dart";
import "package:my_utility/extensions/random.dart";
import "package:my_utility/test/expect.dart";
import "package:test/test.dart";

void main() {
  test("CompletableFuture.delayed", () async {
    final value = Random().nextAsciiString(20);
    final future = CompletableFuture.delayed(
      const Duration(milliseconds: 500),
      () => value,
    );

    expectError<StateError>(() => future.result);

    expect(future.isCompleted, false);
    expect(await future, value);

    expect(future.isCompleted, true);
    expect(future.hasCompletedWithValue, true);
    expect(future.hasCompletedWithError, false);

    expectType<CompletableFutureValue<String>>(future.result);
    expect(future.value, value);
  });
}
