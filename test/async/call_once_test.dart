import "dart:math";

import "package:my_utility/async/call_once.dart";
import "package:my_utility/test.dart";

void main() {
  test("success", () async {
    final instance = _CallOnceTestSuccess();

    const theValue = 1234;
    const count = 100;
    final randomVals =
        List.generate(count, (index) => Random().nextInt(100000));

    final results = await Future.wait([
      instance.assign(theValue),
      for (var i = 0; i < count; i++) instance.assign(randomVals[i]),
    ]);
    expectIterable(results, List.filled(count + 1, true));

    expect(instance.value, theValue);
  });

  test("closure result error", () async {
    final instance = _CallOnceTestTypeError();
    expectError<CallOnceClosureResultError<bool>>(
      () => instance.assign(1234),
    );
  });

  test("invalid arguments error", () async {
    final instance = _CallOnceTestNoSuchMethodError();
    expectError<CallOnceInvalidArgumentsError>(() => instance.assign(1234));
  });
}

//

class _CallOnceTestSuccess {
  _CallOnceTestSuccess() {
    // ignore: avoid_types_on_closure_parameters
    _assignImpl = CallOnce((int value) async {
      final ms = 100 + Random().nextInt(500);
      await Future.delayed(Duration(milliseconds: ms));

      this.value = value;

      return true;
    });
  }

  late int value;
  late final CallOnce<bool> _assignImpl;

  Future<bool> assign(int newValue) => _assignImpl([newValue]);
}

class _CallOnceTestTypeError {
  _CallOnceTestTypeError() {
    // ignore: avoid_types_on_closure_parameters
    _assignImpl = CallOnce((int value) {
      this.value = value;
      return 1234;
    });
  }

  late int value;
  late final CallOnce<bool> _assignImpl;

  Future<bool> assign(int newValue) => _assignImpl([newValue]);
}

class _CallOnceTestNoSuchMethodError {
  _CallOnceTestNoSuchMethodError() {
    // ignore: avoid_types_on_closure_parameters
    _assignImpl = CallOnce((int value) {
      this.value = value;
      return true;
    });
  }

  static const symbol = #nonExistent;

  late int value;
  late final CallOnce<bool> _assignImpl;

  Future<bool> assign(int newValue) => _assignImpl([newValue], {symbol: 123});
}
