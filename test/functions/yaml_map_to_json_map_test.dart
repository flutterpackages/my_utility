// ignore_for_file: avoid_dynamic_calls

import "package:my_utility/functions/yaml_map_to_json_map.dart";
import "package:my_utility/test.dart";
import "package:yaml/yaml.dart";

void main() {
  test("YamlMap.toJsonMap without converters", () {
    const rawYaml = """
a:
  b:
    - 1
    - 2
    - 3
  flag: true

c:
  - foo
  - bar
  - foo bar
  - bar foo
""";

    final YamlMap yaml = loadYaml(rawYaml);
    expectType<YamlMap>(yaml["a"]);
    expectType<YamlList>(yaml["a"]["b"]);
    expectType<bool>(yaml["a"]["flag"]);
    expectType<YamlList>(yaml["c"]);

    final json = yaml.toJsonMap();
    expectType<Map<String, dynamic>>(json["a"]);
    expectType<List<dynamic>>(json["a"]["b"]);
    expectType<bool>(json["a"]["flag"]);
    expectType<List<dynamic>>(json["c"]);
  });

  test("YamlMap.toJsonMap with converters", () {
    final yaml = YamlMap.wrap(<dynamic, dynamic>{
      "a": {
        "b": [1, 2, 3],
        "flag": true,
      },
      "c": ["foo", "bar", "foo bar", "bar foo"],
      "d": DateTime.now(),
    });

    final jsonWithoutConverter = yaml.toJsonMap();
    expectType<Map<String, dynamic>>(jsonWithoutConverter["a"]);
    expectType<List<dynamic>>(jsonWithoutConverter["a"]["b"]);
    expectType<bool>(jsonWithoutConverter["a"]["flag"]);
    expectType<List<dynamic>>(jsonWithoutConverter["c"]);
    expectType<DateTime>(jsonWithoutConverter["d"]);

    final jsonWithConverter = YamlMap.wrap(yaml).toJsonMap(
      converters: {
        ToJsonObjectCallbackConverter<DateTime>(
          (value) => value.microsecondsSinceEpoch,
        ),
      },
    );
    expectType<Map<String, dynamic>>(jsonWithConverter["a"]);
    expectType<List<dynamic>>(jsonWithConverter["a"]["b"]);
    expectType<bool>(jsonWithConverter["a"]["flag"]);
    expectType<List<dynamic>>(jsonWithConverter["c"]);
    expectType<int>(jsonWithConverter["d"]);
  });
}
