import "package:my_utility/data_units.dart";
import "package:test/test.dart";

void main() {
  group("MyUtilityExtensionBinaryPrefixNumToKibi:", () {
    test("from BinaryPrefix.none", () {
      expect(BinaryPrefix.kibi.factor.toKibi(), 1);
      expect(2048.toKibi(), 2);
    });

    test("from BinaryPrefix.kibi", () {
      expect(1.toKibi(BinaryPrefix.kibi), 1);
    });

    test("from BinaryPrefix.mebi", () {
      expect(
        1.toKibi(BinaryPrefix.mebi),
        BinaryPrefix.kibi.factor.toDouble(),
      );
    });

    test("from BinaryPrefix.gibi", () {
      expect(
        1.toKibi(BinaryPrefix.gibi),
        BinaryPrefix.mebi.factor.toDouble(),
      );
    });

    test("from BinaryPrefix.tebi", () {
      expect(
        1.toKibi(BinaryPrefix.tebi),
        BinaryPrefix.gibi.factor.toDouble(),
      );
    });

    test("from BinaryPrefix.pebi", () {
      expect(
        1.toKibi(BinaryPrefix.pebi),
        BinaryPrefix.tebi.factor.toDouble(),
      );
    });
  });

  group("MyUtilityExtensionBinaryPrefixNumToMebi:", () {
    test("from BinaryPrefix.none", () {
      expect(BinaryPrefix.mebi.factor.toMebi(), 1);
      expect(2048.toMebi(BinaryPrefix.kibi), 2);
    });
  });

  group("MyUtilityExtensionBinaryPrefixNumToGibi:", () {
    test("from BinaryPrefix.none", () {
      expect(BinaryPrefix.gibi.factor.toGibi(), 1);
      expect(2048.toGibi(BinaryPrefix.mebi), 2);
    });
  });

  group("MyUtilityExtensionBinaryPrefixNumToTebi:", () {
    test("from BinaryPrefix.none", () {
      expect(BinaryPrefix.tebi.factor.toTebi(), 1);
      expect(2048.toTebi(BinaryPrefix.gibi), 2);
    });
  });

  group("MyUtilityExtensionBinaryPrefixNumToPebi:", () {
    test("from BinaryPrefix.none", () {
      expect(BinaryPrefix.pebi.factor.toPebi(), 1);
      expect(2048.toPebi(BinaryPrefix.tebi), 2);
    });
  });
}
