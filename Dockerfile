# syntax=docker/dockerfile:1

FROM dart:stable AS dart

# Download and build protoc-gen-dart.
WORKDIR /protoc/plugin
RUN dart pub unpack protoc_plugin \
    && cd /protoc/plugin/protoc_plugin* \
    && dart compile exe -o /protoc/plugin/protoc-gen-dart bin/protoc_plugin.dart

FROM debian:bookworm-slim AS base

# Copy protoc plugin for dart stage.
COPY --from=dart /protoc/plugin/protoc-gen-dart /bin

# Install wget.
RUN apt update && apt install -y wget unzip

# Download protoc and update path
WORKDIR /app/protoc
RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v29.3/protoc-29.3-linux-x86_64.zip \
    && unzip protoc-29.3-linux-x86_64.zip
ENV PATH="/app/protoc/bin:$PATH"

# Run protoc on every .proto file.
WORKDIR /app
COPY --chmod=755 proto/ ./proto
RUN mkdir proto_out && chmod 755 proto_out
RUN cd proto && find . -type f -exec protoc --dart_out=/app/proto_out "{}" \; 
RUN find proto_out -type f -name "*.dart" -exec chmod 666 "{}" \;
