# 1.6.3

  * Improved `MyUtilityExtensionIntToRadixStringAsUnsigned` on platform web.

  * Fixed bugs in library [lib/data_units.dart](lib/data_units.dart) and added library [lib/extensions/binary_prefix.dart](lib/extensions/binary_prefix.dart).

  * Improved test suite.

# 1.6.2

  * Put debug-only code segments in `assert(...)` statements.

# 1.6.1

  * Bugfix in `MyUtilityExtensionIntToRadixStringAsUnsigned`: The conversion did not work correctly for all numbers. Also only binary (base 2) and hexadecimal (base 16) was supported. Now this function works like `int.toRadixString` from the core library. 

  * Bugfix in `MyUtilityExtensionFutureWithDelay`: The delay now also applies to errors.

  * Bugfix in `MyUtilityExtensionStreamWithInitialDelay`: Previously errors thrown by the source stream have been sent into the void and the initial delay handling when the subscription was paused/resumed did also not work correctly. This has been fixed by creating a custom `StreamTransformer`.

  * Now ignoring linter warnings for `parameter_assignments`.

# 1.6.0
  
  * Removed dependency on package [random_string](https://pub.dev/packages/random_string).

  * Added function `expectRegexMatch` to library [lib/test/expect.dart](lib/test/expect.dart).

  * Modified library [lib/extensions/random.dart](lib/extensions/random.dart):

    - Added extension `MyUtilityExtensionRandomNextAlphaNumericString`

    - Deprecated args `asciiStart` and `asciiEnd` in function `MyUtilityExtensionRandomNextAsciiString.nextAsciiString`.

# 1.5.14
  
  * Added library [lib/functions/is_nullable_type.dart](lib/functions/is_nullable_type.dart).

  * Added library [lib/errors/argument_error_utils.dart](lib/errors/argument_error_utils.dart).

  * Updated library [lib/src/int_extension.dart](lib/src/int_extension.dart) to be platform agnostic (can now be used when compiling to JavaScript).

# 1.5.13

  * Added library [lib/extensions/future.dart](lib/extensions/future.dart).

  * Added library [lib/extensions/int.dart](lib/extensions/int.dart).

  * Added extension `MyUtilityExtensionStreamWithInitialDelay` to [lib/extensions/stream.dart](lib/extensions/stream.dart).

# 1.5.12

  * Added library [lib/extensions/deep_copy.dart](lib/extensions/deep_copy.dart).

  * Added Dockerfile for building Protobuf models for Dart.

  * Updated deprecation notices to match SemVer expectations, deprecated features will now be removed in version `2.0.0`.

# 1.5.11

  * Bugfix in function `yamlMapToJsonMap`: forgot to pass `converters` to recursive calls of `yamlMapToJsonMap`.

# 1.5.10

  * Deprecated functions `pascalToSnake`, `camelToSnake`, `snakeToPascal` and library [lib/string.dart](lib/string.dart). 

  * Modified library [lib/extensions/string.dart](lib/extensions/string.dart): Added extensions `MyUtilityExtensionStringCamelOrPascalToSnake`, `MyUtilityExtensionStringDecapitalize`, `MyUtilityExtensionStringSnakeToCamel` and `MyUtilityExtensionStringSnakeToPascal`.

# 1.5.9

  * Added getter `originalExecutable` to class `CliTool`

# 1.5.8

  * Added constructors `CliStreamConsumer.fromVoid` and `CliStreamConsumer.fromFake` to class `CliStreamConsumer`.

  * Added constructor parameter `windowsCodePage` to every subclass of `CliTool`.

  * Added new CLI tool libraries: 

    - [lib/cli/chmod.dart](lib/cli/chmod.dart)

    - [lib/cli/git.dart](lib/cli/git.dart)

    - [lib/cli/dotnet.dart](lib/cli/dotnet.dart)

    - [lib/cli/elixir.dart](lib/cli/elixir.dart)

    - [lib/cli/elixir_mix.dart](lib/cli/elixir_mix.dart)

    - [lib/cli/pandoc.dart](lib/cli/pandoc.dart)

  * Renamed `MyUtilityExtensionSetFileStatePermissionCanWrite` to `MyUtilityExtensionFileStatPermissionCanWrite`.

  * Renamed `MyUtilityExtensionSetFileStatePermissionCanRead` to `MyUtilityExtensionFileStatPermissionCanRead`.

  * Renamed `MyUtilityExtensionSetFileStatePermissionCanExecute` to `MyUtilityExtensionFileStatPermissionCanExecute`.

  * Added extensions `MyUtilityExtensionFileStatPermissionToOctalMode` and `MyUtilityExtensionFileStatPermissionOperatorPipe` for `FileStatPermission`.

# 1.5.7

  * Forgot to commit library [lib/extensions/stream.dart](lib/extensions/stream.dart) in last version.

  * Changes in [lib/cli/cli_consumer.dart](lib/cli/cli_consumer.dart)

    - Deprecated `NullConsumer` and replaced it with `VoidConsumer`.

    - Added class `FakeConsumer`.

  * Added library [lib/errors/generic_error.dart](lib/errors/generic_error.dart).

  * Added library [lib/cli/fvm.dart](lib/cli/fvm.dart).

  * Added functions `getDartCliToolSync` and `getDartCliTool` to class `FlutterCliTool` in library [lib/cli/flutter.dart](lib/cli/flutter.dart).

# 1.5.6

  * Added library [lib/extensions/stream.dart](lib/extensions/stream.dart).

# 1.5.5

  * Fixed type on line 108 in library [lib/cli/ffmpeg.dart](lib/cli/ffmpeg.dart).

# 1.5.4

  * Removed deprecated methods from `FFProbeCliTool` and added getter `isMuxed` to enum `MediaFormat` in library [lib/cli/ffprobe.dart](lib/cli/ffprobe.dart). 

# 1.5.3

  * Made constructor for class `FlutterVersion` public.

# 1.5.2

  * Now exporting class `FlutterVersion` from [lib/cli/flutter.dart](lib/cli/flutter.dart).

# 1.5.1

  * Changed dependency constraints for packages.

# 1.5.0

  * Added dependency [async](https://pub.dev/packages/async).

  * Added dependency [io](https://pub.dev/packages/io).

  * Added library [math.dart](lib/math.dart).

  * Added library [result.dart](lib/result.dart).

  * Deprecated library [lib/async/completable_future.dart](lib/async/completable_future.dart).
  
  * Modified library [lib/extensions.dart](lib/extensions.dart): Split all extensions on classes into multiple extensions so that each extension only contains one member (one method, or one getter, ...). Also prefixed every extension with `MyUtilityExtension`.

  * Removed extension method `MyUtilityExtensionDirectory.subdir`

  * Removed `MyUtilityExtensionStdinReadLineAsync`. Use [SharedStdin.nextLine](https://pub.dev/documentation/io/latest/io/SharedStdIn/nextLine.html) from [package io](https://pub.dev/packages/io) instead.

  * Modified extensions for class `Directory`:
    
    - Turned `getUniqueFilename` async and added `getUniqueFilenameSync`.

  * Modified extensions for class `FileSystemEntity`:

    - Renamed `dirName` to `dirname`.

    - Moved getter `extension` to extensions for class `File`.

    - Removed method `withName`.

  * Modified library [lib/io/interactive.dart](lib/io/interactive.dart)

    * Turned `promptYesNo` async and added `promptYesNoSync`.

    * Turned `promptUntilValidPick` async and added `promptUntilValidPickSync`.

# 1.4.4

  * Added library [lib/extensions/stdin.dart](lib/extensions/stdin.dart).

# 1.4.3

  * Added library [lib/functions/yaml_map_to_json_map.dart](lib/functions/yaml_map_to_json_map.dart).

# 1.4.2

  * Added parameters `environment` and `includeParentEnvironment` to methods `runSync`, `runAsync` and `startProcess` in class `CliTool` in library [lib/cli.dart](lib/cli.dart).

# 1.4.1

  * Added parameter `selectorFormatter` to function `promptUntilValidPick` from library [lib/io/interactive.dart](lib/io/interactive.dart)

# 1.4.0

  * Added library [lib/io/interactive.dart](lib/io/interactive.dart)

  * Moved function `promptYesNo` from library [lib/io/utils.dart](lib/io/utils.dart) to library [lib/io/interactive.dart](lib/io/interactive.dart)

  * Changes to `StringMyUtilityExtension` from library [lib/extensions/string.dart](lib/extensions/string.dart)

    - Renamed function `splitLines` to `splitLine` 

    - Improved function `splitLine`

# 1.3.6

  * Added constructor `JsonEnumConverter.withCache` to class `JsonEnumConverter` from library [lib/json_converters/json_enum_converter.dart](lib/json_converters/json_enum_converter.dart)

# 1.3.5

  * Added library [lib/json_converters/semver.dart](lib/json_converters/semver.dart)

  * Now exporting package `version` from library [lib/semver.dart](lib/semver.dart)

# 1.3.4

  * Changes to `JsonEnumConverter` in library [lib/json_converters/json_enum_converter.dart](lib/json_converters/json_enum_converter.dart):
    
    * Removed the getters `enumJsonMap` and `jsonEnumMap`

    * Added the getter `enumValues`

    * Removed override of the method `toJson`

# 1.3.3

  * Changes to `JsonEnumConverter` in library [lib/json_converters/json_enum_converter.dart](lib/json_converters/json_enum_converter.dart):
    
    * Made the class abstract and changed the member `enumJsonMap` to an abstract getter 

# 1.3.2

  * Added library [lib/json_converters/json_enum_converter.dart](lib/json_converters/json_enum_converter.dart)

  * Changes for `MapMyUtilityExtension` in library [lib/extensions/map.dart](lib/extensions/map.dart):

    * Added method `inverted` to `MapMyUtilityExtension` 

    * Improved method `toPrettyString` to `MapMyUtilityExtension` 

  * Added library [lib/dartx.dart](lib/dartx.dart), which just exports the dartx package.

# 1.3.1

  * Bumped minimum Dart SDK up to `>=3.1.5`, because the [Flutter SDK 3.13.9](https://github.com/flutter/flutter/releases/tag/3.13.9) uses Dart `3.1.5`. 

# 1.3.0

  * Updated dependencies for Flutter SDK compatability packages. Now the minimum supported Flutter SDK is [3.13.9](https://github.com/flutter/flutter/releases/tag/3.13.9)

# 1.2.2

  * Removed git dependency of package `dotenv`

  * Added test for class `MariaDbDumpCliTool`

  * Changed version constraints for dependency `xml` from `6.3.0` to `>=5.0.0 <=6.3.0`

# 1.2.1

  * Added libraries `lib/reflection.dart`, `lib/reflection/program_info.dart`, `lib/errors.dart`, `lib/errors/implementation_error.dart`, `lib/errors/result_error.dart` and `lib/errors/unreachable_code_error.dart`

  * Added tests `test/async/call_once_test.dart` and `test/reflection/program_info_test.dart`

# 1.2.0

  * Removed dependency `test_api`

  * Constrained dependency `test` to `^1.23.1`

  * Removed `lib/test/bootstrap`, `lib/test/fake.dart` and `lib/test/scaffolding.dart`

# 1.1.5

  * Made member `encoder` of class `FFProbeFormatTagsModel` nullable.

# 1.1.4

  * Bugfix in `FFMpegCliTool.modifyAudioFile`

# 1.1.3

  * Bugfix in `FFMpegCliTool.modifyAudioFile`

# 1.1.2

  * Modified `lib/cli/ffprobe.dart`

    * Added methods `FFProbeCliTool.getModel` and `MediaFormat.fromStreamModel`

    * Deprecated methods `FFProbeCliTool.getStreamModels` and `FFProbeCliTool.getMediaFormats`

  * Removed dependency `archive`

  * Bumped dependency `json_annotation` to `^4.9.0`

# 1.1.1

  * Modified lib `lib/test.dart`
    
    * Added libs `lib/test/test.dart`, `lib/test/scaffolding.dart`, `lib/test/fake.dart`, `lib/test/bootstrap/browser.dart`, `lib/test/bootstrap/node.dart` and `lib/test/bootstrap/vm.dart`

    * Modified lib `lib/test/expect.dart`. Added export for `package:test/expect.dart`

# 1.1.0

  * Added lib `lib/async.dart`
  
  * Moved lib `lib/call_once.dart` to `lib/async/call_once.dart`

  * Added lib `lib/async/completable_future.dart`

  * Removed dependency `ansicolor`

  * Added dependencies `test_api: ^0.4.18`, `test: any` and `collection: ^1.17.1` to the normal package dependencies

# 1.0.2

  * Modified lib `lib/cli/ffmpeg.dart`

    * Added member `outputFilepath` to class `AudioFileModificationProgressEvent`

# 1.0.1

  * Now exporting package `version` from `lib/cli/cli_tool.dart`
  
  * Modified library `printers.dart`

    * Changed implementation of `setPrintersColorUsage`. This change does not affect any users.

# 1.0.0

  * Added dependency `intl: ^0.18.0`

  * Added library `call_once.dart`

  * Added library `semver.dart`

  * Added library `exceptions.dart`

  * Modified library `json_converters.dart`:
    
    * Added library `json_converters/duration.dart` with classes `DurationNumberJsonConverter` `DurationStringJsonConverter`

    * Added library `json_converters/json_list_converter.dart` with the class `JsonListConverter`

  * Added library `typedefs.dart`

  * Modified library `printers.dart`

    * Renamed functions `setColorUsage` to `setPrintersColorUsage`

  * Replaced imports of `dart:io` with package `universal_io`

  * Modified library `extensions.dart`
    
    * Added methods `subdir`, `containsSync`, `contains`, `file` and `directory` to `DirectoryMyUtilityExtension`. These methods were copied from [package dartx 1.2.0](https://github.com/simc/dartx/blob/9179f570b59dc343e419bc005e71e011a8b0301e/lib/src/io/directory.dart).

    * Added methods `appendBytes`, `appendString` and `forEachBlock` to `FileMyUtilityExtension`. These methods were copied from [package dartx 1.2.0](https://github.com/simc/dartx/blob/9179f570b59dc343e419bc005e71e011a8b0301e/lib/src/io/file.dart).

    * Added methods and getters `name`, `nameWithoutExtension`, `dirName`, `isWithin`, `withName` and `extension` to `FileSystemEntityMyUtilityExtension`. These methods were copied from [package dartx 1.2.0](https://github.com/simc/dartx/blob/9179f570b59dc343e419bc005e71e011a8b0301e/lib/src/io/file_system_entity.dart).

    * Added method `toHumanReadableTimestamp` to `DateTimeMyUtilityExtension`

  * Added cli tool `FFMpegCliTool` and `FFProbeCliTool`

# 0.0.40

  * Added extension `RandomMyUtilityExtension`

  * Added extension `DoubleMyUtilityExtension` and class `DoubleWithTolerance`

  * Added library `date.dart` which containes the classes `MyDateUtility` and `MyDateTimeRange`

  * Added tests for the new functionality

# 0.0.39

  * Added method `FlutterCliTool.clean`

# 0.0.38

  * Allowing unrecognized keys when parsing flutter version

# 0.0.37
 
  * Added `DumpOption.skipTriggers`

# 0.0.36

  * Added parameter `binLinks` to method `NpmCliTool.getPackages`

# 0.0.35

  * Added the `port` parameter to all methods of the `MariaDbCliTool` and `MariaDbDumpCliTool`

# 0.0.34

  * Bugfix in  `PowershellCliTool.runScriptAsync` method

# 0.0.33

  * Changed return value of `CliTool._consumeProcess` method to `int`

# 0.0.32

  * Bugfix in  `CliTool._consumeProcess` method

# 0.0.31

  * Bugfix in  `PowershellCliTool.runScriptAsync` method

# 0.0.30

  * Added `PowershellCliTool.runScriptAsync` method

# 0.0.29

  * Bugfix in `DartCliTool`: The constructor default initialized the executable with `io.Platform.resolvedExecutable`, which works fine as long as no script is compiled, because the value of `io.Platform.resolvedExecutable` is the dart executable if a dart script is run, but if a dart script gets compiled, then the value of the resolved executable is the path to the compiled dart script.

# 0.0.28

  * Added function `printProgessAnimation` to `printers.dart`

# 0.0.27

  * Bugfix in `MariaDbDatabase.write` where columns that needed an enclosure in single quotes did not get one, now this is properly managed.

# 0.0.26

  * Added sublibrary `printers.dart`
  
  * Added sublibrary `string.dart`

# 0.0.25

  * Improved mariadb schemas api 

  * Added `encoding: io.systemEncoding` parameter to `batchFile.writeAsStringSync` in the constructor of a `CliTool`

# 0.0.24

  * Added method `FlutterVersion.toJsone`

  * Added extension `MapMyUtilityExtension`

# 0.0.23

  * Added parameter `entrypoint` to method `FlutterCliTool.build`

# 0.0.22

  * Bugfix in they way some cli tools retrieve their version

# 0.0.21

  * Added the suffix `CliTool` to all subclasses of the `CliTool` class

  * Added `PowershellCliTool`

# 0.0.20

  * Renamed `MariaDbTable.hasRows` to `MariaDbTable.hasColumnsWithRows` and fixed a bug in the getter that caused a crash if the table has no rows.

# 0.0.19

  * Bugfix in `promptYesNo`

# 0.0.18

  * Bugfix in `MariaDbTable.checkColumns`, the program would crash if the table had no columns

# 0.0.17

  * Bugfix in `StringMyUtilityExtension.splitLines`

  * Added `produceXml` parameter to method `MariaDbCliTool.execute`

  * Added `produceXml` parameter to method `MariaDbDumpCliTool.dump`

  * Added dart doc to the `DumpOption` enum

  * Added `MariaDbDumpCliTool.dumpAndParse` method which returns an instance of the class `MariaDbDatabase` that can be used to access the data of the dump result from a dart object.

# 0.0.16

  * Now properly utilizing `windowsCodePage`.

# 0.0.15

  * Added property `windowsCodePage` to `CliTool`, also set the default value of `windowsCodePage` to `WindowsCodePage.utf8`.

# 0.0.14

  * Added `FileStatMyUtilityExtension`

  * Added `NodeCli`

# 0.0.13

  * Fixed `FileSystemEntityMyUtilityExtension.absolutePosixPath` for windows

# 0.0.12

  * Reverted changes from `0.0.11` and added `FileSystemEntityMyUtilityExtension.absolutePosixPath` extension getter

# 0.0.11

  * Added `windows` parameter to `DirectoryMyUtilityExtension.resolveUri`, `DirectoryMyUtilityExtension.resolveUriSync`, `FileMyUtilityExtension.resolveUri` and `FileMyUtilityExtension.resolveUriSync` extension methods.

# 0.0.10

  * Added `host` parameter to all `MariaDbCliTool` and `MariaDbDumpCliTool` methods.

# 0.0.9

  * Added method `execute` to `MariaDbCliTool` and fixed implementation of `runSqlScript` and `restore`.

# 0.0.8

  * Changed the default encoding for all cli methods to `UTF-8`.
