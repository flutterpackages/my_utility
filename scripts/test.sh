#! /bin/bash

# shellcheck disable=SC2164


######################## CONSTANTS ########################

SCRIPT_DIR="$(dirname "$(readlink -e "$0")")"
readonly SCRIPT_DIR

# Used in function log()
# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\x1B[0m"
readonly TEXT_STYLE_FG_ERROR="\x1B[38;5;196m"
readonly TEXT_STYLE_FG_WARNING="\x1B[4;38;5;208m"
readonly TEXT_STYLE_FG_INFO="\x1B[38;5;46m"
readonly TEXT_STYLE_FG_DEBUG="\x1B[38;5;39m"
readonly TEXT_STYLE_FG_VERBOSE="\x1B[2;37m"

######################## GLOBALS ########################

# Used in function log()
g_log_level=1
g_log_enable_ansi=1
g_log_enable_timestamps=0
g_log_use_precise_timestamps=0
g_log_use_short_tags=0
g_log_sink="1"
g_log_sink_is_fd=1
g_log_error_sink="2"
g_log_error_sink_is_fd=1

######################## FUNCTIONS ########################

# Print a log message.
#
# This is version 1 of the log function.
#
# PARAMETERS
#
# a) For levels verbose, debug, info and warning:
#   1. The log level of the message.
#   2. A prefix for the message.
#   3+. The messages that should be printed to $g_log_sink.
#
# b) For level error:
#   1. The log level of the message.
#   2. A prefix for the message.
#   3. Exit code for the script.
#   4. Extra code to evaluate before exiting.
#   5+. The messages that should be printed to $g_log_error_sink.
#
# LEVELS
#
#   verbose -> 1
#   debug -> 2
#   info -> 3
#   warning -> 4
#   error -> 5 or greater
#
# GLOBALS
#
# $g_log_level:
#   Controls which events will be written to stdout, and which
#   will be skipped.
#
# $g_log_enable_ansi:
#   Controls whether ANSI coloring will be enabled for the log messages.
#
# $g_log_enable_timestamps:
#   Controls whether the log messages include timestamps. The timestamps
#   are printed in the ISO 8601 format.
#
# $g_log_use_precise_timestamps:
#   Controls the precision of the timestamps.
#
#   Has no effect, if $g_log_enable_timestamps is 0.
#
# $g_log_use_short_tags:
#   Controls whether the tags for the level of the log message should be
#   short (a single letter) or long (the full word for the level).
#
# $g_log_sink:
#   The sink to which all log messages, except error messages, are written to.
#   Can be a file or a file descriptor. If the sink is a file then the messages
#   will be appended to an existing file, otherwise a new one will be created
#   for the given name. If the sink is a file descriptor, then
#   $g_log_sink_is_fd must be set to 1, otherwise the file descriptor number
#   will be interpreted as a filename.
#
# $g_log_sink_is_fd:
#   Whether or not the value for $g_log_sink should be interpreted as a file
#   descriptor or as a filepath.
#
# $g_log_error_sink:
#   This is effectively the same as $g_log_sink, except only for error log
#   messages.
#
# $g_log_error_sink_is_fd:
#   Whether or not the value for $g_log_error_sink should be interpreted as a
#   file descriptor or as a filepath.
#
# EXAMPLE
#
#   log 1 " " "My verbose message with a tab prefix"
#
#   log 3 "" "My info message"
#
#   log 5 "" 24 "" "My error message with exit code 24"
#
#   log 5 "" 24 "ls -1" \
#     "My error message with exit code 24 and" \
#     "a list of all files in the current directory"
function log() {
  local log_level="$1"
  if [ "$g_log_level" -gt "$log_level" ]; then
    return
  fi

  local prefix="$2"

  local tag
  local style_start
  local style_end="$TEXT_STYLE_END"

  if [ "$log_level" -eq 1 ]; then # verbose
    tag="VERBOSE"
    style_start="$TEXT_STYLE_FG_VERBOSE"
  elif [ "$log_level" -eq 2 ]; then # debug
    tag="DEBUG"
    style_start="$TEXT_STYLE_FG_DEBUG"
  elif [ "$log_level" -eq 3 ]; then # info
    tag="INFO"
    style_start="$TEXT_STYLE_FG_INFO"
  elif [ "$log_level" -eq 4 ]; then # warning
    tag="WARNING"
    style_start="$TEXT_STYLE_FG_WARNING"
  elif [ "$log_level" -ge 5 ]; then # error
    tag="ERROR"
    style_start="$TEXT_STYLE_FG_ERROR"
  fi

  if [ 0 -ne "$g_log_use_short_tags" ]; then
    tag="${tag:0:1}"
  fi

  if [ 0 -ne "$g_log_enable_timestamps" ]; then
    local timestamp
    if [ 0 -eq "$g_log_use_precise_timestamps" ]; then
      timestamp="$(date -Iseconds)"
    else
      timestamp="$(date -Ins)"
    fi
    tag="$timestamp/$tag"
  fi

  if [ 0 -eq "$g_log_enable_ansi" ]; then
    style_start=""
    style_end=""
  fi

  tag="${prefix}${style_start}[$tag]:${style_end}"

  if [ "$log_level" -lt 5 ]; then
    shift 2
    if [ 0 -ne "$g_log_sink_is_fd" ]; then
      echo -e "$tag $*" >&"$g_log_sink"
    else
      echo -e "$tag $*" >>"$g_log_sink"
    fi
  else
    local exit_code="$3"
    local eval_code="$4"

    shift 4

    if [ 0 -ne "$g_log_error_sink_is_fd" ]; then
      echo -e "$tag $*" >&"$g_log_error_sink"
    else
      echo -e "$tag $*" >>"$g_log_error_sink"
    fi

    eval "$eval_code"

    exit "$exit_code"
  fi
}

function check_last_exit() {
  local last_exit=$?
  if [ 0 -ne $last_exit ]; then
    exit $last_exit
  fi
}

######################## SCRIPT ########################

cd "$SCRIPT_DIR/.."

log 3 "" "Testing on Dart VM with compiler kernel"
dart test -p vm -c kernel
check_last_exit
echo
echo

log 3 "" "Testing on Dart VM with compiler exe"
dart test -p vm -c exe
check_last_exit
echo
echo

log 3 "" "Testing on Chrome with compiler dart2js"
dart test -p chrome -c dart2js
check_last_exit
echo
echo

log 3 "" "Testing on Chrome with compiler dart2wasm"
dart test -p chrome -c dart2wasm
check_last_exit
echo
echo

log 3 "" "Sucessfully ran all tests for each configuration!"
