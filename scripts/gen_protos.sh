#! /bin/bash

SCRIPT_DIR="$(dirname "$(readlink -e "$0")")"
readonly SCRIPT_DIR

PROJ_DIR="$(dirname "$SCRIPT_DIR")"
readonly PROJ_DIR

cd "$PROJ_DIR" || (echo "Failed to cd into $PROJ_DIR"; exit 1)

outdir="$(mktemp -d)"

sudo docker build -t my_utility_protos .
sudo docker create --name my_utility_protos_tmp_container my_utility_protos > /dev/null
sudo docker cp my_utility_protos_tmp_container:/app/proto_out "$outdir" > /dev/null
sudo docker rm my_utility_protos_tmp_container > /dev/null

rm -rf test/protos
mkdir -p test/protos
cp "$outdir"/proto_out/* test/protos
